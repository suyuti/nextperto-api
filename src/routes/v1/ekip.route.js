const express = require('express')
const router = express.Router()

const EkipController = require('../../controllers/ekip.controller')

router.get('/',           EkipController.getAll)
router.get('/:id',        EkipController.getById)
router.post('/',          EkipController.create)
router.put('/:id',        EkipController.update)
router.delete('/:id',     EkipController.remove)

module.exports = router
