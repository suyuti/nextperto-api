const express = require('express')
const router = express.Router()

const GorusmeController = require('../../controllers/gorusme.controller')

router.get('/',           GorusmeController.getAll)
router.get('/:id',        GorusmeController.getById)
router.post('/',          GorusmeController.create)
router.put('/:id',        GorusmeController.update)
router.delete('/:id',     GorusmeController.remove)

module.exports = router
