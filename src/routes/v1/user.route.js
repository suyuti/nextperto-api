const express = require('express')
const router = express.Router()

const UserController = require('../../controllers/user.controller')

router.get('/',           UserController.getAll)
router.get('/:id',        UserController.getById)
router.post('/',          UserController.create)
router.put('/:id',        UserController.update)
router.delete('/:id',     UserController.remove)

router.get('/:id/ekip',   UserController.getEkibim)

/*
router.get('/:id/availableusers',       UserController.getAvailableUsers)
router.get('/:id/availabledepartmen',   UserController.getAvailableDepartmen)
router.get('/:id/team',                 UserController.getMyTeam)
router.get('/:id/manager',              UserController.getMyManager)
router.get('/:id/colleauges',           UserController.getMyColleauges)
*/

module.exports = router
