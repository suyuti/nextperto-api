const express = require('express')
const router = express.Router()

const ToplantiController = require('../../controllers/toplanti.controller')

router.get('/',           ToplantiController.getAll)
router.get('/:id',        ToplantiController.getById)
router.post('/',          ToplantiController.create)
router.put('/:id',        ToplantiController.update)
router.delete('/:id',     ToplantiController.remove)

router.get('/:id/karar',  ToplantiController.getKararlar)
router.post('/:id/karar',  ToplantiController.addKararlar)

router.get('/:id/history',      ToplantiController.getHistory)
router.get('/:id/gorev',        ToplantiController.getGorevler)
router.get('/:id/pdfrapor',     ToplantiController.getPdfRapor)

router.post('/:id/tamamla',        ToplantiController.tamamla)


module.exports = router
