const express = require('express')
const router = express.Router()

const TedarikciController = require('../../controllers/tedarikci.controller')

router.get('/',           TedarikciController.getAll)
router.get('/:id',        TedarikciController.getById)
router.post('/',          TedarikciController.create)
router.put('/:id',        TedarikciController.update)
router.delete('/:id',     TedarikciController.remove)

module.exports = router
