const express = require('express')
const router = express.Router()

const TenantController = require('../../controllers/tenant.controller')

router.get('/',           TenantController.getAll)
router.get('/:id',        TenantController.getById)
router.post('/',          TenantController.create)
router.put('/:id',        TenantController.update)
router.delete('/:id',     TenantController.remove)

router.get('/:id/nakitakisi',        TenantController.getNakitAkisi)
router.get('/:id/toplamlar',        TenantController.getToplamlar)
router.get('/:id/gidenbelgelerisorgula', TenantController.gidenBelgeleriSorgula)


router.get('/:id/tahsilat/gerceklesen', TenantController.gerceklesenTahsilatlar)
router.get('/:id/tahsilat/geciken', TenantController.gecikenTahsilatlar)
router.get('/:id/tahsilat/planlanan', TenantController.planlananTahsilatlar)
router.get('/:id/tahsilat/gelecek', TenantController.gelecekTahsilatlar)



module.exports = router
