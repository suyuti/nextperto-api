const express = require('express')
const router = express.Router()

const TopluFaturaController = require('../../controllers/toplufatura.controller')

router.get('/',           TopluFaturaController.getAll)
router.get('/:id',        TopluFaturaController.getById)
router.post('/',          TopluFaturaController.create)
router.put('/:id',        TopluFaturaController.update)
router.delete('/:id',     TopluFaturaController.remove)

module.exports = router
