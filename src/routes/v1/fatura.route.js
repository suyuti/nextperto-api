const express = require('express')
const router = express.Router()

const FaturaController = require('../../controllers/fatura.controller')

router.get('/', FaturaController.getAll)
router.get('/:id', FaturaController.getById)
router.post('/', FaturaController.create)
router.put('/:id', FaturaController.update)
router.delete('/:id', FaturaController.remove)
router.get('/:id/kalem', FaturaController.getKalemler)
router.post('/:id/kalem', FaturaController.addKalem)
router.put('/:id/kalem/:kid', FaturaController.updateKalem)
router.delete('/:id/kalem/:kid', FaturaController.removeKalem)
router.get('/:id/onizleme', FaturaController.getEFaturaOnIzleme)
router.get('/:id/belge', FaturaController.getBelge)
router.get('/:id/belgeindir', FaturaController.getBelge)
router.post('/:id/yenidengonder', FaturaController.yenidenGonder)

module.exports = router
