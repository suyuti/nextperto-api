const express = require('express')
const router = express.Router()

const StatsController = require('../../controllers/stats.controller')

router.get('/gorev',                StatsController.getGorev)
router.get('/tahsilat',             StatsController.getYapilacakTahsilatlar)
router.post('/gunfaturalar',        StatsController.getGundeKesilecekFaturalar)
router.get('/gelecektahsilatlar',   StatsController.getGelecekTahsilatlar)
router.get('/tahsilatrapor',        StatsController.getTahsilatRapor)

// Haftalik fatura vadeleri ve planlanmis tahsilatlar
// Hafta bazinda
// Toplam tutarlar
router.get('/alacaklar',            StatsController.getAlacaklar)
router.get('/odemeler',             StatsController.getOdemeler)

router.get('/gunlukrapor',          StatsController.getGunlukRapor)

module.exports = router
