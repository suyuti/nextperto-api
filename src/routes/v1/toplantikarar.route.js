const express = require('express')
const router = express.Router()

const ToplantiKararController = require('../../controllers/toplantiKarar.controller')

router.get('/',           ToplantiKararController.getAll)
router.get('/:id',        ToplantiKararController.getById)
router.post('/',          ToplantiKararController.create)
router.put('/:id',        ToplantiKararController.update)
router.delete('/:id',     ToplantiKararController.remove)

module.exports = router
