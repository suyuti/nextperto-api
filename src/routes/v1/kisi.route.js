const express = require('express')
const router = express.Router()

const KisiController = require('../../controllers/kisi.controller')

router.get('/',           KisiController.getAll)
router.get('/:id',        KisiController.getById)
router.post('/',          KisiController.create)
router.put('/:id',        KisiController.update)
router.delete('/:id',     KisiController.remove)

module.exports = router
