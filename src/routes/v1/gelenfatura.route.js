const express = require('express')
const router = express.Router()

const GelenFaturaController = require('../../controllers/gelenfatura.controller')

router.get('/', GelenFaturaController.getAll)
router.get('/:id', GelenFaturaController.getById)
//router.post('/',          GelenFaturaController.create)
router.put('/:id', GelenFaturaController.update)
router.delete('/:id', GelenFaturaController.remove)

//router.get('/:id/onizleme', GelenFaturaController.getEFaturaOnIzleme)
router.get('/:id/belge', GelenFaturaController.getBelge)
//router.get('/:id/belgeindir', GelenFaturaController.getBelge)

module.exports = router
