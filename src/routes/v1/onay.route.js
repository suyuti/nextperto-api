const express = require('express')
const router = express.Router()

const OnayController = require('../../controllers/onay.controller')

router.get('/',           OnayController.getAll)
router.get('/:id',        OnayController.getById)
//router.post('/',          OnayController.create)
router.put('/:id/onay',        OnayController.onayla)
router.put('/:id/red',        OnayController.reddet)
router.put('/:id/iptal',        OnayController.iptalet)
//router.delete('/:id',     OnayController.remove)

module.exports = router
