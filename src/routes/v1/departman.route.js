const express = require('express')
const router = express.Router()

const DepartmanController = require('../../controllers/departman.controller')

router.get('/',           DepartmanController.getAll)
router.get('/:id',        DepartmanController.getById)
router.post('/',          DepartmanController.create)
router.put('/:id',        DepartmanController.update)
router.delete('/:id',     DepartmanController.remove)

module.exports = router
