const express = require('express')
const router = express.Router()

const FirmaController = require('../../controllers/firma.controller')

router.get('/',           FirmaController.getAll)
router.get('/:id',        FirmaController.getById)
router.post('/',          FirmaController.create)
router.put('/:id',        FirmaController.update)
router.delete('/:id',     FirmaController.remove)

router.get('/:id/kisi',   FirmaController.getKisiler)
router.get('/:id/fatura/toplam',   FirmaController.getFaturalarToplam)
router.get('/:id/tahsilat/toplam',   FirmaController.getTahsilatlarToplam)


module.exports = router
