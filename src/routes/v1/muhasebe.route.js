const express = require('express')
const router = express.Router()

const MuhasebeController = require('../../controllers/muhasebe.controller')

router.get('/firma',           MuhasebeController.getFirmalar)
router.get('/tahsilat',        MuhasebeController.getTahsilatlar)

router.post('/rapor/fatura',    MuhasebeController.getRaporFatura)
router.get('/rapor/urun',      MuhasebeController.getRaporUrun)
router.get('/rapor/urun/aylik',      MuhasebeController.getRaporUrunAylik)

module.exports = router
