const express = require('express')
const router = express.Router()

const ToplantiController = require('../../controllers/toplanti.controller')

router.get('/toplantiYapilmamisFirmalar',           ToplantiController.getToplantiYapilmamisFirmalar)
router.get('/toplantiYapilmisFirmalar',           ToplantiController.getToplantiYapilmisFirmalar)

module.exports = router
