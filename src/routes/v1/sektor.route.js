const express = require('express')
const router = express.Router()

const SektorController = require('../../controllers/sektor.controller')

router.get('/',           SektorController.getAll)
router.get('/:id',        SektorController.getById)
router.post('/',          SektorController.create)
router.put('/:id',        SektorController.update)
router.delete('/:id',     SektorController.remove)

module.exports = router
