const express = require('express')
const router = express.Router()

const PlanlanmisTahsilatController = require('../../controllers/planlanmistahsilat.controller')

router.get('/',           PlanlanmisTahsilatController.getAll)
router.get('/:id',        PlanlanmisTahsilatController.getById)
router.post('/',          PlanlanmisTahsilatController.create)
router.put('/:id',        PlanlanmisTahsilatController.update)
router.delete('/:id',     PlanlanmisTahsilatController.remove)

module.exports = router
