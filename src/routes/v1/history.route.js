const express = require('express')
const router = express.Router()

const HistoryController = require('../../controllers/history.controller')

router.get('/:refid',        HistoryController.getByRefId)

module.exports = router
