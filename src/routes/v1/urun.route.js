const express = require('express')
const router = express.Router()

const UrunController = require('../../controllers/urun.controller')

router.get('/',           UrunController.getAll)
router.get('/:id',        UrunController.getById)
router.post('/',          UrunController.create)
router.put('/:id',        UrunController.update)
router.delete('/:id',     UrunController.remove)

module.exports = router
