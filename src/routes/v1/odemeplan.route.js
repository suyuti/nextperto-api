const express = require('express')
const router = express.Router()

const OdemePlanController = require('../../controllers/odemeplan.controller')

router.get('/', OdemePlanController.getAll)
router.get('/:id', OdemePlanController.getById)
router.post('/', OdemePlanController.create)
router.put('/:id', OdemePlanController.update)
router.delete('/:id', OdemePlanController.remove)

module.exports = router
