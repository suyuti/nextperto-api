const express = require('express')
const router = express.Router()

const OdemeController = require('../../controllers/odeme.controller')

router.get('/', OdemeController.getAll)
router.get('/:id', OdemeController.getById)
router.post('/', OdemeController.create)
router.put('/:id', OdemeController.update)
router.delete('/:id', OdemeController.remove)

module.exports = router
