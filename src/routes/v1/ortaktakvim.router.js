const express = require('express')
const router = express.Router()

const OrtakTakvimController = require('../../controllers/ortaktakvim.controller')

router.post('/',          OrtakTakvimController.getOrtakTakvim)

module.exports = router
