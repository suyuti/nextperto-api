const express = require('express')
const router = express.Router()

const TahislatController = require('../../controllers/tahsilat.controller')

router.get('/',           TahislatController.getAll)
router.get('/:id',        TahislatController.getById)
router.post('/',          TahislatController.create)
router.put('/:id',        TahislatController.update)
router.delete('/:id',     TahislatController.remove)

module.exports = router
