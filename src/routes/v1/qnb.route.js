const express = require('express')
const router = express.Router()

const QnbController = require('../../controllers/qnb.controller')

router.post('/onizleme',    QnbController.getEFaturaOnIzleme)

module.exports = router
