const express = require('express')
const router = express.Router()

const NotController = require('../../controllers/not.controller')

router.get('/',           NotController.getAll)
router.get('/:id',        NotController.getById)
router.post('/',          NotController.create)
router.put('/:id',        NotController.update)
router.delete('/:id',     NotController.remove)

module.exports = router
