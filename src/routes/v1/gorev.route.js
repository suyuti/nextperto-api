const express = require('express')
const router = express.Router()

const GorevController = require('../../controllers/gorev.controller')

router.get('/',                 GorevController.getAll)
router.get('/:id',              GorevController.getById)
router.post('/',                GorevController.create)
router.put('/:id',              GorevController.update)
router.delete('/:id',           GorevController.remove)

router.get('/:id/history',      GorevController.getHistory)
router.get('/:id/yorum',        GorevController.getYorumlar)
router.post('/:id/yorum',           GorevController.postYorum)
router.post('/:id/tamamla',         GorevController.tamamla)


module.exports = router
