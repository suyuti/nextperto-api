const pdf = require("pdfjs");
const fs = require('fs')
const moment = require('moment')
const numbro = require('numbro')

const font = new pdf.Font(
    fs.readFileSync("./fonts/open-sans/OpenSans-Regular.ttf")
);
const fontBold = new pdf.Font(
    fs.readFileSync("./fonts/open-sans/OpenSans-Semibold.ttf")
);
const fontLigth = new pdf.Font(
    fs.readFileSync("./fonts/open-sans/OpenSans-Light.ttf")
);


const firmaTable = (doc, firma, toplamAlacak, gecikenAlacak, planlananAlacak, toplamOdeme) => {
    var t = doc.table({
        widths: [100, 150, null, 50],
        borderWidth: 0
    })
    var r = t.row({paddingBottom: 0.2*pdf.cm})
    //r.cell('')
    r.cell(firma, { fontSize: 14, colspan: 4, font: fontBold })

    var r2 = t.row()
    r2.cell('')
    r2.cell('Toplam Alacaklar')
    r2.cell(`${numbro(toplamAlacak).format({thousandSeparated: true, mantissa: 2})} TL`, { fontSize: 12, textAlign: 'right' })

    var r3 = t.row()
    r3.cell('')
    r3.cell('Geciken Alacaklar')
    r3.cell(`${numbro(gecikenAlacak).format({thousandSeparated: true, mantissa: 2})} TL`, { fontSize: 12, textAlign: 'right' })

    var r4 = t.row()
    r4.cell('')
    r4.cell('Planlanan Alacaklar')
    r4.cell(`${numbro(planlananAlacak).format({thousandSeparated: true, mantissa: 2})} TL`, { fontSize: 12, textAlign: 'right' })

    var r5 = t.row()
    r5.cell('')
    r5.cell('Toplam Ödeme')
    r5.cell(`${numbro(toplamOdeme).format({thousandSeparated: true, mantissa: 2})} TL`, { fontSize: 12, textAlign: 'right' })
}

const kesilecekFaturalar = (doc, firma, faturalar) => {
    var t = doc.table({
        widths: [100, null, null, null, null, 50],
        borderWidth: 0
    })
    var r = t.row()
    r.cell(firma, { fontSize: 14, colspan: 6, font: fontBold })

    for (let fatura of faturalar) {
        var _r = t.row()
        _r.cell('')
        _r.cell(fatura.firma)
        _r.cell(fatura.urun)
        _r.cell(fatura.vade)
        _r.cell(`${numbro(fatura.tutar).format({thousandSeparated: true, mantissa: 2})} TL`, { fontSize: 12, textAlign: 'right'})
    }
}

const yapilacakTahsilatlar = (doc, firma, tahsilatlar) => {
    var t = doc.table({
        widths: [100, null, null, null, null, 50]
    })
    var r = t.row()
    r.cell(firma, { fontSize: 14, colspan: 6, font: fontBold })

    for (let tahsilat of tahsilatlar) {
        var _r = t.row()
        _r.cell('')
        _r.cell(tahsilat.firma)
        _r.cell(tahsilat.tahsilatTuru)
        _r.cell(tahsilat.tahsilatVade)
        _r.cell(`${numbro(tahsilat.tutar).format({thousandSeparated: true, mantissa: 2})} TL`, { fontSize: 12, textAlign: 'right'})
    }
}

exports.gunlukRapor = (data) => {
    const src = fs.readFileSync("./experto-logo.jpg");
    const logo = new pdf.Image(src);
    var doc = new pdf.Document({
        font: font,
        properties: {
            author: "suyuti",
        },
    });

    //--- Header
    var headerRow = doc
        .header()
        .table({ widths: [null, null] })
        .row();
    headerRow.cell().image(logo, { height: 0.8 * pdf.cm });
    headerRow
        .cell()
        .text({ textAlign: "right" })
        .add(`12345`, { fontSize: 8 });

    doc.footer().pageNumber(function (curr, total) { return curr + ' / ' + total }, { textAlign: 'center' })

    doc.cell().text(`Günlük Rapor `, {
        textAlign: "center",
        fontSize: 18,
        font: fontBold,
        paddingBottom: 1 * pdf.cm,
    }).add(` ${moment().format('DD.MM.YYYY')}`, {fontSize: 12, font: fontLigth});

    firmaTable(doc, 'Experto', 1500000, 200, 300, 400)

    doc.cell('', { minHeight: 20, paddingBottom: 10 })

    firmaTable(doc, 'ArgeEvi', 1100, 1200, 1300, 1400)

    doc.cell('', { minHeight: 20, paddingBottom: 10 })

    firmaTable(doc, 'ISG', 5600, 2200, 2300, 2400)

    doc.cell("Bugün Kesilecek Faturalar", {
        textAlign: "center",
        fontSize: 18,
        font: fontBold,
        paddingTop: 2 * pdf.cm,
        paddingBottom: 0.2 * pdf.cm,
    });

    kesilecekFaturalar(doc, 'Experto', [
        {
            firma: 'Firma 1',
            urun: 'destek urunu',
            vade: '25.03.2021',
            tutar: 2530
        },
        {
            firma: 'Firma 1',
            urun: 'destek urunu',
            vade: '25.03.2021',
            tutar: 4200
        }
    ])

    doc.cell("Bugün Yapılacak Tahsilatlar", {
        textAlign: "center",
        fontSize: 18,
        font: fontBold,
        paddingTop: 2 * pdf.cm,
        paddingBottom: 0.2 * pdf.cm,
    });
    yapilacakTahsilatlar(doc, 'Experto', [
        {
            firma: 'Firma 1',
            tahsilatTuru: '',
            tahsilatVade: '',
            tutar: 4520
        },
        {
            firma: 'Firma 1',
            tahsilatTuru: 'çek',
            tahsilatVade: '25.03.2021',
            tutar: 12000
        }
    ])


    return doc.asBuffer();
}