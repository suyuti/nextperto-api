exports.parse = (query) => {
    let result = {}

    let parts = query.split('&')
    for(let part of parts) {
        if (part.startsWith('select')) { // select=a,b,c
            let selectPart = part.split('=')[1]
            let selects = selectPart.split(',')
            result.select = {}
            for (let s of selects) {
                result.select[s] = 1
            }
        }
        else if (part.startsWith('populate')) { // select=a,b,c
            let popPart = part.split('=')[1]
            let pops = popPart.split(',')
            result.populate = []
            for (let p of pops) {
                result.populate.push({path: p})
            }
        }
    }
    return result
}