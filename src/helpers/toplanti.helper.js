exports.generateContent = (toplanti) => {
    return `<table style="width: 90%;">
    <tbody>
      <tr style="height: 62px;">
        <td style="width: 608px; height: 62px;" colspan="2">
          <p><strong>Toplantı Bilgileri</strong></p>
        </td>
      </tr>
      <tr style="height: 21px;">
        <td style="width: 157px; height: 21px;"><strong>Toplantı Türü</strong></td>
        <td style="width: 451px; height: 21px;">${toplanti.tur}</td>
      </tr>
      <tr style="height: 10px;">
        <td style="width: 157px; vertical-align: top; height: 10px;"><b>Toplantı Kategorisi</b></td>
        <td style="width: 451px; height: 10px;">${toplanti.kategori}</td>
      </tr>
      <tr style="height: 10px;">
        <td style="width: 157px; vertical-align: top; height: 10px;"><b>Açıklama</b></td>
        <td style="width: 451px; height: 10px;">${toplanti.aciklama}</td>
      </tr>
    </tbody>
    </table>
    <p></p>
    <p><em>Bu mesaj otomatik olarak gönderilmiştir</em></p>`;
}