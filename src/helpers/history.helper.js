const HistoryModel = require('../models/history.model')
const {difference} = require('./diff.helper')

exports.addHistory = async (prevState, nextState, referenceItem, message, user, collection) => {
    try {
        let diff = difference(nextState, prevState)
        let diffStates = []
        Object.keys(diff).map((key, i) => {
            let item = {
                field: key,
                prev: prevState[key],
                next: nextState[key]
            }
            diffStates.push(item)

        })

        let newHistoy = await new HistoryModel({
            createdBy: user._id,
            createdAt: new Date(),
            referenceItem: referenceItem,
            changeDetail: message,
            diff: diffStates,
            refCollection: collection
        }).save()
    }
    catch (e) {
        console.log(e)
    }
}

exports.addHistory2 = async (diff, referenceItem, message, user, collection) => {
    try {
        //let diffStates = []
        //Object.keys(diff).map((key, i) => {
        //    let item = {
        //        field: key,
        //        prev: prevState[key],
        //        next: nextState[key]
        //    }
        //    diffStates.push(item)
        //})

        let newHistoy = await new HistoryModel({
            createdBy: user._id,
            createdAt: new Date(),
            referenceItem: referenceItem,
            changeDetail: message,
            diff: diff,
            refCollection: collection
        }).save()
    }
    catch (e) {
        console.log(e)
    }
}