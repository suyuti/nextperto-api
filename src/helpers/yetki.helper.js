exports.yetkiler = () => {
    return [
        {key: 'aktifmusteri', create: true, read: true, write: true, dlete: true},
        {key: 'gorev', create: true, read: true, write: true, dlete: true},
        {key: 'toplanti', create: true, read: true, write: false, dlete: true}
    ]
}