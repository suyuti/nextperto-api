const _ = require('lodash')

exports.difference = (object, base) => {
	function changes(object, base) {
		return _.transform(object, function(result, value, key) {
			if (!_.isEqual(value, base[key])) {

                if (_.isDate(value) && _.isDate(base[key])) {
                    result[key] = value                    
                }
                else {
                    if (_.isObject(value) && _.isObject(base[key])) {
                        result[key] = changes(value, base[key])
                    }
                    else {
                        result[key] = value
                    }
                }
			}
		});
	}
	return changes(object, base);
}
