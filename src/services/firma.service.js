const MongooseQueryParser = require('mongoose-query-parser')
const Firma = require('../models/firma.model')
const Fatura = require('../models/fatura.model')
const Tahsilat = require('../models/tahsilat.model')

exports.find = async (query) => {
    let filter = query || {}
    let parser = new MongooseQueryParser.MongooseQueryParser({})
    let f = parser.parse(filter)
    let firmalar = await Firma
        .find(f.filter)
        .select(f.select)
        .skip(f.skip)
        .limit(f.limit)
        .populate(f.populate)
        .sort(f.sort)
        .lean(true)
        .exec();
    if (firmalar) {
        let count = await Firma.countDocuments(f.filter).exec()
        return {
            data: firmalar,
            page: f.skip || 0,
            total: count
        }
    }
}

exports.findOne = async (id, query) => {
    let filter = query || {}
    let parser = new MongooseQueryParser.MongooseQueryParser({})
    let f = parser.parse(filter)
    let firma = await Firma
        .find({ _id: id, ...f.filter })
        .select(f.select)
        .populate(f.populate)
        .exec();
    if (firma && firma.length == 1) {
        return firma[0]
    }
    throw 'Firma Bulunamadi'

    //let firma = await Firma.findById(id).exec()
    //return firma
}

exports.create = async (firma) => {
    let newFirma = await new Firma(firma).save()
    return newFirma
}

exports.update = async (id, firmaUpdateDto) => {
    let firma = await Firma.findByIdAndUpdate(id, firmaUpdateDto, { new: true }).exec()
    return firma
}

exports.remove = async (id) => {
    let deleted = await Firma.findByIdAndDelete(id)
    return deleted
}

exports.getToplamFatura = async (key) => {
    let toplamFatura = await Fatura.aggregate([
        {
            $match: {
                vfirma: key
            }
        }, 
        {
            $project: {
                genelToplam: 1
            }
        }, 
        {
            $group: {
                _id: 0,
                toplam: {
                    $sum : '$genelToplam'
                },
                adet: {
                    $sum: 1
                }
            }
        }
    ]).exec()
    return toplamFatura
}

exports.getToplamTahsilat = async (key) => {
    let toplamTahsilat = await Tahsilat.aggregate([
        {
            $match: {
                vfirma: key
            }
        }, 
        {
            $project: {
                tutar: 1
            }
        }, 
        {
            $group: {
                _id: 0,
                toplam: {
                    $sum : '$tutar'
                },
                adet: {
                    $sum: 1
                }
            }
        }
    ]).exec()
    return toplamTahsilat
}
