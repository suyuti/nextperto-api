const MongooseQueryParser = require('mongoose-query-parser')
const diffHistory = require('mongoose-diff-history/diffHistory');
const TopluFatura = require('../models/toplufatura.model')
const FaturaModel = require('../models/fatura.model')
const Firma = require('../models/firma.model')
const myQueryParser = require('../helpers/queryparser')

exports.find = async (query) => {
    try {
        let filter = query || {}
        let parser = new MongooseQueryParser.MongooseQueryParser({})
        let f = parser.parse(filter)
        let listeler = await TopluFatura
            .find(f.filter)
            .select(f.select)
            .skip(f.skip)
            .limit(f.limit)
            .populate(f.populate)
            .lean(true)
            .exec();
        if (listeler) {
            let count = await TopluFatura.countDocuments(f.filter).exec()
            return {
                data: listeler,
                page: f.skip,
                total: count
            }
        }
    }
    catch (e) {
        console.log(e)
    }
}

//exports.original_findOne = async (id, query) => {
//    let filter = query || {}
//    let parser = new MongooseQueryParser.MongooseQueryParser({})
//    let f = parser.parse(filter)
//    let topluFatura = await TopluFatura
//    .find({_id: id, ...f.filter})
//    .select(f.select)
//    .populate(f.populate)
//    .exec();
//    if (topluFatura && topluFatura.length == 1) {
//        return topluFatura[0]
//    }
//    throw 'Toplu Fatura Bulunamadi'
//}

// TODO 05.02.2021
// Bu metodda populate kisminda problem var.
// TopluFatura'da liste[] altinda firma bilgilerinin (vfirma) virtual olarak populate edilmesi gerekiyor
// MongooseQueryParser liste altinda populate yapamiyor
// Bu yuzden populate kismi dogrudan query'den alindi.
// https://github.com/leodinas-hao/mongoose-query-parser/issues/8

exports.findOne = async (id, query) => {
    let filter = query || {}
    let parser = new MongooseQueryParser.MongooseQueryParser({})
    let f = parser.parse(filter)
    let populates = query.populate.split(',')
    let topluFatura = await TopluFatura
        .findOne({ _id: id, ...f.filter })
        .select(f.select)
        .populate(populates)
        .lean(true)
        .exec();

    //let faturalar = await FaturaModel.find().lean(true).exec()
    //for( let f of topluFatura.liste) {
    //    faturalar.filter(fat => fat.key == f.key)
    //}



    if (topluFatura) {
        return topluFatura
    }
    //if (topluFatura && topluFatura.length == 1) {
    //    return topluFatura[0]
    //}
    throw 'Toplu Fatura Bulunamadi'
}

exports.create = async (liste) => {
    let newListe = await new TopluFatura(liste).save()
    return newListe
}

exports.update = async (id, listeUpdateDto, user) => {
    TopluFatura.findByIdAndUpdate(id, listeUpdateDto, {
        new: true,
        __user: user._id,
        __reason: "updated"
    }, (err, doc) => {
        if (!err) {
            return doc
        }
    })
}

exports.remove = async (id) => {
    let deleted = await TopluFatura.findByIdAndDelete(id)
    return deleted
}

exports.getHistory = async (id) => {
    let histories = await diffHistory.getDiffs('TopluFatura', id)
    return histories
}
//-------------------------------------------------------------------------------------
// Ayin belli gununde kesilecek faturalar

exports.gundeKesilecekFaturalar = async (ayinGunu) => {
    let result = {
        faturalar: [],
        araToplam: 0,
        kdv: 0,
        genelToplam: 0,
        adet: 0
    }

    let topluFaturaListeleri = await TopluFatura.find({ faturaKesimGunu: ayinGunu, active: true }).populate('liste.firma', 'marka').lean(true).exec()

    if (topluFaturaListeleri) {
        for (let liste of topluFaturaListeleri) {
            for (let fatura of liste.liste) {
                result.faturalar.push({
                    vfirma: fatura.vfirma,
                    marka: fatura.firma.marka,
                    urun: fatura.urun,
                    aciklama: fatura.aciklama,
                    toplam: fatura.araToplam,
                    kdv: fatura.kdv,
                    genelToplam: fatura.genelToplam
                })
                result.toplam += fatura.araToplam
                result.kdv += fatura.kdv
                result.genelToplam += fatura.genelToplam
                result.adet += 1
            }
        }
    }
    return result
}