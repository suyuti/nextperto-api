const MongooseQueryParser = require('mongoose-query-parser')
const Urun = require('../models/urun.model')

exports.find = async (query) => {
    let filter = query || {}
    let parser = new MongooseQueryParser.MongooseQueryParser({})
    let f = parser.parse(filter)
    let urunler = await Urun
        .find(f.filter)
        .select(f.select)
        .skip(f.skip)
        .limit(f.limit)
        .populate(f.populate)
        .sort(f.sort)
        .lean(true)
        .exec();
    if (urunler) {
        let count = await Urun.countDocuments(f.filter).exec()
        return {
            data: urunler,
            page: f.skip || 0,
            total: count
        }
    }
}

exports.findOne = async (id, query) => {
    let filter = query || {}
    let parser = new MongooseQueryParser.MongooseQueryParser({})
    let f = parser.parse(filter)
    let urun = await Urun
        .find({ _id: id, ...f.filter })
        .select(f.select)
        .populate(f.populate)
        .exec();
    if (urun && urun.length == 1) {
        return urun[0]
    }
    throw 'Urun Bulunamadi'

    //let urun = await Urun.findById(id).exec()
    //return urun
}

exports.create = async (urun) => {
    let newUrun = await new Urun(urun).save()
    return newUrun
}

exports.update = async (id, urunUpdateDto) => {
    let urun = await Urun.findByIdAndUpdate(id, urunUpdateDto, { new: true }).exec()
    return urun
}

exports.remove = async (id) => {
    let deleted = await Urun.findByIdAndDelete(id)
    return deleted
}
