const MongooseQueryParser = require('mongoose-query-parser')
const Departman = require('../models/departman.model')

exports.find = async (query) => {
    let type = query.type
    let filter = query || {}
    delete filter.type
    if (type == 'asTree') {
        let departmanlar = await Departman.getChildrenTree({
            //filters: filter
        })
        if (departmanlar) {
            //let count = await Departman.countDocuments(f.filter).exec()
            return {
                data: departmanlar,
                page: 0, //f.skip,
                total: 0 //count
            }
        }
    }
    else if (type == 'asList') {
        let parser = new MongooseQueryParser.MongooseQueryParser({})
        let f = parser.parse(filter)
        let departmanlar = await Departman
            .find(f.filter)
            .select(f.select)
            .skip(f.skip)
            .limit(f.limit)
            .populate(f.populate)
            .exec();
        if (departmanlar) {
            let count = await Departman.countDocuments(f.filter).exec()
            return {
                data: departmanlar,
                page: f.skip,
                total: count
            }
        }
    }
}

exports.findOne = async (id, query) => {
    let filter = query || {}
    let parser = new MongooseQueryParser.MongooseQueryParser({})
    let f = parser.parse(filter)
    let departman = await Departman
    .find({_id: id, ...f.filter})
    .select(f.select)
    .populate(f.populate)
    .exec();
    if (departman && departman.length == 1) {
        return departman[0]
    }
    throw 'Departman Bulunamadi'
}

exports.create = async (departman) => {
    let newDepartman = await new Departman(departman).save()
    return newDepartman
}

exports.update = async (id, departmanUpdateDto) => {
    let departman = await Departman.findByIdAndUpdate(id, departmanUpdateDto, { new: true }).exec()
    return departman
}

exports.remove = async (id) => {
    let deleted = await Departman.findByIdAndDelete(id)
    return deleted
}
