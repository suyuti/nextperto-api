const MongooseQueryParser = require('mongoose-query-parser')
const diffHistory = require('mongoose-diff-history/diffHistory');
const PlanlanmisTahsilatModel = require('../models/planlanmistahsilat.model')
const Firma = require('../models/firma.model');
const { uuid } = require('uuidv4');

exports.find = async (query) => {
    try {
        let filter = query || {}
        let parser = new MongooseQueryParser.MongooseQueryParser({})
        let f = parser.parse(filter)
        let ptahsilatlar = await PlanlanmisTahsilatModel
            .find(f.filter)
            .select(f.select)
            .skip(f.skip)
            .limit(f.limit)
            .populate(f.populate)
            .sort(f.sort)
            .exec();
        if (ptahsilatlar) {
            let count = await PlanlanmisTahsilatModel.countDocuments(f.filter).exec()
            return {
                data: ptahsilatlar,
                page: f.skip,
                total: count
            }
        }
    }
    catch (e) {
        console.log(e)
    }
}

exports.findOne = async (id, query) => {
    let filter = query || {}
    let parser = new MongooseQueryParser.MongooseQueryParser({})
    let f = parser.parse(filter)
    let pTahsilat = await PlanlanmisTahsilatModel
    .find({_id: id, ...f.filter})
    .select(f.select)
    .populate(f.populate)
    .exec();
    if (pTahsilat && pTahsilat.length == 1) {
        return pTahsilat[0]
    }
    throw 'PlanlanmisTahsilat Bulunamadi'
}

exports.create = async (ptahsilat, user) => {
    ptahsilat.key = uuid()
    ptahsilat.vcreatedBy = user.key
    ptahsilat.createdAt=new Date()
    let newPTahsilat = await new PlanlanmisTahsilatModel(ptahsilat).save()
    return newPTahsilat
}

exports.update = async (id, tahsilatUpdateDto, user) => {
    PlanlanmisTahsilatModel.findByIdAndUpdate(id, tahsilatUpdateDto, {
        new: true,
        __user: user._id,
        __reason: "updated"
    }, (err, doc) => {
        if (!err) {
            return doc
        }
    })
}

exports.remove = async (id) => {
    let deleted = await PlanlanmisTahsilatModel.findByIdAndDelete(id)
    return deleted
}

exports.getHistory = async (id) => {
    let histories = await diffHistory.getDiffs('PlanlanmisTahsilat', id)
    return histories
}