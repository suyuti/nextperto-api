const MongooseQueryParser = require('mongoose-query-parser')
const User = require('../models/user.model')
const EkipModel = require('../models/ekip.model')

exports.find = async (query) => {
    let filter = query || {}
    let parser = new MongooseQueryParser.MongooseQueryParser({})
    let f = parser.parse(filter)
    let userler = await User
        .find(f.filter)
        .select(f.select)
        .skip(f.skip)
        .limit(f.limit)
        .populate(f.populate)
        .exec();
    if (userler) {
        let count = await User.countDocuments(f.filter).exec()
        return {
            data: userler,
            page: f.skip,
            total: count
        }
    }
}

exports.findOne = async (query) => {
    let filter = query || {}
    let parser = new MongooseQueryParser.MongooseQueryParser({})
    let f = parser.parse(filter)
    let user = await User
    //.find({_id: id, ...f.filter})
    .find(f.filter)
    .select(f.select)
    .populate(f.populate)
    .exec();
    if (user && user.length == 1) {
        return user[0]
    }
    throw 'User Bulunamadi'

    //let user = await User.findOne(id).populate('tenants').exec()
    //return user
}

exports.create = async (user) => {
    let newUser = await new User(user).save()
    return newUser
}

exports.update = async (id, userUpdateDto) => {
    let user = await User.findByIdAndUpdate(id, userUpdateDto, {new:true}).exec()
    return user
}

exports.remove = async (id) => {
    let deleted = await User.findByIdAndDelete(id)
    return deleted
}

/*
    Kullanicinin yetkileri listesini doner
*/
exports.getPermissions = async (id) => {
    // User'ın rolleri bulunur
    // Bu rollerin permissionları toplanır

    let user = await User.findOne({_id: id}).populate('roles').lean(true).exec()
    if (!user) {
        return []
    }

    var result = []

    for( var role of user.roles) {
        if (!role.active) {
            continue
        }
        for (let permission of role.permissions) {
            let i = result.findIndex(r => r.key == permission.key)
            if (i >= 0) {
                let r = result[i]
                let p = {
                    key: permission.key,
                    create: permission.create || r.create,
                    read: permission.create || r.read,
                    write: permission.create || r.write,
                    dlete: permission.create || r.dlete,
                }
                result[i] = p
            }
            else {
                result.push(permission)
            }
        }
    }
    return result


//    return [
//        {key: 'aktifmusteri', create: true, read: true, write: true, dlete: true},
//        {key: 'gorev', create: true, read: true, write: true, dlete: true},
//        {key: 'toplanti', create: true, read: true, write: false, dlete: true}
//    ]
}

exports.getYoneticisi = async (user) => {
    let uyeOlduguEkipler = await EkipModel.find({vuyeler: user.key}).lean(true).exec()
    if (uyeOlduguEkipler.length == 1) {
        let yonetici = await User.findOne({key: uyeOlduguEkipler[1].vyonetici}).lean(true).exec()
        return yonetici
    }
    else if (uyeOlduguEkipler.length > 1) {
        throw `${user.key} Birden fazla yoneticisi var`
    }
    else if (uyeOlduguEkipler.length == 0) {
        return user
        //throw `${user.key} yoneticisi yok`
    }
}
