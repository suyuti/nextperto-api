const MongooseQueryParser = require('mongoose-query-parser')
const Gorusme = require('../models/gorusme.model')

exports.find = async (query) => {
    try {
        let filter = query || {}
        let parser = new MongooseQueryParser.MongooseQueryParser({})
        let f = parser.parse(filter)
        let gorusmeler = await Gorusme
            .find(f.filter)
            .select(f.select)
            .skip(f.skip)
            .limit(f.limit)
            .populate(f.populate)
            .sort(f.sort)
            .exec();
        if (gorusmeler) {
            let count = await Gorusme.countDocuments(f.filter).exec()
            return {
                data: gorusmeler,
                page: f.skip,
                total: count
            }
        }
    }
    catch(e) {
        console.log(e)
    }
}

exports.findOne = async (id, query) => {
    let filter = query || {}
    let parser = new MongooseQueryParser.MongooseQueryParser({})
    let f = parser.parse(filter)
    let gorusme = await Gorusme
    .find({_id: id, ...f.filter})
    .select(f.select)
    .populate(f.populate)
    .exec();
    if (gorusme && gorusme.length == 1) {
        return gorusme[0]
    }
    throw 'Gorusme Bulunamadi'
}

exports.create = async (gorusme) => {
    let newGorusme = await new Gorusme(gorusme).save()
    return newGorusme
}

exports.update = async (id, gorusmeUpdateDto, user) => {
    Gorusme.findByIdAndUpdate(id, gorusmeUpdateDto, {
        new:true,
        __user: user._id,
        __reason: "updated"
    }, (err, doc) => {
        if (!err) {
            return doc
        }
    })
}

exports.remove = async (id) => {
    let deleted = await Gorusme.findByIdAndDelete(id)
    return deleted
}
