const MongooseQueryParser = require('mongoose-query-parser')
const diffHistory = require('mongoose-diff-history/diffHistory');
const Fatura = require('../models/fatura.model')
const FaturaKalem = require('../models/faturakalem.model')
const Firma = require('../models/firma.model')
const FaturaKalemService = require('./faturakalem.service');
const mongoose = require('mongoose');
const AmqpService = require('../providers/amqp.provider')
const TenantService = require('../services/tenant.service')
const FirmaService = require('../services/firma.service');
const moment = require('moment');
const { uuid } = require('uuidv4');

exports.find = async (query) => {
    try {
        let filter = query || {}
        let parser = new MongooseQueryParser.MongooseQueryParser({})
        let f = parser.parse(filter)
        let faturalar = await Fatura
            .find(f.filter)
            .select(f.select)
            .skip(f.skip)
            .populate(f.populate)
            .sort(f.sort)
            .limit(f.limit)
            .lean(true)
            .exec();
        if (faturalar) {
            let count = await Fatura.countDocuments(f.filter).exec()
            return {
                data: faturalar,
                page: f.skip,
                total: count
            }
        }
    }
    catch (e) {
        console.log(e)
    }
}

exports.findOne = async (id, query) => {
    let filter = query || {}
    let parser = new MongooseQueryParser.MongooseQueryParser({})
    let f = parser.parse(filter)
    let fatura = await Fatura
        .find({ _id: id, ...f.filter })
        .select(f.select)
        .populate(f.populate)
        .exec();
    if (fatura && fatura.length == 1) {
        return fatura[0]
    }
    throw 'Fatura Bulunamadi'
}

exports.create = async (fatura) => {
    try {
        let newFatura = await new Fatura(fatura).save()
        for (let kalem of fatura.kalemler) {
            this.addKalem(newFatura.key, kalem)
        }
        if (!newFatura.taslak) {
            // Taslak degilse ve Efatura ise EFatura islemi yapilir
            await this.sendEFatura(newFatura, fatura.kalemler)
        }
        return newFatura
    }
    catch (e) {
        console.log(e)
    }
    /*
        //const session = await mongoose.startSession()
        //try {
        //    session.startTransaction()
        let kalemler = fatura.kalemler
        let newFatura = await new Fatura(fatura).save()
    
        // Fatura kalemleri kaydedilir
        for (let kalem of kalemler) {
            this.addKalem(newFatura.key, kalem)
        }
    
        if (!newFatura.taslak) {
            // Taslak degilse ve Efatura ise EFatura islemi yapilir
            await this.sendEFatura(newFatura, kalemler)
        }
    
        //await session.commitTransaction()
        return newFatura
        //}
        //catch(e) {
        //    console.log(e)
        //}
        //finally{
        //    session.endSession()
        //}
        */
}

exports.update = async (id, faturaUpdateDto, user) => {
    Fatura.findByIdAndUpdate(id, faturaUpdateDto, {
        new: true,
        __user: user._id,
        __reason: "updated"
    }, (err, doc) => {
        if (!err) {
            return doc
        }
    })
}

exports.remove = async (id) => {
    let deleted = await Fatura.findByIdAndDelete(id)
    return deleted
}

exports.getHistory = async (id) => {
    let histories = await diffHistory.getDiffs('Fatura', id)
    return histories
}

exports.getKalemler = async (id) => {
    let fatura = await Fatura.findById(id).exec()
    if (!fatura) {
        throw 'Fatura bulunamadi'
    }
    let kalemler = await FaturaKalemService.find(`vfatura=${fatura.key}&populate=urun`)
    return kalemler
}

exports.addKalem = async (faturaKey, kalem) => {
    let _kalem = await FaturaKalemService.create({ vfatura: faturaKey, ...kalem })
    return _kalem
}

exports.sendEFatura = async (fatura, kalemler) => {
    try {
        let eFaturaResp = await AmqpService.sendFatura({
            command: 'FATURA_KES',
            payload: {key: fatura.key}
        })





/*


        let _tenant = await TenantService.find(`key=${fatura.vkesenFirma}`)
        let _firma = await FirmaService.find(`key=${fatura.vfirma}`)

        if (_tenant.data.length <= 0 || _firma.data.length <= 0) {
            return
        }

        let tenant = _tenant.data[0]
        let firma = _firma.data[0]

        let today = new Date(tenant.sonFaturaTarihi)
        today.setHours(0, 0, 0, 0)

        let faturaDuzenlemeTarihi = new Date(fatura.duzenlemeTarihi)
        faturaDuzenlemeTarihi.setHours(0, 0, 0, 0)

        let _fatura = {
            _id: fatura._id,
            tenant: tenant.key,
            url: tenant.efaturaUrl,
            user: tenant.efaturaUser,
            pwd: tenant.efaturaPwd,
            gecmisTarihli: faturaDuzenlemeTarihi < today,
            faturaKodu: faturaDuzenlemeTarihi < today ? tenant.gecmisTarihFaturaKodu : tenant.faturaKodu,   // tenant.faturaKodu,
            gonderen: {
                vkn: tenant.vkn,   //'3810685526',
                firmaAdi: tenant.unvan, // 'Gonderen Firma AS',
                adres: tenant.adres, //'gonderen cd.',
                ilce: tenant.ilce,  //'gungoren',
                il: tenant.il,    //'istanbul',
                ulke: tenant.ulke,  //'Turkiye'
                vd: tenant.vd,
                kontakTelefon: tenant.kontakTelefon,
                kontakMail: tenant.kontakMail

            },
            alan: {
                vkn: firma.vergiNo,//'3810685527',
                firmaAdi: firma.unvan,//'Alan Firma AS',
                adres: firma.adres,//'alan cd.',
                ilce: firma.ilce,//'Sariyer',
                il: firma.il,//'Istanbul',
                ulke: firma.ulke,//'Turkiye'
                vd: firma.efaturaTCKNKullan ? firma.efaturaTCKN : firma.vergiDairesi
            },
            tcknKullan: firma.efaturaTCKNKullan == undefined ? false : firma.efaturaTCKNKullan,
            //duzenlemeTarihi : moment().format('DD-MM-YYYY'), // '',
            duzenlemeTarihi: moment(fatura.duzenlemeTarihi).format('YYYY-MM-DD'), // '',
            uuid: uuid(), // '44828186-d039-4f7e-b23b-792a2db5b52b',
            no: '',
            kdv: fatura.toplamKdv,//'180',
            araToplam: fatura.araToplam, //'1000',
            //vergiMatrahi    : fatura.vergiMatrahi, //'180',
            genelToplam: fatura.genelToplam, //'1180',
            kalemler: kalemler.map(k => {
                return {
                    adet: k.miktar,
                    birimFiyat: k.birimFiyat,
                    toplam: k.toplam,
                    adi: `${k.urun ? k.urun.adi : ''} ${k.aciklama}`,
                    kdvOran: k.kdvOran
                }
            }),
            faturaTuru: fatura.faturaTuru,
            iadeFaturaNo: fatura.iadeFaturaNo,
            iadeFaturaTarih: moment(fatura.iadeFaturaTarih).format('YYYY-MM-DD'),
            aciklama: fatura.aciklama
        }
        let eFaturaResp = await AmqpService.sendFatura(_fatura)
        */
        return eFaturaResp
    }
    catch (e) {
        console.log(e)
    }
}

exports.tahsilatYap = async (tahsilat) => {
    try {
        let vfirma = tahsilat.vfirma
        let tahsilatTutari = Math.floor(parseFloat(tahsilat.tutar) * 100) / 100

        // 1. Firmanin acik faturalari bulunur. Zama sirali
        let faturalar = await Fatura.find({ acik: true, vfirma: vfirma }, 'genelToplam tahsilatTutari acik vadeTarihi').sort('vadeTarihi').lean(true).exec()

        // 2. Tahsilat tutari bitene kadar faturalarin acik kalan tutari kadar dusulur. Faturada tahsilatTutari set edilir. Yapilan odeme fatura tutari kadar ise acik: false yapilir
        for (let fatura of faturalar) {
            if (tahsilatTutari <= 0) {
                break
            }
            let tahsilEdilecekTutar = Math.floor((fatura.genelToplam - fatura.tahsilatTutari) * 100) / 100
            if (tahsilatTutari < tahsilEdilecekTutar) {
                let t = Math.floor((tahsilatTutari + fatura.tahsilatTutari) * 100) / 100
                let er = await Fatura.findByIdAndUpdate(fatura._id, {
                    acik: true,
                    tahsilatTutari: t
                }, {
                    new: true
                }).exec()
                console.log(er)
            }
            else {
                let t = Math.floor((tahsilEdilecekTutar + fatura.tahsilatTutari) * 100) / 100
                let er = await Fatura.findByIdAndUpdate(fatura._id, {
                    acik: false,
                    tahsilatTutari: t
                }, {
                    new: true
                }).exec()
                //console.log(er)
            }
            tahsilatTutari -= tahsilEdilecekTutar
        }

        // 3. Yapilan tahsilattan kalan varsa musteri bakiyesine yazilir
        if (tahsilatTutari > 0) {
            let firma = await Firma.findOne({ key: vfirma }).lean(true).exec()
            if (firma) {
                await Firma.findOneAndUpdate({ key: vfirma }, { bakiye: firma.bakiye + tahsilatTutari }).exec()
            }
        }
    }
    catch (e) {
        console.log(e)
    }
}

exports.yenidenGonder = async (fatura) => {
    let kalemler = await this.getKalemler(fatura._id)
    console.log(kalemler)
    this.sendEFatura(fatura, kalemler.data)
}