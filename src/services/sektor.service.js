const MongooseQueryParser = require('mongoose-query-parser')
const Sektor = require('../models/sektor.model')
const Firma = require('../models/firma.model')
const UserService = require('./user.service')
const SocketService = require('../services/socket.service')
const AmqpProvider = require('../providers/amqp.provider')
const moment=require('moment')

exports.find = async (query) => {
    try {
        let filter = query || {}
        let parser = new MongooseQueryParser.MongooseQueryParser({})
        let f = parser.parse(filter)
        let sektorler = await Sektor
            .find(f.filter)
            .select(f.select)
            .skip(f.skip)
            .limit(f.limit)
            .populate(f.populate)
            .sort(f.sort)
            .exec();
        if (sektorler) {
            let count = await Sektor.countDocuments(f.filter).exec()
            return {
                data: sektorler,
                page: f.skip,
                total: count
            }
        }
    }
    catch(e) {
        console.log(e)
    }
}

exports.findOne = async (id, query) => {
    let filter = query || {}
    let parser = new MongooseQueryParser.MongooseQueryParser({})
    let f = parser.parse(filter)
    let sektor = await Sektor
    .find({_id: id, ...f.filter})
    .select(f.select)
    .populate(f.populate)
    .exec();
    if (sektor && sektor.length == 1) {
        return sektor[0]
    }
    throw 'Sektor Bulunamadi'
}

exports.create = async (sektor, user) => {
    let newSektor = await new Sektor(sektor).save()
    return newSektor
}

exports.update = async (id, sektorUpdateDto, user) => {
    Sektor.findByIdAndUpdate(id, sektorUpdateDto, (err, doc) => {
        if (!err) {
            return doc
        }
    })
}

exports.remove = async (id) => {
    let deleted = await Sektor.findByIdAndDelete(id)
    return deleted
}
