const MongooseQueryParser = require('mongoose-query-parser')
const Ekip = require('../models/ekip.model')
const moment=require('moment')
const _ = require('lodash')

exports.find = async (query) => {
    try {
        let filter = query || {}
        let parser = new MongooseQueryParser.MongooseQueryParser({})
        let f = parser.parse(filter)
        let ekipler = await Ekip
            .find(f.filter)
            .select(f.select)
            .skip(f.skip)
            .limit(f.limit)
            .populate(f.populate)
            .sort(f.sort)
            .exec();
        if (ekipler) {
            let count = await Ekip.countDocuments(f.filter).exec()
            return {
                data: ekipler,
                page: f.skip,
                total: count
            }
        }
    }
    catch(e) {
        console.log(e)
    }
}

exports.findOne = async (id, query) => {
    let filter = query || {}
    let parser = new MongooseQueryParser.MongooseQueryParser({})
    let f = parser.parse(filter)
    let ekip = await Ekip
    .find({_id: id, ...f.filter})
    .select(f.select)
    .populate(f.populate)
    .exec();
    if (ekip && ekip.length == 1) {
        return ekip[0]
    }
    throw 'Ekip Bulunamadi'
}

exports.create = async (ekip, user) => {
    let newEkip = await new Ekip(ekip).save()
    return newEkip
}

exports.update = async (id, ekipUpdateDto, user) => {
    Ekip.findByIdAndUpdate(id, ekipUpdateDto, (err, doc) => {
        if (!err) {
            return doc
        }
    })
}

exports.remove = async (id) => {
    let deleted = await Ekip.findByIdAndDelete(id)
    return deleted
}

const getAllChildren = (ekipler, user) => {
    let result = []
    let yoneticiOlduguEkipler = ekipler.filter(e => e.vyonetici == user)
    if (yoneticiOlduguEkipler.length == 0) {
        return null
    }
    for (let ekip of yoneticiOlduguEkipler) {
        result = _.concat(result, ekip.vuyeler)
        for (let user of ekip.vuyeler) {
            let r = getAllChildren(ekipler, user)
            if (r) {
                result = _.concat(result, r)
            }
        }
    } 
    return result
}

exports.getEkibim = async (user) => {

    let yoneticisiOldugumEkipler    = await Ekip.find({vyonetici: user.key, active:true}).exec()
    let dahilOldugumEkipler         = await Ekip.find({vuyeler: user.key, active: true}).exec()
    let tumEkipler                  = await Ekip.find({active:true}).exec()

    let result = _.uniq(getAllChildren(tumEkipler, user.key))

//    if (yoneticisiOldugumEkipler.length > 0) {
//        for (let ekip of yoneticisiOldugumEkipler) {
//            for (let user of ekip.vuyeler) {
//                getAllChildren(tumEkipler, )
//
//            }
//        }
//    }
//
//    for (let ekip of dahilOldugumEkipler) {
//        _.concat(result, ekip.vuyeler)
//    }
    return result
}
