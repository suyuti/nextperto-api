const MongooseQueryParser = require('mongoose-query-parser')
const ToplantiKarar = require('../models/toplantikarar.model')
const Toplanti = require('../models/toplanti.model')
const moment = require('moment')

exports.find = async (query) => {
    let filter = query || {}
    let parser = new MongooseQueryParser.MongooseQueryParser({})
    let f = parser.parse(filter)
    let toplantilar = await ToplantiKarar
        .find(f.filter)
        .select(f.select)
        .skip(f.skip)
        .limit(f.limit)
        .populate(f.populate)
        .exec();
    if (toplantilar) {
        let count = await ToplantiKarar.countDocuments(f.filter).exec()
        return {
            data: toplantilar,
            page: f.skip,
            total: count
        }
    }
}

exports.findOne = async (id) => {
    let toplanti = await ToplantiKarar.findById(id).exec()
    return toplanti
}

exports.create = async (toplanti) => {
    try {
        let newToplanti = await new ToplantiKarar(toplanti).save()
        return newToplanti
    }
    catch(e) {
        console.log(e)
    }
}

exports.update = async (id, toplantiUpdateDto) => {
    try {
        //toplantiUpdateDto.tarih = toplantiUpdateDto.tarih.toDate()
        let toplanti = await ToplantiKarar.findByIdAndUpdate(id, toplantiUpdateDto, {new:true}).exec()
        return toplanti
    }
    catch(e) {
        console.log(e)
    }
}

exports.remove = async (id) => {
    let deleted = await ToplantiKarar.findByIdAndDelete(id)
    return deleted
}

exports.addOrCreateKararlar = async (toplantiId, kararlar) => {
    let toplanti = await Toplanti.findById(toplantiId).exec()
    let results = []
    for (let karar of kararlar) {
        if (!karar._id) {
            karar.tarih = moment(karar.tarih, 'DD.MM.YYYY')
            let newKarar = await this.create({...karar, vToplanti: toplanti.key})
            results.push(newKarar)
        }
        else {
            karar.tarih = moment(karar.tarih, 'DD.MM.YYYY')
            let updatedKarar = await this.update(karar._id, karar)
            results.push(updatedKarar)
        }
    }
    return results
}
