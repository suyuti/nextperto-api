const MongooseQueryParser = require('mongoose-query-parser')
const diffHistory = require('mongoose-diff-history/diffHistory');
const GelenFatura = require('../models/gelenfatura.model')
const FaturaKalem = require('../models/faturakalem.model')
const Firma = require('../models/firma.model')
const FaturaKalemService = require('./faturakalem.service');
const mongoose = require('mongoose');
const AmqpService = require('../providers/amqp.provider')
const TenantService = require('../services/tenant.service')
const FirmaService = require('../services/firma.service');
const moment = require('moment');
const { uuid } = require('uuidv4');

exports.find = async (query) => {
    try {
        let filter = query || {}
        let parser = new MongooseQueryParser.MongooseQueryParser({})
        let f = parser.parse(filter)
        let faturalar = await GelenFatura
            .find(f.filter)
            .select(f.select)
            .skip(f.skip)
            .limit(f.limit)
            .populate(f.populate)
            .sort(f.sort)
            .exec();
        if (faturalar) {
            let count = await GelenFatura.countDocuments(f.filter).exec()
            return {
                data: faturalar,
                page: f.skip,
                total: count
            }
        }
    }
    catch (e) {
        console.log(e)
    }
}

exports.findOne = async (id, query) => {
    let filter = query || {}
    let parser = new MongooseQueryParser.MongooseQueryParser({})
    let f = parser.parse(filter)
    let fatura = await GelenFatura
    .find({_id: id, ...f.filter})
    .select(f.select)
    .populate(f.populate)
    .exec();
    if (fatura && fatura.length == 1) {
        return fatura[0]
    }
    throw 'Gelen Fatura Bulunamadi'
}

exports.create = async (fatura) => {
    try {
        //let newFatura = await new Fatura(fatura).save()
        //for (let kalem of fatura.kalemler) {
        //    this.addKalem(newFatura.key, kalem)
        //}
        //if (!newFatura.taslak) {
        //    // Taslak degilse ve Efatura ise EFatura islemi yapilir
        //    await this.sendEFatura(newFatura, fatura.kalemler)
        //}
        //    return newFatura
    }
    catch(e) {
        console.log(e)
    }
}

exports.update = async (id, faturaUpdateDto, user) => {
    GelenFatura.findByIdAndUpdate(id, faturaUpdateDto, {
        new: true,
        __user: user._id,
        __reason: "updated"
    }, (err, doc) => {
        if (!err) {
            return doc
        }
    })
}

exports.remove = async (id) => {
    let deleted = await GelenFatura.findByIdAndDelete(id)
    return deleted
}

