const io = require('socket.io')

var ioInstance
const onlineUsers = {}

const onLogin = (socket, user) => {
    // user var mi kontrol
    onlineUsers[socket.id] = {
        userId: user.userId,
        socket: socket
    }
}

const onDisconnect = (socket) => {
    delete onlineUsers[socket.id]
}

exports.initialize = (server) => {
    ioInstance = io(server, {
        cors: {
            origin: 'http://localhost:3000',
            methods: ['GET', 'POST']
        }
    })
    ioInstance.on('connection', socket => {
        socket.emit('welcome')
        socket.on('login', data => {onLogin(socket, data)})
        socket.on('disconnect', () => {onDisconnect(socket)})
    })

}

exports.send = (userId, message) => {
    if (!userId) {
        // broadcast
        //ioInstance.emit('message', message)
    }
    else {
        let onlineUserCLients = Object.keys(onlineUsers).filter(key => onlineUsers[key].userId == userId)
        if (onlineUserCLients.length > 0) {
            for(let client of onlineUserCLients) {
                onlineUsers[client].socket.emit('message', message)
            }
        }
    }
}
