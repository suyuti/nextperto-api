const MongooseQueryParser = require('mongoose-query-parser')
const Kisi = require('../models/kisi.model')

exports.find = async (query) => {
    let filter = query || {}
    let parser = new MongooseQueryParser.MongooseQueryParser({})
    let f = parser.parse(filter)
    let kisiler = await Kisi
        .find(f.filter)
        .select(f.select)
        .skip(f.skip)
        .limit(f.limit)
        .populate(f.populate)
        .exec();
    if (kisiler) {
        let count = await Kisi.countDocuments(f.filter).exec()
        return {
            data: kisiler,
            page: f.skip,
            total: count
        }
    }
}

exports.findOne = async (id, query) => {
    let filter = query || {}
    let parser = new MongooseQueryParser.MongooseQueryParser({})
    let f = parser.parse(filter)
    let kisi = await Kisi
    .find({_id: id, ...f.filter})
    .select(f.select)
    .populate(f.populate)
    .exec();
    if (kisi && kisi.length == 1) {
        return kisi[0]
    }
    throw 'Kisi Bulunamadi'
}

exports.create = async (kisi) => {
    let newKisi = await new Kisi(kisi).save()
    return newKisi
}

exports.update = async (id, kisiUpdateDto) => {
    let kisi = await Kisi.findByIdAndUpdate(id, kisiUpdateDto, {new:true}).exec()
    return kisi
}

exports.remove = async (id) => {
    let deleted = await Kisi.findByIdAndDelete(id)
    return deleted
}
