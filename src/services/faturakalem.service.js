const MongooseQueryParser = require('mongoose-query-parser')
const diffHistory = require('mongoose-diff-history/diffHistory');
const FaturaKalem = require('../models/faturakalem.model')
const Firma = require('../models/firma.model')

exports.find = async (query) => {
    try {
        let filter = query || {}
        let parser = new MongooseQueryParser.MongooseQueryParser({})
        let f = parser.parse(filter)
        let faturaKalemler = await FaturaKalem
            .find(f.filter)
            .select(f.select)
            .skip(f.skip)
            .limit(f.limit)
            .populate(f.populate)
            .exec();
        if (faturaKalemler) {
            let count = await FaturaKalem.countDocuments(f.filter).exec()
            return {
                data: faturaKalemler,
                page: f.skip,
                total: count
            }
        }
    }
    catch(e) {
        console.log(e)
    }
}

exports.findOne = async (id) => {
    let faturaKalem = await FaturaKalem.findById(id).exec()
    return faturaKalem
}

exports.create = async (faturaKalem) => {
    let newFaturaKalem = await new FaturaKalem(faturaKalem).save()
    return newFaturaKalem
}

exports.update = async (id, faturaKalemUpdateDto, user) => {
    FaturaKalem.findByIdAndUpdate(id, faturaKalemUpdateDto, (err, doc) => {
        if (!err) {
            return doc
        }
    })
}

exports.remove = async (id) => {
    let deleted = await FaturaKalem.findByIdAndDelete(id)
    return deleted
}
