const MongooseQueryParser = require('mongoose-query-parser')
const diffHistory = require('mongoose-diff-history/diffHistory');
const Gorev = require('../models/gorev.model')
const Firma = require('../models/firma.model')
const OnayModel = require('../models/onay.model')
const UserModel = require('../models/user.model')
const ToplantiKararModel = require('../models/toplantikarar.model')
const UserService = require('./user.service')
const SocketService = require('../services/socket.service')
const AmqpProvider = require('../providers/amqp.provider')
const moment = require('moment')
const YorumModel = require('../models/yorum.model');
const { addHistory, addHistory2 } = require('../helpers/history.helper');
const OnayService = require('../services/onay.service');
const { difference } = require('../helpers/diff.helper');

exports.find = async (query) => {
    try {
        let filter = query || {}
        let parser = new MongooseQueryParser.MongooseQueryParser({})
        let f = parser.parse(filter)
        let gorevler = await Gorev
            .find(f.filter)
            .select(f.select)
            .skip(f.skip)
            .limit(f.limit)
            .populate(f.populate)
            .sort(f.sort)
            .exec();
        if (gorevler) {
            let count = await Gorev.countDocuments(f.filter).exec()
            return {
                data: gorevler,
                page: f.skip,
                total: count
            }
        }
    }
    catch (e) {
        console.log(e)
    }
}

exports.findOne = async (id, query) => {
    let filter = query || {}
    let parser = new MongooseQueryParser.MongooseQueryParser({})
    let f = parser.parse(filter)
    let gorev = await Gorev
        .findOne({ _id: id, ...f.filter })
        .select(f.select)
        .populate(f.populate)
        .lean(true)
        .exec();
    if (gorev) {
        return gorev
    }
    throw 'Gorev Bulunamadi'
}

exports.create = async (gorev, user) => {
    let gorevDto = gorev
    gorevDto.createdAt = new Date()
    gorevDto.vcreatedBy = user.key
    gorevDto.updatedAt = null
    gorevDto.updatedBy = null

    let newGorev = await new Gorev(gorevDto).save()
    addHistory2(null, newGorev._id, 'Görev oluşturuldu', user, 'Gorev')
    SocketService.send(newGorev.vsorumlu, {
        type: 'gorev.create',
        message: 'Yeni görev oluşturuldu',
        gorevId: newGorev._id,
        baslik: newGorev.baslik,
        sonTarih: newGorev.sonTarih,
        olusturan: newGorev.createdBy,
        oncelik: newGorev.oncelik
    })

    await sendCreateMail(newGorev, user)

    return newGorev
}

exports.update = async (id, gorevUpdateDto, user) => {
    let gorevDto = gorevUpdateDto
    gorevDto.updatedAt = moment().toISOString()
    gorevDto.updatedBy = user._id

    let prevState = await Gorev.findById(id).lean(true).exec()

    // Eger onay gereken bir islemse onay kaydi olusturulur.
    let onayReq = await isOnayRequired(prevState, gorevUpdateDto)
    if (onayReq) {
        let yoneticisi = await UserService.getYoneticisi(user)
        let onay = await OnayService.create({
            onayIsteyen     : user._id,
            onayIstenen     : yoneticisi._id,
            onaylayan       : null,
            refCollection   : 'Gorev',
            referenceItem   : id,
            istemeTarihi    : new Date(),
            onayTarihi      : null,
            onayDurumu      : 'acik',
            onayAciklamasi  : '',
            nextState       : gorevUpdateDto,
            diff            : onayReq
        })

        // Gorev icin onay bekleniyor yapilir
        await Gorev.findByIdAndUpdate(id, { onayBekleniyor: true }).exec()

        await addHistory2(onayReq, prevState._id, 'Görev değişikliği için onay istendi', user, 'Gorev')
        
        // Onay istenene notification mesaj ilet
        SocketService.send(yoneticisi.key, {
            type: 'onay.create',
            message: 'Değişiklik onayı',

            onayId: onay._id,

            //gorevId: newGorev._id,
            //baslik: newGorev.baslik,
            //sonTarih: newGorev.sonTarih,
            //olusturan: newGorev.createdBy,
            //oncelik: newGorev.oncelik
        })

        await OnayService.sendOnayMail(onay, yoneticisi, user)

        return prevState
    }
    else {
        let response = await Gorev.findByIdAndUpdate(id, gorevDto, {
            new: true,
            lean: true,
        })

        let _prevState = { ...prevState }
        let _nextState = { ...response }
        delete _nextState.updatedAt
        delete _nextState.updatedBy

        await addHistory(_prevState, _nextState, id, 'Görev güncellendi', user, 'Gorev')
        return response
    }





    //if (gorevUpdateDto.sonuc == 'basarili') {
    //    if (response.vtoplantiKarar) {
    //        await ToplantiKararModel.findOneAndUpdate({ key: response.vtoplantiKarar }, { durum: 'Tamamlandı' }, { new: true, lean: true }).exec()
    //    }
    //}
}

exports.remove = async (id) => {
    let deleted = await Gorev.findByIdAndDelete(id)
    return deleted
}

exports.getHistory = async (id) => {
    //let histories = await diffHistory.getHistories('Gorev', id)
    let histories = await diffHistory.getDiffs('Gorev', id)
    // console.log(histories)
    return histories
}

const sendCreateMail = async (gorev, user) => {
    let to = await UserService.findOne({ key: gorev.vsorumlu })

    let content = `
    <table style="width: 90%;">
    <tbody>
    <tr>
    <td style="width: 445px;" colspan="2">Sayın ${to.username},</td>
    </tr>
    <tr>
    <td style="width: 445px;" colspan="2">
    <p>&nbsp;</p>
    <p>Aşağıda bilgileri bulunan görev sistem üzerinden size atanmıştır.&nbsp;</p>
    </td>
    </tr>
    <tr>
    <td style="width: 445px;" colspan="2"><hr />&nbsp;</td>
    </tr>
    <tr>
    <td style="width: 157px; vertical-align: top;"><strong>Görev adı</strong></td>
    <td style="width: 288px;">${gorev.baslik}</td>
    </tr>
    <tr>
    <td style="width: 157px; vertical-align: top;"><strong>Görev Önceliği</strong></td>
    <td style="width: 288px;">${gorev.oncelik}</td>
    </tr>
    <tr>
    <td style="width: 157px; vertical-align: top;"><strong>Açıklama</strong></td>
    <td style="width: 288px;">${gorev.aciklama}</td>
    </tr>
    <tr>
    <td style="width: 157px; vertical-align: top;"><strong>Son Tarih</strong></td>
    <td style="width: 288px;">${moment(gorev.sonTarih).format("DD.MM.yyyy")}</td>
    </tr>
    <tr>
    <td style="width: 157px; vertical-align: top;"><strong>İlgili Müşteri</strong></td>
    <td style="width: 288px;">${gorev.icGorevMi ? "EXPERTO İç Görev" : ''
        }</td>
    </tr>
    <tr>
    <td style="width: 157px; vertical-align: top;"><strong>Görev Kategorisi</strong></td>
    <td style="width: 288px;">${gorev.kategori}</td>
    </tr>
    <tr>
    <td style="width: 157px; vertical-align: top;"><strong>Görev Oluşturan</strong></td>
    <td style="width: 288px;">${user.username}</td>
    </tr>
    <tr>
    <td style="width: 157px; vertical-align: top;"><strong>Görev Oluşturma Tarihi</strong></td>
    <td style="width: 288px;">${moment(gorev.createdAt).format("DD.MM.yyyy HH:mm")}</td>
    </tr>
    </tbody>
    </table>
    <p>&nbsp;</p>
    <p><em>Bu mesaj otomatik olarak gönderilmiştir.</em></p>
`

    await AmqpProvider.sendMail({
        from: user.oid,
        to: [to.email],
        subject: `[EXPERTO-APP] Yeni Görev Oluşturuldu`,
        content: content
    })
}

exports.getYorumlar = async (gorev, user) => {

    let yorumlar = await YorumModel.find({ referans: gorev }).sort('tarih').exec()

    // user gorevin sorumlusu ise bu yorumlar okundu yapilir
    let _gorev = await Gorev.findById(gorev).lean(true).exec()
    if (_gorev.vsorumlu == user.key) {
        for (let yorum of yorumlar) {
            if (!yorum.okundu) {
                let okumaTarihi = new Date()
                await YorumModel.findByIdAndUpdate(yorum._id, {
                    okundu: true,
                    okunmaTarihi: okumaTarihi,
                    okuyan: user.key
                })
                yorum.okundu = true,
                    yorum.okunmaTarihi = okumaTarihi,
                    yorum.okuyan = user.key
            }
        }
    }

    return yorumlar
}

exports.addYorum = async (yorum, user) => {
    let gorev = await Gorev.findOne({ _id: yorum.referans }).exec()
    let _yorum = await new YorumModel(yorum).save()

    await addHistory(null, null, gorev._id, 'Yorum eklendi', user, 'Gorev')


    SocketService.send(gorev.vsorumlu, {
        type: 'yorum.create',
        message: 'Yeni yorum',
        gorevId: gorev._id,
        baslik: _yorum.yorum,
        sonTarih: gorev.sonTarih,
        olusturan: gorev.createdBy,
        oncelik: gorev.oncelik
    })

    return _yorum
}

exports.tamamla = async (id, tamamlamaDto, user) => {
    let gorev = await Gorev.findById(id).lean(true).exec()

    let today = moment().startOf('day')
    let sonTarih = moment(gorev.sonTarih).startOf('day')
    let gecikmeGun = 0
    let kapatan = user.key

    if (today > sonTarih) {
        gecikmeGun = today.diff(sonTarih, 'days')
    }

    let gorevUpdateDto = {
        gecikmeGun: gecikmeGun,
        vkapatan: kapatan,
        sonuc: tamamlamaDto.sonuc,
        sonucAciklama: tamamlamaDto.sonucAciklama,
        kapatmaTarihi: new Date()
    }

    let toplantiKararDurum = ''
    if (gorev.vtoplantiKarar) {
        // Eger toplanti kararindan olusturulmussa
        if (gorevUpdateDto.sonuc == 'basarili') {
            toplantiKararDurum = 'Tamamlandi'
        }
        else if (gorevUpdateDto.sonuc == 'iptal') {
            toplantiKararDurum = 'İptal'
        }
        else if (gorevUpdateDto.sonuc == 'basarisiz') {
            toplantiKararDurum = 'Başarısız'
        }
    }

    if (gorev.onayBekleniyor) {
        // Eger bu gorev icin beklenen bir onay varsa Onay iptal edilir. Gorev kapatildigi icin artik onaya ihtiyac yok
        gorevUpdateDto.onayBekleniyor = false
        await addHistory2(null, gorev._id, 'Görev değişiklik isteği iptal edildi', user, 'Gorev')
        await OnayModel.findOneAndUpdate({referenceItem: id, refCollection: 'Gorev'}, {onayDurumu: 'iptal', onayTarihi: new Date(), onaylayan: user._id}).exec()
    }

    let response = await Gorev.findByIdAndUpdate(id, gorevUpdateDto, {
        new: true,
        lean: true,
        //__user: user._id,
        //__reason: "updated"
    })

    let _prev = { ...gorev }
    let _next = { ...response }
    await addHistory(_prev, _next, gorev._id, 'Görev tamamlandı', user, 'Gorev')

    if (gorev.vtoplantiKarar) {
        // Toplanti kararini da guncelle
        await ToplantiKararModel.findOneAndUpdate({ key: response.vtoplantiKarar }, { durum: toplantiKararDurum }, { new: true, lean: true }).exec()
    }

    return response
}

const isOnayRequired = async (prevState, next) => {
    return null
    // son Tarih
    // sorumlu
    let ret = []
    if (!moment(prevState.sonTarih).isSame(next.sonTarih)) {
        ret.push({
            field: 'Görev Son Tarih',
            prev: moment(prevState.sonTarih).format('DD.MM.YYYY'),
            next: moment(next.sonTarih).format('DD.MM.YYYY')
        })
    }
    if (prevState.vsorumlu != next.vsorumlu) {
        let pUser = await UserModel.findOne({key: prevState.vsorumlu}).lean(true).exec()
        let nUser = await UserModel.findOne({key: next.vsorumlu}).lean(true).exec()
        ret.push({
            field: 'Sorumlu',
            prev: pUser ? pUser.username : '',
            next: nUser ? nUser.username : ''
        })
    }
    if (ret.length > 0) {
        return ret
    }
    else {
        return null
    }
}