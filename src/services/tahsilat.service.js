const MongooseQueryParser = require('mongoose-query-parser')
const diffHistory = require('mongoose-diff-history/diffHistory');
const Tahsilat = require('../models/tahsilat.model')
const Firma = require('../models/firma.model');
const { uuid } = require('uuidv4');

exports.find = async (query) => {
    try {
        let filter = query || {}
        let parser = new MongooseQueryParser.MongooseQueryParser({})
        let f = parser.parse(filter)
        let tahsilatlar = await Tahsilat
            .find(f.filter)
            .select(f.select)
            .skip(f.skip)
            .limit(f.limit)
            .populate(f.populate)
            .sort(f.sort)
            .exec();
        if (tahsilatlar) {
            let count = await Tahsilat.countDocuments(f.filter).exec()
            return {
                data: tahsilatlar,
                page: f.skip,
                total: count
            }
        }
    }
    catch (e) {
        console.log(e)
    }
}

exports.findOne = async (id) => {
    let tahsilat = await Tahsilat.findById(id).exec()
    return tahsilat
}

exports.create = async (tahsilat, user) => {
    tahsilat.key = uuid()
    tahsilat.vcreatedBy = user.key
    tahsilat.createdAt=new Date()
    let newTahsilat = await new Tahsilat(tahsilat).save()
    return newTahsilat
}

exports.update = async (id, tahsilatUpdateDto, user) => {
    Tahsilat.findByIdAndUpdate(id, tahsilatUpdateDto, {
        new: true,
        __user: user._id,
        __reason: "updated"
    }, (err, doc) => {
        if (!err) {
            return doc
        }
    })
}

exports.remove = async (id) => {
    let deleted = await Tahsilat.findByIdAndDelete(id)
    return deleted
}

exports.getHistory = async (id) => {
    let histories = await diffHistory.getDiffs('Tahsilat', id)
    return histories
}