const MongooseQueryParser = require('mongoose-query-parser')
const Tenant = require('../models/tenant.model')
const FaturaModel = require('../models/fatura.model')
const TahsilatModel = require('../models/tahsilat.model')
const PlanlananTahsilatModel = require('../models/planlanmistahsilat.model')
const moment = require('moment')
moment.locale('tr')

exports.find = async (query) => {
    try {
        let filter = query || {}
        let parser = new MongooseQueryParser.MongooseQueryParser({})
        let f = parser.parse(filter)
        let tenantler = await Tenant
            .find(f.filter)
            .select(f.select)
            .skip(f.skip)
            .limit(f.limit)
            .populate(f.populate)
            .exec();
        if (tenantler) {
            let count = await Tenant.countDocuments(f.filter).exec()
            return {
                data: tenantler,
                page: f.skip,
                total: count
            }
        }
    }
    catch (e) {
        console.log(e)
    }
}

exports.findOne = async (id, query) => {
    let filter = query || {}
    let parser = new MongooseQueryParser.MongooseQueryParser({})
    let f = parser.parse(filter)
    let tenant = await Tenant
        .find({ _id: id, ...f.filter })
        .select(f.select)
        .populate(f.populate)
        .exec();
    if (tenant && tenant.length == 1) {
        return tenant[0]
    }
    throw 'Tenant Bulunamadi'
}

exports.create = async (tenant) => {
    let newTenant = await new Tenant(tenant).save()
    return newTenant
}

exports.update = async (id, tenantUpdateDto) => {
    //let tenant = await 
    Tenant.findByIdAndUpdate(id, tenantUpdateDto, { new: true }, (err, doc) => {
        if (!err) {
            return doc
        }
    })
    //return tenant
}

exports.remove = async (id) => {
    let deleted = await Tenant.findByIdAndDelete(id)
    return deleted
}


exports.nakitAkisi = async (id) => {
    let count = 6
    let tenant = await Tenant.findById(id).lean(true).exec()
    //let faturalar = await FaturaModel.find({vkesenFirma: tenant.key, acik: true}).lean().exec()
    let toplamAlacak = 0
    //for (let fatura of faturalar) {
    //    toplamAlacak += (fatura.genelToplam - fatura.tahsilatTutari)
    //}




    let alacaklar = []
    let odemeler = []
    let netler = []

    faturalar = await FaturaModel.find({ vkesenFirma: tenant.key, acik: true }, 'genelToplam tahsilatTutari vadeTarihi').sort('-vadeTarihi').exec()
    let weeks = []
    while (count) {
        let start = moment().add(count, 'weeks').startOf('isoWeek')
        let end = moment().add(count, 'weeks').endOf('isoWeek')
        weeks.push({ start, end })
        count--
    }

    weeks = weeks.reverse()

    for (let week of weeks) {
        let f = faturalar.filter(f => moment(f.vadeTarihi).isBetween(week.start, week.end, undefined, '[]'))
        let toplam = f.reduce((acc, v) => { return acc + (Math.round((v.genelToplam - v.tahsilatTutari) * 100) / 100) }, 0)
        alacaklar.push({ toplam, adet: f.length })
        toplamAlacak += toplam
        netler.push(toplam)
    }


    return {
        toplamAlacak: toplamAlacak,
        toplamOdeme: 0,
        alacaklar: alacaklar,
        netler: netler
    }
}

exports.toplamlar = async (tenant) => {
    try {
        let toplamAlacaklar = 0
        let alacak = await FaturaModel.aggregate([
            {
                $match: {
                    acik: { $eq: true },
                    vkesenFirma: { $eq: tenant.key }
                }
            },
            {
                $group: {
                    _id: null,
                    toplam: { $sum: { $subtract: ['$genelToplam', '$tahsilatTutari'] } }
                }
            }
        ]).exec()
        //console.log(alacak)

        let gecikenAlacaklar = 0
        let geciken = await FaturaModel.aggregate([
            {
                $match: {
                    acik: { $eq: true },
                    vadeTarihi: { $lt: new Date() },
                    vkesenFirma: { $eq: tenant.key }
                }
            },
            {
                $group: {
                    _id: null,
                    toplam: { $sum: { $subtract: ['$genelToplam', '$tahsilatTutari'] } }
                }
            }
        ]).exec()
        //console.log(geciken)

        let planlananAlacaklar = 0
        let planlanan = await PlanlananTahsilatModel.aggregate([
            {
                $match: {
                    durum: { $eq: 'acik' },
                    vtahsilatYapanFirma: { $eq: tenant.key }
                }
            },
            {
                $group: {
                    _id: null,
                    toplam: { $sum: '$tutar' }
                }
            }
        ]).exec()
        //console.log(geciken)
        let toplamOdemeler = 0

        return {
            toplamAlacaklar: alacak.length > 0 ? alacak[0].toplam : 0,
            gecikenAlacaklar: geciken.length > 0 ? geciken[0].toplam : 0,
            planlananAlacaklar: planlanan.length > 0 ? planlanan[0].toplam : 0,
            toplamOdemeler
        }
    }
    catch (e) {
        console.log(e)
    }
}


exports.gerceklesenTahsilatlat = async (tenant) => {
    try {
        let data = []
        let toplam = 0
        let adet = 0

        if (!tenant) {
            return { data, toplam, adet }
        }

        let _tenant = await Tenant.findById(tenant)

        let tahsilatlar = await TahsilatModel.find({ vtahsilatYapanFirma: _tenant.key }).sort('tahsilatTarihi').exec()
        if (tahsilatlar.length == 0) {
            return { data, toplam, adet }
        }

        let timeSlots = []
        let count = 12

        while (count) {
            let start = moment().add(-count, 'weeks').startOf('isoWeek')
            let end = moment().add(-count, 'weeks').endOf('isoWeek')
            timeSlots.push({ start, end })
            count--
            data.push({
                start: start,
                end: end,
                label: `${moment(start).format('DD MMM')}-${moment(end).format('DD MMM')}`,
                value: 0
            })
        }
        let today = moment()//.add(-1, 'day')
        let thisWeekStart = moment().startOf('isoWeek')
        timeSlots.push({ start: thisWeekStart, end: today })
        data.push({
            start: thisWeekStart,
            end: today,
            label: `${moment(thisWeekStart).format('DD MMM')}-${moment(today).format('DD MMM')}`,
            value: 0
        })

        //timeSlots = timeSlots.reverse()
        //console.log(timeSlots.map(t => `${moment(t.start).format('DD.MM.YYYY')} - ${moment(t.end).format('DD.MM.YYYY')}`))

        for (let tahsilat of tahsilatlar) {
            let i = 0
            for (let timeSlot of timeSlots) {
                if (moment(tahsilat.tahsilatTarihi).isBetween(timeSlot.start, timeSlot.end)) {
                    data[i].value = data[i].value + tahsilat.tutar
                    adet++
                    toplam += tahsilat.tutar
                    break
                }
                i++
            }
        }
        return { data, toplam, adet }
    }
    catch (e) {
        console.log(e)
    }
}

exports.gecikenTahsilatlar = async (tenant) => {
    let data = []
    let toplam = 0
    let adet = 0

    if (!tenant) {
        return { data, toplam, adet }
    }
    let _tenant = await Tenant.findById(tenant)

    // Kesilmis faturalardan toplanir
    let faturalar = await FaturaModel.find({ acik: true, vkesenFirma: _tenant.key }).sort('-vadeTarihi').exec()

    let timeSlots = []
    let count = 12


    while (count) {
        let start = moment().add(-count, 'weeks').startOf('isoWeek')
        let end = moment().add(-count, 'weeks').endOf('isoWeek')
        timeSlots.push({ start, end })
        count--
        data.push({
            start: start,
            end: end,
            label: `${moment(start).format('DD MMM')}-${moment(end).format('DD MMM')}`,
            value: 0
        })
    }
    let yesterday = moment().add(-1, 'day')
    let thisWeekStart = moment().startOf('isoWeek')
    timeSlots.push({ start: thisWeekStart, end: yesterday })
    data.push({
        start: yesterday,
        end: thisWeekStart,
        label: `${moment(yesterday).format('DD MMM')}-${moment(thisWeekStart).format('DD MMM')}`,
        value: 0
    })

    //    timeSlots = timeSlots.reverse()


    for (let fatura of faturalar) {
        if (moment(fatura.vadeTarihi).isAfter(moment())) {
            continue
        }

        let i = 0
        for (let timeSlot of timeSlots) {
            if (moment(fatura.vadeTarihi).isBetween(timeSlot.start, timeSlot.end)) {
                data[i].value = data[i].value + (fatura.genelToplam - fatura.tahsilatTutari)
                toplam += (fatura.genelToplam - fatura.tahsilatTutari)
                adet++
                break
            }
            i++
        }
    }

    return { data, toplam, adet }
}

exports.planlananTahsilatlar = async (tenant) => {
    let data = []
    let toplam = 0
    let adet = 0
    if (!tenant) {
        return { data, toplam, adet }
    }
    let _tenant = await Tenant.findById(tenant)

    let planlar = await PlanlananTahsilatModel.find({vtahsilatYapanFirma: _tenant.key, durum: 'acik'}).exec()
    let count = 12
    let i = 1
    let today = moment()
    let thisWeekEnd = moment().endOf('isoWeek')
    data.push({
        start: today,
        end: thisWeekEnd,
        label: `${moment(today).format('DD MMM')}-${moment(thisWeekEnd).format('DD MMM')}`,
        value: 0
    })

    while (i < count) {
        let start = moment().add(i, 'weeks').startOf('isoWeek')
        let end = moment().add(i, 'weeks').endOf('isoWeek')
        data.push({
            start: start,
            end: end,
            label: `${moment(start).format('DD MMM')}-${moment(end).format('DD MMM')}`,
            value: 0
        })
        ++i
    }

    for (let plan of planlar) {
        if (moment(plan.tarih).isBefore(moment())) {
            continue
        }

        let i = 0
        for(let d of data) {
            if (moment(plan.tarih).isBetween(d.start, d.end)) {
                d.value = d.value + plan.tutar
                toplam += (plan.tutar)
                adet++
                break
            }
        }
    }

    return { data, toplam, adet }
}

exports.gelecekTahsilatlar = async (tenant) => {
    let data = []
    let toplam = 0
    let adet = 0

    if (!tenant) {
        return { data, toplam, adet }
    }
    let _tenant = await Tenant.findById(tenant)

    // Kesilmis faturalardan toplanir
    let faturalar = await FaturaModel.find({ acik: true, vkesenFirma: _tenant.key }).sort('vadeTarihi').exec()

    let timeSlots = []
    let count = 12
    let i = 1


    let today = moment().startOf('day')
    let thisWeekEnd = moment().endOf('isoWeek').startOf('day')
    timeSlots.push({ start: today, end: thisWeekEnd })
    data.push({
        start   : today,
        end     : thisWeekEnd,
        label   : `${moment(today).format('DD MMM')}-${moment(thisWeekEnd).format('DD MMM')}`,
        value   : 0
    })

    while (i < count) {
        let start = moment().add(i, 'weeks').startOf('isoWeek').startOf('day')
        let end = moment().add(i, 'weeks').endOf('isoWeek').startOf('day')
        timeSlots.push({ start, end })
        //count--
        data.push({
            start: start,
            end: end,
            label: `${moment(start).format('DD MMM')}-${moment(end).format('DD MMM')}`,
            value: 0
        })
        ++i
    }

    //console.log(timeSlots.map(t => `${moment(t.start).format('DD.MM.YYYY')} - ${moment(t.end).format('DD.MM.YYYY')}`))

    let _today = moment().startOf('day') //moment(new Date().setHours(0,0,0,0))

    for (let fatura of faturalar) {

        let faturaVadeTarihi = moment().startOf('day') // new Date(fatura.vadeTarihi)
        //faturaVadeTarihi = faturaVadeTarihi.setHours(0,0,0,0)
        if (moment(faturaVadeTarihi).isBefore(_today)) {
            continue
        }

        let i = 0
        for(let d of data) {
            if (moment(fatura.vadeTarihi).isBetween(d.start, d.end)) {
                d.value = d.value + fatura.genelToplam
                toplam += (fatura.genelToplam - fatura.tahsilatTutari)
                adet++
                break
            }
        }
    }

    return { data, toplam, adet }
}
