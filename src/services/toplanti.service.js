const MongooseQueryParser = require('mongoose-query-parser')
const UserService = require('./user.service')
const KisiService = require('./kisi.service')
const Toplanti = require('../models/toplanti.model')
const ToplantiKarar = require('../models/toplantikarar.model')
const Firma = require('../models/firma.model')
const diffHistory = require('mongoose-diff-history/diffHistory');
const GorevService = require('./gorev.service')
const MailService = require('./mail.service')
const AmqpProvider = require('../providers/amqp.provider')
const MsService = require('./ms.service')
const moment = require('moment')
const ToplantiHelper = require('../helpers/toplanti.helper')
const { uuid } = require('uuidv4')
const PdfProvider = require('../providers/pdf.provider')
const ToplantiKararService = require('./toplantiKarar.service')

exports.find = async (query) => {
    try {
        let filter = query || {}
        let parser = new MongooseQueryParser.MongooseQueryParser({})
        let f = parser.parse(filter)
        let toplantilar = await Toplanti
            .find(f.filter)
            .select(f.select)
            .skip(f.skip)
            .limit(f.limit)
            .populate(f.populate)
            .sort(f.sort)
            .exec();
        if (toplantilar) {
            let count = await Toplanti.countDocuments(f.filter).exec()
            return {
                data: toplantilar,
                page: f.skip,
                total: count
            }
        }
    }
    catch (e) {
        console.log(e)
    }
}

exports.findOne = async (id, query) => {
    let filter = query || {}
    let parser = new MongooseQueryParser.MongooseQueryParser({})
    let f = parser.parse(filter)
    let toplanti = await Toplanti
        .find({ _id: id, ...f.filter })
        .select(f.select)
        .populate(f.populate)
        .exec();
    if (toplanti && toplanti.length == 1) {
        return toplanti[0]
    }
    throw 'Toplanti Bulunamadi'
}

exports.create = async (toplanti, user) => {
    let newToplanti = await new Toplanti(toplanti).save()

    let _user = await UserService.findOne({ key: toplanti.vcreatedBy })
    let firmaKatilimcilar = await KisiService.find(`skip=0&limit=100&select=adi,soyadi,mail&filter={"key":{"$in":["${toplanti.vfirmaKatilimcilar.join('","')}"]}}`)
    let organizerKatilimcilar = await UserService.find(`skip=0&limit=100&select=username,email&filter={"key":{"$in":["${toplanti.vorganizerKatilimcilar.join('","')}"]}}`)

    let firmaAttendees = firmaKatilimcilar.data.map(k => {
        if (k.mail) {
            return { name: `${k.adi} ${k.soyadi}`, email: k.mail }
        }
    })
    let organizerAttendees = organizerKatilimcilar.data.map(k => { return { name: k.username, email: k.email } })

    let eventDto = {
        type: 'create',
        _id: newToplanti._id,
        from: _user.oid,
        subject: newToplanti.baslik,
        content: ToplantiHelper.generateContent(toplanti),
        start: moment(newToplanti.start).toISOString(),
        end: moment(newToplanti.end).toISOString(),      // TODO sureye gore hesaplanacak
        location: newToplanti.yer,
        isOnline: newToplanti.tur == 'videokonferans',                          // TODO
        attendees: [...firmaAttendees, ...organizerAttendees]
    }
    await sendCreateMail(eventDto, user)

    return newToplanti
}

exports.update = async (id, toplantiUpdateDto, user) => {
    let toplanti = await Toplanti.findByIdAndUpdate(id, toplantiUpdateDto, { new: true }).exec()

    let _user = await UserService.findOne(`key=${toplanti.vcreatedBy}`)
    let firmaKatilimcilar = await KisiService.find(`skip=0&limit=100&select=adi,soyadi,mail&filter={"key":{"$in":["${toplanti.vfirmaKatilimcilar.join('","')}"]}}`)
    let organizerKatilimcilar = await UserService.find(`skip=0&limit=100&select=username,email&filter={"key":{"$in":["${toplanti.vorganizerKatilimcilar.join('","')}"]}}`)

    let firmaAttendees = firmaKatilimcilar.data.map(k => {
        if (k.mail) {
            return { name: `${k.adi} ${k.soyadi}`, email: k.mail }
        }
    })
    let organizerAttendees = organizerKatilimcilar.data.map(k => { return { name: k.username, email: k.email } })

    let eventDto = {
        type: 'update',
        toplantiId: toplanti.msEventId,
        _id: toplanti._id,
        from: _user.oid,
        subject: toplanti.baslik,
        content: ToplantiHelper.generateContent(toplanti),
        start: moment(toplanti.start).toISOString(),
        end: moment(toplanti.end).toISOString(),      // TODO sureye gore hesaplanacak
        location: toplanti.yer,
        isOnline: toplanti.tur == 'videokonferans',                          // TODO
        attendees: [...firmaAttendees, ...organizerAttendees]
    }
    await sendCreateMail(eventDto, user)

    return toplanti
}

exports.remove = async (id) => {
    let deleted = await Toplanti.findByIdAndDelete(id)
    return deleted
}

exports.getHistory = async (id) => {
    let histories = await diffHistory.getDiffs('Toplanti', id)
    return histories
}

exports.tamamla = async (id, tamamlamaDto, user) => {
    // toplanti bulunur
    let toplanti = await Toplanti.findById(id).exec()
    if (!toplanti) {
        throw 'Toplanti bulunamadi'
    }

    let kararlar = await ToplantiKarar.find({ vToplanti: toplanti.key }).exec()
    for (let karar of kararlar) {
        if (karar.durum == 'Açık') {
            // karar icin gorev olusturulur
            await GorevService.create({
                key: uuid(),
                baslik: karar.karar,
                aciklama: 'Toplantı kararından otomatik oluşturulmuştur.',
                sonTarih: karar.tarih,
                vfirma: toplanti.vfirma,
                vsorumlu: karar.vorganizerSorumlu,
                vtoplanti: toplanti.key,
                vtoplantiKarar: karar.key,
                sonuc: 'acik',
                createdAt: new Date(),
                vcreatedBy: user.key
            }, user)
        }
    }

    toplanti = await Toplanti.findByIdAndUpdate(id, tamamlamaDto, { new: true }).exec()

    // Eger Toplanti IPTAL edildigse taraflara Rapor gonderilmez.
    if (tamamlamaDto.status == 'iptal') {
        return toplanti
    }
    // Toplanti sonucu taraflara gonderilir
    let _toplanti = await this.find(`skip=0&limit=1&_id=${toplanti._id}&populate=firma.marka,organizerKatilimcilar.username,firmaKatilimcilar.adi,firmaKatilimcilar.soyadi`)
    let _kararlar = await ToplantiKararService.find(`skip=0&limit=100&vToplanti=${toplanti.key}&populate=firmaSorumlu,organizerSorumlu.username`)

    let pdf = await PdfProvider.createToplantiPdf(_toplanti.data[0], _kararlar.data)
    let eventDto = {
        toplanti: _toplanti.data[0],
        kararlar: _kararlar.data,
        rapor: pdf.toString("base64")
    }

    await sendToplantiKapatmaMail(eventDto, user)

    return toplanti
}

const sendToplantiKapatmaMail = async (eventDto, user) => {
    let contnet = `
    <p>Merhaba,</p>
    <p><em>${moment(eventDto.toplanti.start).format(
        "DD.MM.yyyy"
    )}</em> tarihili "<em>${eventDto.toplanti.baslik
        }</em>" konulu toplantımızın karar notları ekte sunulmuştur.</p>
    <p>Saygılarımızla</p>`

    let firmaKatilimcilar = await KisiService.find(`skip=0&limit=100&select=adi,soyadi,mail&filter={"key":{"$in":["${eventDto.toplanti.vfirmaKatilimcilar.join('","')}"]}}`)
    let organizerKatilimcilar = await UserService.find(`skip=0&limit=100&select=username,email&filter={"key":{"$in":["${eventDto.toplanti.vorganizerKatilimcilar.join('","')}"]}}`)

    let firmaAttendees = firmaKatilimcilar.data.map(k => {
        if (k.mail) {
            //return { name: `${k.adi} ${k.soyadi}`, email: k.mail }
            return k.mail
        }
    })
    let organizerAttendees = organizerKatilimcilar.data.map(k => { return k.email })


    let dto = {
        from: user.oid,
        to: [...firmaAttendees, ...organizerAttendees],
        subject: `Toplantı Notları [${moment(eventDto.toplanti.start).format('DD.MM.YYYY')}]: ${eventDto.toplanti.baslik}`,
        content: contnet,
        attachments: [{ name: 'ToplantiRaporu.pdf', content: eventDto.rapor }]
    }
    await AmqpProvider.sendMail(dto)
}

const sendCreateMail = async (eventDto) => {
    await AmqpProvider.sendEvent(eventDto)
}

exports.toplantiYapilmisFirmalar = async () => {
    let start = moment().startOf('month')
    let end = moment().endOf('month')

    let toplantilar = await Toplanti.find({
        tarih: {
            $gte: start,
            $lte: end
        }
    }).lean().sort('tarih').exec()

    let tumFirmalar = await Firma.find({}).lean().exec()
    let result = []

    for (let toplanti of toplantilar) {
        let f = tumFirmalar.find(f => f.key == toplanti.vfirma)
        if (f) {
            result.push(f)
        }
    }
    return result
}

exports.toplantiYapilmamisFirmalar = async () => {
    let firmalar = []
    let start = moment().startOf('month')
    let end = moment().endOf('month')
    let seciliFirmalar = await Firma.find({
        durum: 'aktif',
        $or: [
            { argeMerkezi: true },
            { tasarimMerkezi: true }
        ]
    }).lean().exec()
    let toplantilar = await Toplanti.find({
        tarih: {
            $gte: start,
            $lte: end
        }
    }).lean().sort('tarih').exec()

    //console.table(seciliFirmalar.map(f => {return {key: f.key,marka: f.marka, durum: f.durum, argeMerkezi: f.argeMerkezi, tasarimMerkezi: f.tasarimMerkezi}}))
    //console.table(toplantilar.map(t => { return {vfirma: t.vfirma, tarih: t.tarih}}))

    for (let firma of seciliFirmalar) {
        if (toplantilar.find(t => t.vfirma == firma.key) >= 0) {

        }
        else {
            firmalar.push(firma)
        }
    }

    return firmalar
}