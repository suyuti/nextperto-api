const UserService = require('../services/user.service')
const User = require('../models/user.model')
const Role = require('../models/role.model')
const jwt = require("jsonwebtoken");
const YetkiHelper = require('../helpers/yetki.helper')
const SECRET = process.env.SECRET || 'sdfsdfasdufybwoqybfewfWEFweqFG#$G43gerwg32@%3Y54Y56y%$g3gr^5h5hreweww32213##@R@##$F@D'

exports.login = async ({ oid, email, displayName, userName }) => {
    let user = await User.findOne({ oid: oid })
        .populate('roles')
        .lean(true)
        .exec()
    if (user) {
        // yetkiler
        //let permissions = YetkiHelper.yetkiler()
        let permissions = await UserService.getPermissions(user._id)

        // token
        const token = jwt.sign({
            id: user._id,
            oid: oid,
            //permissions: permissions,
            key: user.key
        },
            SECRET)


        return {
            user: {
                _id: user._id,
                active: user.active,
                email: user.email,
                image: user.image,
                displayName: user.displayName,
                isAdmin: user.isAdmin,
                key: user.key
            },
            permissions: permissions,
            token
        }
    }
    else {
        // Kullanici bulunamadi pasif olarak kayit yapilir
        try {
            let newUser = await new User({
                username: userName,
                email: email,
                displayName: displayName,
                active: false,
                oid: oid
            }).save()
            // token
            const token = jwt.sign({
                id: newUser._id,
                oid: oid
            },
                SECRET)
            return {
                user: {
                    active: newUser.active,
                    email: newUser.email,
                    image: newUser.image,
                    displayName: newUser.displayName
                },
                permissions: [],
                token
            }
        }
        catch (e) {
            console.log(e)
        }
    }
}

exports.session = async ({ oid }) => {
    let user = await User.findOne({ oid: oid })
        .populate('roles')
        .lean(true)
        .exec()
    if (user) {
        // yetkiler
        let permissions = await UserService.getPermissions(user._id)

        // token
        const token = jwt.sign({
            id: user._id,
            oid: oid,
            permissions: permissions
        },
            SECRET)

        //let permissions = YetkiHelper.yetkiler()

        return {
            user: {
                active: user.active,
                email: user.email,
                image: user.image,
                displayName: user.displayName,
                userId: user._id,
                isAdmin: user.isAdmin
            },
            permissions: permissions,
            token
        }
    }
}