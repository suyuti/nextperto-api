const MongooseQueryParser = require('mongoose-query-parser')
const OdemeModel = require('../models/odeme.model')
const GelenFaturaService = require('./gelenfatura.service')

exports.find = async (query) => {
    try {
        let filter = query || {}
        let parser = new MongooseQueryParser.MongooseQueryParser({})
        let f = parser.parse(filter)
        let odemeler = await OdemeModel
            .find(f.filter)
            .select(f.select)
            .skip(f.skip)
            .limit(f.limit)
            .populate(f.populate)
            .sort(f.sort)
            .exec();
        if (odemeler) {
            let count = await OdemeModel.countDocuments(f.filter).exec()
            return {
                data: odemeler,
                page: f.skip,
                total: count
            }
        }
    }
    catch(e) {
        console.log(e)
    }
}

exports.findOne = async (id, query) => {
    let filter = query || {}
    let parser = new MongooseQueryParser.MongooseQueryParser({})
    let f = parser.parse(filter)
    let odeme = await OdemeModel
    .find({_id: id, ...f.filter})
    .select(f.select)
    .populate(f.populate)
    .exec();
    if (odeme && odeme.length == 1) {
        return odeme[0]
    }
    throw 'OdemeModel Bulunamadi'
}

exports.create = async (odeme, user) => {
    odeme.createdBy = user,
    odeme.createdAt = new Date()
    let newOdeme = await new OdemeModel(odeme).save()

    // Odeme yapilan fatura ODENDI yapilir
    await GelenFaturaService.update(odeme.fatura._id, {odendi: true, odemeTarihi: odeme.createdAt}, user)
    
    return newOdeme
}

exports.update = async (id, dto, user) => {
    OdemeModel.findByIdAndUpdate(id, dto, {
        new:true,
        __user: user._id,
        __reason: "updated"
    }, (err, doc) => {
        if (!err) {
            return doc
        }
    })
}

exports.remove = async (id) => {
    let deleted = await OdemeModel.findByIdAndDelete(id)
    return deleted
}
