const MongooseQueryParser = require('mongoose-query-parser')
const OdemePlanModel = require('../models/odemeplani.model')
const GelenFaturaService = require('./gelenfatura.service')

exports.find = async (query) => {
    try {
        let filter = query || {}
        let parser = new MongooseQueryParser.MongooseQueryParser({})
        let f = parser.parse(filter)
        let odemePlanlari = await OdemePlanModel
            .find(f.filter)
            .select(f.select)
            .skip(f.skip)
            .limit(f.limit)
            .populate(f.populate)
            .sort(f.sort)
            .exec();
        if (odemePlanlari) {
            let count = await OdemePlanModel.countDocuments(f.filter).exec()
            return {
                data: odemePlanlari,
                page: f.skip,
                total: count
            }
        }
    }
    catch(e) {
        console.log(e)
    }
}

exports.findOne = async (id, query) => {
    let filter = query || {}
    let parser = new MongooseQueryParser.MongooseQueryParser({})
    let f = parser.parse(filter)
    let odemePlan = await OdemePlanModel
    .find({_id: id, ...f.filter})
    .select(f.select)
    .populate(f.populate)
    .exec();
    if (odemePlan && odemePlan.length == 1) {
        return odemePlan[0]
    }
    throw 'Odeme Plan Bulunamadi'
}

exports.create = async (odeme, user) => {
    odeme.createdBy = user,
    odeme.createdAt = new Date()
    let newOdemePlan = await new OdemePlanModel(odeme).save()

    // Odeme yapilan fatura ODENDI yapilir
    await GelenFaturaService.update(odeme.fatura._id, {odemePlanlandi: true, planlananOdemeTarihi: odeme.createdAt}, user)
    
    return newOdemePlan
}

exports.update = async (id, dto, user) => {
    OdemePlanModel.findByIdAndUpdate(id, dto, {
        new:true,
        __user: user._id,
        __reason: "updated"
    }, (err, doc) => {
        if (!err) {
            return doc
        }
    })
}

exports.remove = async (id) => {
    let deleted = await OdemePlanModel.findByIdAndDelete(id)
    return deleted
}
