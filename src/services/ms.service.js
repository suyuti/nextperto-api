const ToplantiService = require('./toplanti.service')
const axios = require('axios')
var querystring = require("querystring");

const tenantId = process.env.TENANT_ID
const clientId = process.env.CLIENT_ID
const clientSecret = process.env.CLIENT_SECRET

exports.login = async () => {
    let loginResp = await axios.post(
        `https://login.microsoftonline.com/${tenantId}/oauth2/v2.0/token`,
        querystring.stringify({
            client_id: clientId,
            scope: "https://graph.microsoft.com/.default",
            client_secret: clientSecret,
            grant_type: "client_credentials",
        }),
        {
            headers: { "Content-Type": "application/x-www-form-urlencoded" },
        }
    )
    return loginResp
}


// TODO bu metodlar ayri uygulamada calisacak. MQ ile gonderilecek

exports.createEvent = async (eventDto) => {
    let loginResp = await this.login()

    var eventParameters = {
        subject: eventDto.subject,
        body: {
            contentType: 'HTML',
            content: eventDto.content
        },
        start: {
            dateTime: `${eventDto.start}`,
            timeZone: "Turkey Standard Time",
        },
        end: {
            dateTime: `${eventDto.end}`,
            timeZone: "Turkey Standard Time",
        },
        location: {
            displayName: `${eventDto.location}`,
        },
        isOnlineMeeting: eventDto.isOnline,
        onlineMeetingProvider: "teamsForBusiness",
        attendees: eventDto.attendees.map(e => { 
            return { 
                emailAddress: { 
                    address: e.email, 
                    name: e.name
                }, 
                type: 'required' 
            }
        })
    }

    let eventResp = await axios.post(`https://graph.microsoft.com/v1.0/users/${eventDto.from}/calendar/events`,
        eventParameters,
        { headers: { Authorization: "Bearer " + loginResp.data.access_token } }
    );

    if (eventResp.status == 201) {
        await ToplantiService.update(eventDto._id, { msEventId: eventResp.data.id })
        return eventResp.data
    }
}
