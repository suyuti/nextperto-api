const Gorev = require('../models/gorev.model')
const Fatura = require('../models/fatura.model')
const moment = require('moment')

exports.getGorev = async (query) => {
    let result = await Gorev.aggregate([
        {
            $match: query
        },
        {
            $project: {
                acik: { $cond: [{ $eq: ['$sonuc', 'acik'] }, 1, 0] },
                basarili: { $cond: [{ $eq: ['$sonuc', 'basarili'] }, 1, 0] },
                basarisiz: { $cond: [{ $eq: ['$sonuc', 'basarisiz'] }, 1, 0] },
                iptal: { $cond: [{ $eq: ['$sonuc', 'iptal'] }, 1, 0] }
            }
        },
        {
            $group: {
                _id: 0,
                acik: { $sum: '$acik' },
                basarili: { $sum: '$basarili' },
                basarisiz: { $sum: '$basarisiz' },
                iptal: { $sum: '$iptal' },
            }
        }
    ]).exec()
    return result
}

//--------------------------------------------------------------------------------------------
// Vadesi bugun olan faturalar

exports.getBugunYapilacakTahsilatlar = async (query) => {
    //let today = new Date()
    //let oneDay = (1000 * 60 * 60 * 24)
    //let oneWeek = new Date(today.valueOf() - (7 * oneDay))

    let start = new Date()
    let end = new Date()
    end.setDate(start.getDate() + 1)

    start.setHours(0, 0, 0, 0)
    end.setHours(0, 0, 0, 0)

    let result = await Fatura.aggregate([
        {
            $match: {
                vadeTarihi: { $gte: start, $lt: end }
            }
        },
        {
            $project: {
                genelToplam: 1,
                adet: 1
            }
        },
        {
            $group: {
                _id: 0,
                toplam: {
                    $sum: '$genelToplam'
                },
                adet: {
                    $sum: 1
                }
            }
        }
    ]).exec()
    return result
}

const prepareQuery = (weeks, pos) => {

    if (pos == 0) return `"A"`
    return { "$cond": [{ "$lte": ["$vadeTarihi", new Date(weeks[pos - 1])] }, prepareQuery(weeks, pos - 1), `Week${pos}`] }
}

//https://stackoverflow.com/questions/33657242/mongodb-aggregate-group-for-multiple-date-ranges
exports.getGelecekTahsilatlar = async (count) => {
    let weeks = []
    let _count = parseInt(count)
    for (let i = 0; i < _count; ++i) {
        let a = moment().add(i, 'weeks').endOf('isoWeek')
        weeks.push(new Date(a))
    }
    let r = prepareQuery(weeks, _count)

    let result = await Fatura.aggregate([
        {
            $group: {
                _id: r,
                toplam: {
                    $sum: '$genelToplam'
                },
                adet: {
                    $sum: 1
                }
            }
        }
    ]).exec()
    return result
}

exports.tahsilatRapor = async () => {
    let result = await Fatura.aggregate([
        {
            $group: {
                _id: 'vadesiGecen',
                toplam: {
                    $cond: { if: { $lte: ['$vadeTarihi', new Date()] }, then: '$genelToplam', else: 0 }
                }
            }
        }
    ]).exec()

    //console.log(result)

    return {
        planlanmis: 10,
        vadesiGecen: 20,
        toplamTahsilat: 30,
        ortalamaVadeAsimi: 40
    }
}

// Haftalik fatura vadeleri ve planlanmis tahsilatlar
// https://stackoverflow.com/questions/34610096/how-to-group-by-documents-by-week-in-mongodb
exports._getAlacaklar = async (count) => {
    try {
        let result = await Fatura.aggregate([
            {
                $match: {
                    taslak: { $eq: false },
                    acik: { $eq: true },
                    vadeTarihi: { $gte: new Date() }
                }
            },
            {
                $project: {
                    vadeTarihi: 1,
                    genelToplam: 1,
                    vkesenFirma: 1
                }
            },
            {
                $group: {
                    _id: { $week: '$vadeTarihi' },
                    toplam: {
                        $sum: '$genelToplam'
                    },
                    adet: {
                        $sum: 1
                    }
                }
            },
            {
                $sort: {
                    _id: 1
                }
            }
        ]).exec()

        //console.log(result)

        return result
    }
    catch (e) {
        console.log(e)
    }
}

// Haftalik ödeme vadeleri ve planlanmis ödemeler
// https://stackoverflow.com/questions/34610096/how-to-group-by-documents-by-week-in-mongodb
exports.getVerecekler = async (count) => {
    let result = await Fatura.aggregate([
        {
            $match: {
                taslak: { $eq: false },
                acik: { $eq: true },
                //vadeTarihi: {$gte: moment().toISOString()}
            }
        },
        {
            $project: {
                vadeTarihi: 1,
                genelToplam: 1,
                vkesenFirma: 1
            }
        },
        {
            $group: {
                _id: { $week: '$vadeTarihi' },
                toplam: {
                    $sum: '$genelToplam'
                },
                adet: {
                    $sum: 1
                }
            }
        },
        {
            $sort: {
                _id: 1
            }
        }
    ]).exec()

    return result
}


exports.getAlacaklar = async (count, query) => {
    try {
        // 1. Faturalari al
        let faturalar = await Fatura.find({ acik: true, ...query }, 'genelToplam vadeTarihi').sort('-vadeTarihi').exec()

        let weeks = []
        while (count) {
            let start = moment().add(count, 'weeks').startOf('isoWeek')
            let end = moment().add(count, 'weeks').endOf('isoWeek')
            weeks.push({start, end})
            count--
        }

        weeks = weeks.reverse()

        let result = []
        for ( let week of weeks) {
            let f = faturalar.filter(f => moment(f.vadeTarihi).isBetween(week.start, week.end, undefined, '[]'))
            let toplam = f.reduce((acc, v) => {return acc + v.genelToplam}, 0)
            result.push({toplam, adet: f.length})
        }

        // 2. Haftalik olarak grupla
        return result 
    }
    catch (e) {
        console.log(e)
    }
}