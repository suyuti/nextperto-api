const axios = require('axios')
const moment = require('moment')
const MsService = require('./ms.service')
const UserService = require('./user.service')

exports.getEvents = async (form) => {
    let login = await MsService.login()
    let events=[]

    for (let user of form.kisiler) {
        let _user = await UserService.findOne({_id:user})



        let startDateTimeStr = moment(form.baslangicTarihi).format('YYYY-MM-DDTHH:mm')
        let endDateTimeStr = moment(form.bitisTarihi).format('YYYY-MM-DDTHH:mm')
        //let user = '092a66e6-2a24-4b54-aea2-d1658185da35'
    
        let nextLink = `https://graph.microsoft.com/v1.0/users/${_user.oid}/events?$select=subject,bodyPreview,organizer,attendees,start,end,location&$filter=start/dateTime%20ge%20%27${startDateTimeStr}%27%20and%20end/dateTime%20le%20%27${endDateTimeStr}%27`
        do {
            let resp = await axios.get(
                nextLink,
                { headers: { Authorization: "Bearer " + login.data.access_token } }
            )
            //events = [...events, ...resp.data.value ]
            nextLink = resp.data[`@odata.nextLink`]

            //console.log(JSON.stringify(resp.data.value, null, 4))

            let result = resp.data.value.map(e => {
                return {
                    id: e.id,
                    user: user,
                    title: e.subject,
                    allDay: false,
                    start: new Date(e.start.dateTime),
                    end: new Date(e.end.dateTime)
                }
            })
            events = [...events, ...result ]
        } while(nextLink)


    }

    return events
}
