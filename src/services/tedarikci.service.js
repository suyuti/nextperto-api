const MongooseQueryParser = require('mongoose-query-parser')
const diffHistory = require('mongoose-diff-history/diffHistory');
const TedarikciModel = require('../models/tedarikci.model')
const moment = require('moment');
const { uuid } = require('uuidv4');

exports.find = async (query) => {
    try {
        let filter = query || {}
        let parser = new MongooseQueryParser.MongooseQueryParser({})
        let f = parser.parse(filter)
        let tedarikciler = await TedarikciModel
            .find(f.filter)
            .select(f.select)
            .skip(f.skip)
            .limit(f.limit)
            .populate(f.populate)
            .sort(f.sort)
            .exec();
        if (tedarikciler) {
            let count = await TedarikciModel.countDocuments(f.filter).exec()
            return {
                data: tedarikciler,
                page: f.skip,
                total: count
            }
        }
    }
    catch (e) {
        console.log(e)
    }
}

exports.findOne = async (id, query) => {
    let filter = query || {}
    let parser = new MongooseQueryParser.MongooseQueryParser({})
    let f = parser.parse(filter)
    let tedarikci = await TedarikciModel
        .find({ _id: id, ...f.filter })
        .select(f.select)
        .populate(f.populate)
        .exec();
    if (tedarikci && tedarikci.length == 1) {
        return tedarikci[0]
    }
    throw 'Tedarikci Bulunamadi'
}

exports.create = async (tedarikci) => {
    try {
        let newTedarikci = await new TedarikciModel(tedarikci).save()
        return newTedarikci
    }
    catch (e) {
        console.log(e)
    }
}

exports.update = async (id, dto, user) => {
    TedarikciModel.findByIdAndUpdate(id, dto, {
        new: true,
    }, (err, doc) => {
        if (!err) {
            return doc
        }
    })
}

exports.remove = async (id) => {
    let deleted = await TedarikciModel.findByIdAndDelete(id)
    return deleted
}

