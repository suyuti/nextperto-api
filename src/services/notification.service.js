const MongooseQueryParser = require('mongoose-query-parser')
const Notification = require('../models/notification.model')
const SocketService = require('../services/socket.service')
const moment=require('moment')

exports.find = async (query) => {
    try {
        let filter = query || {}
        let parser = new MongooseQueryParser.MongooseQueryParser({})
        let f = parser.parse(filter)
        let notification = await Notification
            .find(f.filter)
            .select(f.select)
            .skip(f.skip)
            .limit(f.limit)
            .populate(f.populate)
            .sort(f.sort)
            .exec();
        if (notification) {
            let count = await Notification.countDocuments(f.filter).exec()
            return {
                data: notification,
                page: f.skip,
                total: count
            }
        }
    }
    catch(e) {
        console.log(e)
    }
}

exports.findOne = async (id, query) => {
    let filter = query || {}
    let parser = new MongooseQueryParser.MongooseQueryParser({})
    let f = parser.parse(filter)
    let notification = await Notification
    .find({_id: id, ...f.filter})
    .select(f.select)
    .populate(f.populate)
    .exec();
    if (notification && notification.length == 1) {
        return notification[0]
    }
    throw 'Notification Bulunamadi'
}

exports.create = async (notification, user) => {
    let newNotification = await new Notification(notification).save()
    
    //SocketService.send(newGorev.sorumlu, {
    //    type        : 'gorev.create',
    //    message     : 'Yeni görev oluşturuldu',
    //    gorevId     : newGorev._id,
    //    baslik      : newGorev.baslik,
    //    sonTarih    : newGorev.sonTarih,
    //    olusturan   : newGorev.createdBy,
    //    oncelik     : newGorev.oncelik
    //})
    //await sendCreateMail(newGorev, user)
    
    return newNotification
}

exports.update = async (id, notificationUpdateDto, user) => {
    Notification.findByIdAndUpdate(id, notificationUpdateDto, {
        new:true,
        //__user: user._id,
        //__reason: "updated"
    }, (err, doc) => {
        if (!err) {
            return doc
        }
    })
}

exports.remove = async (id) => {
    let deleted = await Notification.findByIdAndDelete(id)
    return deleted
}

