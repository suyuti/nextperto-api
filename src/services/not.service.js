const MongooseQueryParser = require('mongoose-query-parser')
const Not = require('../models/not.model')

exports.find = async (query) => {
    let filter = query || {}
    let f = MongooseQueryParser.parse(filter)
    let notlar = await Not
        .find(f.filter)
        .select(f.select)
        .skip(f.skip)
        .limit(f.limit)
        .populate(f.populate)
        .exec();
    if (notlar) {
        let count = await Not.countDocuments(f.filter).exec()
        return {
            data: notlar,
            page: f.skip,
            total: count
        }
    }
}

exports.findOne = async (id) => {
    let not = await Not.findById(id).exec()
    return not
}

exports.create = async (not) => {
    let newNot = await new Not(not).save()
    return newNot
}

exports.update = async (id, notUpdateDto) => {
    let not = await Not.findByIdAndUpdate(id, notUpdateDto, {new:true}).exec()
    return not
}

exports.remove = async (id) => {
    let deleted = await Not.findByIdAndDelete(id)
    return deleted
}
