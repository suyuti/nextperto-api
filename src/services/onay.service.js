const MongooseQueryParser = require('mongoose-query-parser')
const OnayModel = require('../models/onay.model')
const GorevModel = require('../models/gorev.model')
const AmqpProvider = require('../providers/amqp.provider')
const moment = require('moment')
const { addHistory, addHistory2 } = require('../helpers/history.helper');

exports.find = async (query) => {
    try {
        let filter = query || {}
        let parser = new MongooseQueryParser.MongooseQueryParser({})
        let f = parser.parse(filter)
        let onaylar = await OnayModel
            .find(f.filter)
            .select(f.select)
            .skip(f.skip)
            .limit(f.limit)
            .populate(f.populate)
            .sort(f.sort)
            .exec();
        if (onaylar) {
            let count = await OnayModel.countDocuments(f.filter).exec()
            return {
                data: onaylar,
                page: f.skip,
                total: count
            }
        }
    }
    catch(e) {
        console.log(e)
    }
}

exports.findOne = async (id, query) => {
    let filter = query || {}
    let parser = new MongooseQueryParser.MongooseQueryParser({})
    let f = parser.parse(filter)
    let onay = await OnayModel
    .findOne({_id: id, ...f.filter})
    .select(f.select)
    .populate(f.populate)
    .exec();
    if (onay) {
        return onay
    }
    throw 'Onay Bulunamadi'
}

exports.create = async (onay) => {
    let newOnay = await new OnayModel(onay).save()
    if (newOnay) {
        return newOnay.toObject()
    }
}

exports.update = async (id, onayDto, user) => {
    Gorusme.findByIdAndUpdate(id, onayDto, {
        new:true,
        //__user: user._id,
        //__reason: "updated"
    }, (err, doc) => {
        if (!err) {
            return doc
        }
    })
}

exports.remove = async (id) => {
    let deleted = await OnayModel.findByIdAndDelete(id)
    return deleted
}

const _onayla = async (onay, user, onayDurum, aciklama) => {
    if (onay.refCollection == 'Gorev') {
        if (onayDurum == 'onay') {
            let gorev = await GorevModel.findByIdAndUpdate(onay.referenceItem, {...onay.nextState, onayBekleniyor: false}, {lean:true, new: true}).exec()
            if (gorev) {
                await addHistory2(onay.diff, onay.referenceItem, 'Görev değişikliği onaylandı', user, 'Gorev')
            }
        }
        else {
            let gorev = await GorevModel.findByIdAndUpdate(onay.referenceItem, {onayBekleniyor: false}, {lean:true, new: true}).exec()
            if (gorev) {
                await addHistory2(onay.diff, onay.referenceItem, 'Görev değişikliği reddedildi', user, 'Gorev')
            }
        }
        await OnayModel.findByIdAndUpdate(onay._id, {
            onaylayan: user._id,
            onayTarihi: new Date(),
            onayDurumu: onayDurum,
            onayAciklamasi: aciklama
        }).exec()    
    }
}

exports.onayla = async (onayId, user, aciklama) => {
    let onay = await OnayModel.findById(onayId).lean(true).exec()
    _onayla(onay, user, 'onay', aciklama)
}

exports.red = async (onayId, user, aciklama) => {
    let onay = await OnayModel.findById(onayId).lean(true).exec()
    _onayla(onay, user, 'red', aciklama)
}

exports.iptal = async (onayId, user, aciklama) => {
    let onay = await OnayModel.findById(onayId).lean(true).exec()
    _onayla(onay, user, 'iptal', aciklama)
}

exports.sendOnayMail = async (onay, to, from) => {
    let content = `
    <table style="width: 90%;">
    <tbody>
    <tr>
    <td style="width: 445px;" colspan="2">Sayın ${to.username},</td>
    </tr>
    <tr>
    <td style="width: 445px;" colspan="2">
    <p>&nbsp;</p>
    <p>Sizden bir onay bekleniyor.&nbsp;</p>
    </td>
    </tr>
    <tr>
    <td style="width: 445px;" colspan="2"><hr />&nbsp;</td>
    </tr>
    <tr>
    <td style="width: 157px; vertical-align: top;"><strong>Onay Konusu</strong></td>
    <td style="width: 288px;">${onay.refCollection}</td>
    </tr>
    <tr>
    <td style="width: 157px; vertical-align: top;"><strong>Onay İsteyen</strong></td>
    <td style="width: 288px;">${from.username}</td>
    </tr>
    <tr>
    <td style="width: 157px; vertical-align: top;"><strong>Talep Tarihi</strong></td>
    <td style="width: 288px;">${moment(onay.istemeTarihi).format('HH:mm DD.MM.YYYY')}</td>
    </tr>
    </tbody>
    </table>
    <p>&nbsp;</p>
    <p><em>Bu mesaj otomatik olarak gönderilmiştir.</em></p>
`

    await AmqpProvider.sendMail({
        from: '5b487f55-8501-4b6f-9297-f025cd56e1e1', //from.oid,
        to: [to.email],
        subject: `[EXPERTO-APP] Yeni Onay Talebi`,
        content: content
    })
}
