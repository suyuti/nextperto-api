const MongooseQueryParser = require('mongoose-query-parser')
const Role = require('../models/role.model')

exports.find = async (query) => {
    let filter = query || {}
    let parser = new MongooseQueryParser.MongooseQueryParser({})
    let f = parser.parse(filter)
    let roleler = await Role
        .find(f.filter)
        .select(f.select)
        .skip(f.skip)
        .limit(f.limit)
        .populate(f.populate)
        .exec();
    if (roleler) {
        let count = await Role.countDocuments(f.filter).exec()
        return {
            data: roleler,
            page: f.skip,
            total: count
        }
    }
}

exports.findOne = async (id) => {
    let role = await Role.findById(id).exec()
    return role
}

exports.create = async (role) => {
    let newRole = await new Role(role).save()
    return newRole
}

exports.update = async (id, roleUpdateDto) => {
    let role = await Role.findByIdAndUpdate(id, roleUpdateDto, {new:true}).exec()
    return role
}

exports.remove = async (id) => {
    let deleted = await Role.findByIdAndDelete(id)
    return deleted
}
