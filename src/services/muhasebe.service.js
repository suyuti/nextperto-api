const FirmaModel = require('../models/firma.model')
const FaturaModel = require('../models/fatura.model')
const FaturaKalemModel = require('../models/faturakalem.model')
const UrunModel = require('../models/urun.model')
const FirmaService = require('../services/firma.service')
const FaturaService = require('../services/fatura.service')
const moment = require('moment')
const _ = require('lodash')

exports.getFirmalar = async (query) => {
    try {
        let firmalar = await FirmaService.find(query)
        if (firmalar.data) {
            for (let firma of firmalar.data) {
                let toplamFatura = await FirmaService.getToplamFatura(firma.key)
                let toplamTahsilat = await FirmaService.getToplamTahsilat(firma.key)

                firma.toplamFatura = toplamFatura.length > 0 ? toplamFatura[0].toplam : 0
                firma.toplamFaturaAdet = toplamFatura.length > 0 ? toplamFatura[0].adet : 0
                firma.toplamTahsilat = toplamTahsilat.length > 0 ? toplamTahsilat[0].toplam : 0
                firma.toplamTahsilatAdet = toplamTahsilat.length > 0 ? toplamTahsilat[0].adet : 0
            }
        }
        return firmalar
    }
    catch (e) {
        console.log(e)
    }
}

exports.getTahsilatlar = async (query) => {
    let faturalar = await FaturaService.find(query)
    return faturalar
}

const aylar = [
    'Ocak',
    'Şubat',
    'Mart',
    'Nisan',
    'Mayıs',
    'Haziran',
    'Temmuz',
    'Ağustos',
    'Eylül',
    'Ekim',
    'Kasım',
    'Aralık',
]
exports.getRaporFatura = async (query) => {
    //console.log(query)
    let data = [
        //{ id: 1, m: 1, y: 2021, ay: 'Ocak', adet: 15, toplam: 123000 },
        //{ id: 1, m: 1, y: 2021, ay: 'Ocak', adet: [5, 10], toplam: [15000, 35000] },
    ]
    let sorgulananFirmalar = query.firma
    let faturalar = await FaturaModel.find().exec()
    for (let ay = 0; ay < 12; ++ay) {
        let r = {
            id: ay,
            m: ay + 1,
            y: 2021,
            ay: aylar[ay],
            adet: 0,
            toplam: ay
        }

        let start = moment(`${r.y}.${r.m}.${1}`).startOf('month')
        let end = moment(start).endOf('month')
        let _f = faturalar.filter(f => {
            if (!query.firma.includes(f.vkesenFirma)) {
                return false
            }
            if (moment(f.duzenlemeTarihi).isBetween(start, end)) {
                if (f.vfirma == '6f3ed423-78ac-4fe4-84c7-80aa0e90a719') { // EXPERTO
                    return false
                }
                return true
            }
            else {
                return false
            }
        })

        // Adet ve Toplam degerleri sorgudaki kesenFirmalara gore ayri ayri dizi halinde veriliyor
        // Sorgudaki kesen Firma sirasina gore adet ve toplamda aynisirada ilgili degerler hesaplaniyor.
        r.adet = []
        r.toplam = []
        for (var i = 0; i < sorgulananFirmalar.length; ++i) {
            let firmaIcinFaturalar = _f.filter(f => f.vkesenFirma == sorgulananFirmalar[i])
            r.adet.push(firmaIcinFaturalar.length)

            let firmaIcinToplam = firmaIcinFaturalar.reduce((acc, v) => { return acc + v.genelToplam }, 0)
            r.toplam.push(firmaIcinToplam)
        }

        //r.adet = _f.length
        //r.toplam = _f.reduce((acc, v) => { return acc + v.genelToplam }, 0)
        data.push(r)
    }

    return data
}

exports.getRaporUrun = async (query) => {
    let data = [
        //{ id: 0, urun: 'Urun1', toplam: 120, adet: 2 },
    ]
    let urunler = await UrunModel.find().exec()
    let faturalar = await FaturaKalemModel.find().exec()
    for (let i = 0; i < urunler.length; ++i) {
        let f = faturalar.filter(f => f.vurun == urunler[i].key)
        let toplam = 0
        if (f) {
            toplam = f.reduce((acc, v) => { return acc + v.toplam }, 0)
        }
        let d = {
            id: i,
            urun: urunler[i].adi,
            toplam: toplam,
            adet: f.length || 0
        }
        data.push(d)
    }

    return data
}

exports.getRaporUrunAylik = async (query) => {
    /*
        [{
            ay: 'Ocak', 
            yil: 2021,
            urunler: [
                {urun: 'Urun 1', toplam: 150000, adet: 3},
                {urun: 'Urun 2', toplam:  50000, adet: 1}
            ]
        }]
    */

    let data = []

    // Yilin ilk gunu bulunur
    let yilBasiGunu = moment().startOf('year')

    // Bu yila ait tum faturalar bulunur
    let faturalar = await FaturaModel.find({ duzenlemeTarihi: { $gte: yilBasiGunu } }).lean(true).exec()

    // Bu yila ait faturalarin detaylari bulunur
    let faturaKeys = faturalar.map(f => f.key)
    let faturaKalemler = await FaturaKalemModel.find({ vfatura: {$in : faturaKeys} }).lean(true).exec()

    // Urun cesitleri bulunur
    let urunler = await UrunModel.find().lean(true).exec()
    data = urunler.map(urun => { return {name: urun.adi, key: urun.key, data: [0,0,0,0,0,0,0,0,0,0,0,0]}})

    // Her ay icin toplamlar hesaplanir
    for (let ay = 1; ay <= 12; ++ay) {
        // Bu ayin faturalari ve detaylari ayiklanir
        let ayinFaturalari = faturalar.filter(f => moment(f.duzenlemeTarihi).get('month') == ay)
        let ayinFaturaKalemleri = faturaKalemler.filter(f => ayinFaturalari.find(af => af.key == f.vfatura))


        ayinFaturaKalemleri.forEach(f => {
            let d = data.filter(d => d.key == f.vurun)
            d.data[ay - 1] += f.toplam
        })
        console.log(data)
    }
    console.log(data)

    //let urunler = await UrunModel.find().exec()
    //let faturalar = await FaturaKalemModel.find().exec()
    //for (let i = 0; i < urunler.length; ++i) {
    //    let f = faturalar.filter(f => f.vurun == urunler[i].key)
    //    let toplam = 0
    //    if (f) {
    //        toplam = f.reduce((acc, v) => {return acc + v.toplam}, 0)
    //    }
    //    let d = {
    //        id: i,
    //        urun: urunler[i].adi,
    //        toplam: toplam,
    //        adet: f.length || 0
    //    }
    //    data.push(d)
    //}
    //
    //return data
}
