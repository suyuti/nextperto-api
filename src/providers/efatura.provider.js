process.env["NODE_TLS_REJECT_UNAUTHORIZED"] = 0;
const soap = require("soap");
const moment = require('moment')
const EFaturaHelper = require('../helpers/fatura.helper')
const EFATURA_URL   = process.env.EFATURA_URL
const EFATURA_USER  = process.env.EFATURA_USER
const EFATURA_PWD   = process.env.EFATURA_PWD

let lastLoginDate   = undefined
let client          = undefined

exports.login = async (url, user, password) => {
    return new Promise((resolve, reject) => {
        const wsSecurity = new soap.WSSecurity(user, password, {})
        soap.createClient(url, (err, client) => {
            if (err) {
                console.log(err)
                reject();
            } else {
                client.setSecurity(wsSecurity);
                console.log('login ok')
                lastLoginDate = new Date()
                resolve(client);
            }
        });
    });
}

const faturaNoUret = (data) => {
    return new Promise((resolve, reject) => {
        client.faturaNoUret(data, (e, res) => {
            if (e) {
                reject(e)
            }
            else {
                resolve(res.return)
            }
        })
    })
}

const eFaturaKullanicisiMi = (data) => {
    return new Promise((resolve, reject) => {
        client.efaturaKullaniciBilgisi(data, (e, res) => {
            if (e) {
                reject(e)
            }
            else {
                resolve(res.return)
            }
        })
    })
}

const _gidenBelgeleriIndir = (data) => {
    return new Promise((resolve, reject) => {
        client.gidenBelgeleriIndir(data, (e, res) => {
            if (e) {
                reject(e)
            }
            else {
                resolve(res.return)
            }
        })
    })
}

const getOnIzleme = (data) => {
    return new Promise((resolve, reject) => {
        client.ublOnizleme(data, (e, res) => {
            if (e) {
                reject(e)
            }
            else {
                resolve(res.return)
            }
        })
    })
}

exports.onIzleme = async (fatura, tenant) => {
    console.log(`Tenant: ${tenant.efaturaUrl} ${tenant.efaturaUser} ${tenant.efaturaPwd}`)
    if (!lastLoginDate) {
        console.log('login')
        client = await this.login(
            tenant.efaturaUrl,
            tenant.efaturaUser,
            tenant.efaturaPwd
            //EFATURA_URL, EFATURA_USER, EFATURA_PWD
        )
    }

    let today = new Date(tenant.sonFaturaTarihi)
    today.setHours(0,0,0,0)

    let faturaDuzenlemeTarihi = new Date(fatura.duzenlemeTarihi)
    faturaDuzenlemeTarihi.setHours(0,0,0,0)
    let faturaKodu = faturaDuzenlemeTarihi < today ? tenant.gecmisTarihFaturaKodu : tenant.faturaKodu


    //let _eFaturaKullanicisiMi = await eFaturaKullanicisiMi({vergiTcKimlikNo: fatura.gonderen.vkn})
    let _eFaturaKullanicisiMi = await eFaturaKullanicisiMi({vergiTcKimlikNo: fatura.alan.vkn})
    console.log("_eFaturaKullanicisiMi: ", _eFaturaKullanicisiMi)
    console.log(`gonderen vkn: ${fatura.gonderen.vkn}`)
    console.log(`alan vkn: ${fatura.alan.vkn}`)
    if (_eFaturaKullanicisiMi) {
        let faturaNo    = await faturaNoUret({ vknTckn: fatura.gonderen.vkn, faturaKodu: faturaKodu })
        fatura.no               = faturaNo
        fatura.duzenlemeTarihi  = moment(fatura.duzenlemeTarihi).format('YYYY-MM-DD')

        let ubl         = EFaturaHelper.makeUbl(fatura)
        console.log(fatura)
        console.log(ubl)
        console.log(fatura.gonderen.vkn)
        let preview     = await getOnIzleme({
            vergiTcKimlikNo : fatura.gonderen.vkn,
            veri            : Buffer.from(ubl).toString('base64'), //'FATURA_UBL',
            belgeFormati    : 'HTML' // PDF, HTML, UBL
        })
        //console.log(preview)
        return preview
    }
}

exports.gidenBelgeleriIndir = async (tenant, belgeOid) => {
    try {
        if (!lastLoginDate) {
            client = await this.login(
                tenant.efaturaUrl,
                tenant.efaturaUser,
                tenant.efaturaPwd
            )
        }
        let belgeler = await _gidenBelgeleriIndir({vergiTcKimlikNo: tenant.vkn, belgeOidListesi: [belgeOid], belgeTuru: 'FATURA', belgeFormati: 'PDF'})
        //console.log(belgeler)
    }
    catch( e) {
        console.log(e)
    }

}