const pdf = require("pdfjs");
const fs = require("fs");
const moment = require("moment");

exports.createToplantiPdf = async (toplanti, kararlar) => {
    const font = new pdf.Font(
        fs.readFileSync("./fonts/open-sans/OpenSans-Regular.ttf")
    );
    const fontBold = new pdf.Font(
        fs.readFileSync("./fonts/open-sans/OpenSans-Semibold.ttf")
    );
    const fontLigth = new pdf.Font(
        fs.readFileSync("./fonts/open-sans/OpenSans-Light.ttf")
    );
    const src = fs.readFileSync("./experto-logo.jpg");
    const logo = new pdf.Image(src);
    var doc = new pdf.Document({
        font: font,
        properties: {
            author: "suyuti",
        },
    });

    //--- Header
    var headerRow = doc
        .header()
        .table({ widths: [null, null] })
        .row();
    headerRow.cell().image(logo, { height: 0.8 * pdf.cm });
    headerRow
        .cell()
        .text({ textAlign: "right" })
        .add(`${toplanti.key}`, { fontSize: 8 });

    doc.footer().pageNumber(function (curr, total) { return curr + ' / ' + total }, { textAlign: 'center' })


    doc.cell("Toplantı Notları", {
        textAlign: "center",
        fontSize: 18,
        font: fontBold,
        paddingBottom: 1 * pdf.cm,
    });

    var toplantiBilgileri = doc.table({ widths: [60, null, 60, null] });
    var row1 = toplantiBilgileri.row();
    row1.cell("Firma", { font: fontBold });
    if (toplanti.icToplanti) {
        row1.cell("İç Toplantı", { font: fontLigth });
    }
    else {
        row1.cell(toplanti.firma.marka, { font: fontLigth });
    }
    row1.cell("Tarih", { font: fontBold });
    row1.cell(`${moment(toplanti.start).format('DD.MM.yyyy')} ${toplanti.saat}`, { font: fontLigth });
    var row2 = toplantiBilgileri.row();
    row2.cell("Konu", { font: fontBold });
    row2.cell(toplanti.baslik, { font: fontLigth });
    row2.cell("Tür", { font: fontBold });
    row2.cell(toplanti.tur, { font: fontLigth });
    var row3 = toplantiBilgileri.row();
    row3.cell("Kategori", { font: fontBold });
    row3.cell(toplanti.kategori, { font: fontLigth });
    row3.cell("Konum", { font: fontBold });
    row3.cell(toplanti.yer, { font: fontLigth });

    doc.cell(' ', { paddingTop: 1 * pdf.cm })

    var katilimciBilgileri = doc.table({ widths: [150, null] });
    let eKatilimcilar = toplanti.organizerKatilimcilar.map(o => o.username).join(', ')
    //let eKatilimcilar = toplanti.expertoKatilimcilarDisplayName && toplanti.expertoKatilimcilarDisplayName.map(ek => `${ek}`).join(',  ')

    var rowExperto = katilimciBilgileri.row();
    rowExperto.cell("Experto Katılımcılar", { font: fontBold });
    rowExperto.cell(eKatilimcilar, { font: fontLigth, fontSize: 10 });

    var rowFirma = katilimciBilgileri.row();
    let fKatilimcilar = toplanti.firmaKatilimcilar.map(u => ` ${u.adi} ${u.soyadi}`).join(', ')
    //let fKatilimcilar = firmaKatilimcilar.map(fk => ` ${fk.adi} ${fk.soyadi}`).join(',  ')
    rowFirma.cell("Firma Katılımcılar", { font: fontBold });
    rowFirma.cell(fKatilimcilar, { font: fontLigth, fontSize: 10 });

    doc.cell(' ', { paddingTop: 1 * pdf.cm })

    var toplantiKararlari = doc.table({
        widths: [1.5 * pdf.cm, null, 3 * pdf.cm, 3 * pdf.cm, 3 * pdf.cm, 2.5 * pdf.cm],
        borderHorizontalWidths: function (i) {
            return i < 2 ? 1 : 0.1;
        },
        padding: 5,
    });
    var toplantiKararlariHeader = toplantiKararlari.header({
        font: fontBold,//require("pdfjs/font/Helvetica"),
        borderBottomWidth: 1.5,
    });
    toplantiKararlariHeader.cell("No");
    toplantiKararlariHeader.cell("Karar");
    toplantiKararlariHeader.cell("Experto İlgili");
    toplantiKararlariHeader.cell("Firma İlgili");
    toplantiKararlariHeader.cell("Son Tarih");
    toplantiKararlariHeader.cell("Durum");

    kararlar.map((k, i) => {
        var row = toplantiKararlari.row();
        row.cell((i + 1).toString(), { fontSize: 10, font: fontLigth });
        row.cell(k.karar, { fontSize: 10, font: fontLigth });
        row.cell(k.organizerSorumlu.username, { fontSize: 10, font: fontLigth }); // Unvansiz firma kisi
        row.cell(k.firmaSorumlu? `${k.firmaSorumlu.adi} ${k.firmaSorumlu.soyadi}`:'', { fontSize: 10, font: fontLigth }); // Unvansiz firma kisi
        row.cell(moment(k.tarih).format('DD.MM.YYYY'), { fontSize: 10, font: fontLigth });
        row.cell(k.durum, { fontSize: 10, font: fontLigth });
    });
    return doc.asBuffer();

}