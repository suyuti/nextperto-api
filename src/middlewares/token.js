const jwt = require("jsonwebtoken");
const SECRET = process.env.SECRET || 'sdfsdfasdufybwoqybfewfWEFweqFG#$G43gerwg32@%3Y54Y56y%$g3gr^5h5hreweww32213##@R@##$F@D'
const UserService = require('../services/user.service')

let publicUrls = [
    '/api/v1/auth/login',
    '/api/v1/auth/register',
    '/api/v1/auth/session',
    '/socket.io/'
]

exports.tokenControl = async (req, res, next) => {
    if (publicUrls.includes(req.path)) {
        next()
        return
    }

    if (!req.headers.authorization) {
        console.log(`TOKEN yok: ${req.path}`)
        return
    }
    const token = req.headers.authorization.replace("Bearer ", "").trim();
    if (token) {
        jwt.verify(token, SECRET, (err, data) => {
            if (!err) {
                UserService.findOne({_id: data.id, oid: data.oid}).then(user => {
                    if (user) {
                        req.user = user
                        //req.permissions = data.permissions
                        next()
                    }
                })
            }
        })
    }
}