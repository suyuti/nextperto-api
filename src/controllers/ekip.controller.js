const EkipService = require('../services/ekip.service')

exports.getAll = async (req, res) => {
    let ekipler = await EkipService.find(req.query)
    return res.status(200).json({error: null, data: ekipler})
}

exports.getById = async (req, res) => {
    let id = req.params.id
    let ekip = await EkipService.findOne(id, req.query)
    return res.status(200).json({error: null, data: ekip})
}

exports.create = async (req, res) => {
    let data = req.body
    let ekip = await EkipService.create(data, req.user)
    return res.status(200).json({error: null, data: ekip})
}

exports.update = async (req, res) => {
    let id = req.params.id
    let data = req.body
    let ekip = await EkipService.update(id, data, req.user)
    return res.status(200).json({error: null, data: ekip})
}

exports.remove = async (req, res) => {
    let id = req.params.id
    return res.status(200).json({error: null, data: null})
}
