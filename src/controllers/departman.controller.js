const DepartmanService = require('../services/departman.service')

exports.getAll = async (req, res) => {
    let departmanlar = await DepartmanService.find(req.query)
    return res.status(200).json({error: null, data: departmanlar})
}

exports.getById = async (req, res) => {
    let id = req.params.id
    let departman = await DepartmanService.findOne(id, req.query)
    return res.status(200).json({error: null, data: departman})
}

exports.create = async (req, res) => {
    let data = req.body
    let departman = await DepartmanService.create(data)
    return res.status(200).json({error: null, data: departman})
}

exports.update = async (req, res) => {
    let id = req.params.id
    let data = req.body
    return res.status(200).json({error: null, data: null})
}

exports.remove = async (req, res) => {
    let id = req.params.id
    return res.status(200).json({error: null, data: null})
}
