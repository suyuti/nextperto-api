const UrunService = require('../services/urun.service')

exports.getAll = async (req, res) => {
    let urunler = await UrunService.find(req.query)
    return res.status(200).json({error: null, data: urunler})
}

exports.getById = async (req, res) => {
    let id = req.params.id
    let urun = await UrunService.findOne(id, req.query)
    return res.status(200).json({error: null, data: urun})
}

exports.create = async (req, res) => {
    let data = req.body
    let urun = await UrunService.create(data, req.user)
    return res.status(200).json({error: null, data: urun})
}

exports.update = async (req, res) => {
    let id = req.params.id
    let data = req.body
    let urun = await UrunService.update(id, data, req.user)
    return res.status(200).json({error: null, data: urun})
}

exports.remove = async (req, res) => {
    let id = req.pararms.id
    return res.status(200).json({error: null, data: null})
}
