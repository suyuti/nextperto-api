const EFaturaProvider = require('../providers/efatura.provider')
const EFaturaHelper = require('../helpers/fatura.helper')
const TenantModel = require('../models/tenant.model')

exports.getEFaturaOnIzleme = async (req, res) => {
    let fatura = req.body
    let _fatura = await EFaturaHelper.fatura_to_eFatura(fatura)
    let tenant = await TenantModel.findOne({key: fatura.vkesenFirma}).lean(true).exec()  
    let result = await EFaturaProvider.onIzleme(_fatura, tenant)
    return res.status(200).json({error: null, data: result})
}