const FaturaService = require('../services/fatura.service')
const TenantService = require('../services/tenant.service')
const EFaturaProvider = require('../providers/efatura.provider')
//const zlib = require('zlib')
const fs = require('fs')
var AdmZip = require('adm-zip');

exports.getAll = async (req, res) => {
    let faturalar = await FaturaService.find(req.query)
    return res.status(200).json({error: null, data: faturalar})
}

exports.getById = async (req, res) => {
    let id = req.params.id
    let fatura = await FaturaService.findOne(id, req.query)
    return res.status(200).json({error: null, data: fatura})
}

exports.create = async (req, res) => {
    let data = req.body
    let fatura = await FaturaService.create(data)
    return res.status(200).json({error: null, data: fatura})
}

exports.update = async (req, res) => {
    let id = req.params.id
    let data = req.body
    let gorev = await GorevService.update(id, data, req.user)
    return res.status(200).json({error: null, data: gorev})
}

exports.remove = async (req, res) => {
    let id = req.params.id
    return res.status(200).json({error: null, data: null})
}

exports.getHistory = async (req, res) => {
    let id = req.params.id
    let histories = await GorevService.getHistory(id)
    return res.status(200).json({error: null, data: histories})
}

exports.getKalemler = async (req, res) => {
    let id = req.params.id
    let kalemler = await FaturaService.getKalemler(id)
    return res.status(200).json({error: null, data: kalemler})
}

exports.addKalem = async (req, res) => {
    let id = req.params.id
    return res.status(200).json({error: null, data: null})
}

exports.updateKalem = async (req, res) => {
    let id = req.params.id
    return res.status(200).json({error: null, data: null})
}

exports.removeKalem = async (req, res) => {
    let id = req.params.id
    return res.status(200).json({error: null, data: null})
}

exports.getEFaturaOnIzleme = async (req, res) => {
    let id = req.params.id
    return res.status(200).json({error: null, data: null})
}

function unzip(str) {
    return new Promise((res, rej) => {
        // we read in the base64
        const data = Buffer.from(str, "base64");

        //fs.writeFileSync('test.zip', data)

        // we create a transform string to gunzip it
        const gzip = zlib.createGunzip();
        // initialize ret with an empty buffer
        let ret = Buffer.alloc(0);
        gzip.on("data", function(dat) {
            // concatenate the data to ret
            ret = Buffer.concat([ret, dat], res.length + dat.length);
        });
        gzip.on("error", function(err) {
            rej(err);
        });
        gzip.on("end", function() {
            // we're done, resolve the promise
            res(ret);
        });
        // send the data to the gunzip stream
        //gzip.end(data);
        gzip.end(fs.createReadStream('./test.zip'));
    });
}

exports.getBelge = async (req, res) => {
    let id = req.params.id
    let fatura = await FaturaService.findOne(id)
   
    let buf = Buffer.from(fatura.gidenBelgePdf, 'base64')
    var zip = new AdmZip(buf);
	var zipEntries = zip.getEntries(); // an array of ZipEntry records

	zipEntries.forEach(function(zipEntry) {
        return res.end(zipEntry.getData())
	});
}

exports.getBelgeIndir = async (req, res) => {
    let id = req.params.id
    let fatura = await FaturaService.findOne(id)
    let tenant = await TenantService.findOne({key:fatura.vkesenFirma})
    let belge = await EFaturaProvider.gidenBelgeleriIndir(tenant, fatura.belgeOid)
    return res.status(200).json({error: null, data: null})
    //let buf = Buffer.from(fatura.belgeXml, 'base64')
    //return res.end(buf)
}


exports.yenidenGonder = async (req, res) => {
    let id = req.params.id
    let fatura = await FaturaService.findOne(id)
    await FaturaService.yenidenGonder(fatura)
    return res.status(200).json({error: null, data: null})
}