const OdemePlanService = require('../services/odemeplan.service')

exports.getAll = async (req, res) => {
    let odemePlanlari = await OdemePlanService.find(req.query)
    return res.status(200).json({error: null, data: odemePlanlari})
}

exports.getById = async (req, res) => {
    let id = req.params.id
    let odemePlan = await OdemePlanService.findOne(id, req.query)
    return res.status(200).json({error: null, data: odemePlan})
}

exports.create = async (req, res) => {
    let data = req.body
    let odemePlan = await OdemePlanService.create(data, req.user)
    return res.status(200).json({error: null, data: odemePlan})
}

exports.update = async (req, res) => {
    let id = req.params.id
    let data = req.body
    let odemePlan = await OdemePlanService.update(id, data, req.user)
    return res.status(200).json({error: null, data: odemePlan})
}

exports.remove = async (req, res) => {
    let id = req.params.id
    return res.status(200).json({error: null, data: null})
}
