const OdemeService = require('../services/odeme.service')

exports.getAll = async (req, res) => {
    let odemeler = await OdemeService.find(req.query)
    return res.status(200).json({error: null, data: odemeler})
}

exports.getById = async (req, res) => {
    let id = req.params.id
    let odeme = await OdemeService.findOne(id, req.query)
    return res.status(200).json({error: null, data: odeme})
}

exports.create = async (req, res) => {
    let data = req.body
    let odeme = await OdemeService.create(data, req.user)
    return res.status(200).json({error: null, data: odeme})
}

exports.update = async (req, res) => {
    let id = req.params.id
    let data = req.body
    let odeme = await OdemeService.update(id, data, req.user)
    return res.status(200).json({error: null, data: odeme})
}

exports.remove = async (req, res) => {
    let id = req.params.id
    return res.status(200).json({error: null, data: null})
}
