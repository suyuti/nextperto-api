const OrtakTakvimService = require('../services/ortaktakvim.service')

exports.getOrtakTakvim = async (req, res) => {
    let form = req.body
    let events = await OrtakTakvimService.getEvents(form)
    return res.status(200).json({error: null, data: events})
}
