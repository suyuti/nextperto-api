const TedarikciService = require('../services/tedarikci.service')

exports.getAll = async (req, res) => {
    let tedarikciler = await TedarikciService.find(req.query)
    return res.status(200).json({ error: null, data: tedarikciler })
}

exports.getById = async (req, res) => {
    let id = req.params.id
    let tedarikci = await TedarikciService.findOne(id, req.query)
    return res.status(200).json({ error: null, data: tedarikci })
}

exports.create = async (req, res) => {
    let data = req.body
    let tedarikci = await TedarikciService.create(data)
    return res.status(200).json({ error: null, data: tedarikci })
}

exports.update = async (req, res) => {
    let id = req.params.id
    let data = req.body
    let tedarikci = await TedarikciService.update(id, data, req.user)
    return res.status(200).json({ error: null, data: tedarikci })
}

exports.remove = async (req, res) => {
    let id = req.params.id
    return res.status(200).json({ error: null, data: null })
}
