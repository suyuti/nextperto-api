const HistoryModel = require('../models/history.model')

exports.getByRefId = async (req, res) => {
    let refid = req.params.refid
    let history = await HistoryModel
        .find({referenceItem: refid})
        .populate('createdBy')
        .sort('-createdAt')
        .lean(true)
        .exec()
    return res.status(200).json({error: null, data: history})
}