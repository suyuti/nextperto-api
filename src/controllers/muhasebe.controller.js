const MuhasebeService = require('../services/muhasebe.service')

exports.getFirmalar = async (req, res) => {
    let firmalar = await MuhasebeService.getFirmalar(req.query)
    return res.status(200).json({error: null, data: firmalar})
}

exports.getTahsilatlar = async (req, res) => {
    let firmalar = await MuhasebeService.getTahsilatlar(req.query)
    return res.status(200).json({error: null, data: firmalar})
}

exports.getRaporFatura = async (req, res) => {
    let query = req.body
    let data = await MuhasebeService.getRaporFatura(query)
    return res.status(200).json({error: null, data: data})
}

exports.getRaporUrun = async (req, res) => {
    let data = await MuhasebeService.getRaporUrun()
    return res.status(200).json({error: null, data: data})
}

exports.getRaporUrunAylik = async (req, res) => {
    let data = await MuhasebeService.getRaporUrunAylik()
    return res.status(200).json({error: null, data: data})
}
