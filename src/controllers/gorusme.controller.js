const GorusmeService = require('../services/gorusme.service')

exports.getAll = async (req, res) => {
    let gorusmeler = await GorusmeService.find(req.query)
    return res.status(200).json({error: null, data: gorusmeler})
}

exports.getById = async (req, res) => {
    let id = req.params.id
    let gorusme = await GorusmeService.findOne(id, req.query)
    return res.status(200).json({error: null, data: gorusme})
}

exports.create = async (req, res) => {
    let data = req.body
    let gorusme = await GorusmeService.create(data, req.user)
    return res.status(200).json({error: null, data: gorusme})
}

exports.update = async (req, res) => {
    let id = req.params.id
    let data = req.body
    let gorusme = await GorusmeService.update(id, data, req.user)
    return res.status(200).json({error: null, data: gorusme})
}

exports.remove = async (req, res) => {
    let id = req.params.id
    return res.status(200).json({error: null, data: null})
}
