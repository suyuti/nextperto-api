const GorevService = require('../services/gorev.service')
const AmqpProvider = require('../providers/amqp.provider')

exports.getAll = async (req, res) => {
    let gorevler = await GorevService.find(req.query)
    return res.status(200).json({error: null, data: gorevler})
}

exports.getById = async (req, res) => {
    let id = req.params.id
    let gorev = await GorevService.findOne(id, req.query)
    return res.status(200).json({error: null, data: gorev})
}

exports.create = async (req, res) => {
    let data = req.body
    let gorev = await GorevService.create(data, req.user)
    return res.status(200).json({error: null, data: gorev})
}

exports.update = async (req, res) => {
    let id = req.params.id
    let data = req.body
    let gorev = await GorevService.update(id, data, req.user)
    return res.status(200).json({error: null, data: gorev})
}

exports.remove = async (req, res) => {
    let id = req.params.id
    return res.status(200).json({error: null, data: null})
}

exports.getHistory = async (req, res) => {
    let id = req.params.id
    let histories = await GorevService.getHistory(id)
    return res.status(200).json({error: null, data: histories})
}

exports.tamamla = async (req, res) => {
    let id = req.params.id
    let tamamlaDto = req.body

    let gorev = await GorevService.tamamla(id, tamamlaDto, req.user)


    //let gorev = await GorevService.update(id, tamamlaDto, req.user)
    return res.status(200).json({error: null, data: gorev})
}


exports.getYorumlar = async (req, res) => {
    let id = req.params.id
    let yorumlar = await GorevService.getYorumlar(id, req.user)
    return res.status(200).json({error: null, data: yorumlar})
}

exports.postYorum = async (req, res) => {
    let id = req.params.id
    let yorumData = req.body
    let yorum = await GorevService.addYorum(yorumData, req.user)
    return res.status(200).json({error: null, data: yorum})
}
