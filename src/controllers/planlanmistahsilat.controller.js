const PlanlanmisTahsilatService = require('../services/planlanmisTahsilat.service')

exports.getAll = async (req, res) => {
    let planlanmisTahsilatlar = await PlanlanmisTahsilatService.find(req.query)
    return res.status(200).json({error: null, data: planlanmisTahsilatlar})
}

exports.getById = async (req, res) => {
    let id = req.params.id
    let planlanmisTahsilat = await PlanlanmisTahsilatService.findOne(id, req.query)
    return res.status(200).json({error: null, data: planlanmisTahsilat})
}

exports.create = async (req, res) => {
    let data = req.body
    let planlanmisTahsilat = await PlanlanmisTahsilatService.create(data, req.user)
    return res.status(200).json({error: null, data: planlanmisTahsilat})
}

exports.update = async (req, res) => {
    let id = req.params.id
    let data = req.body
    let planlanmisTahsilat = await PlanlanmisTahsilatService.update(id, data, req.user)
    return res.status(200).json({error: null, data: planlanmisTahsilat})
}

exports.remove = async (req, res) => {
    let id = req.params.id
    return res.status(200).json({error: null, data: null})
}

exports.getHistory = async (req, res) => {
    let id = req.params.id
    let histories = await PlanlanmisTahsilatService.getHistory(id)
    return res.status(200).json({error: null, data: histories})
}
