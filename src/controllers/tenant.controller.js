const TenantService = require('../services/tenant.service')
const AmqpService = require('../providers/amqp.provider')
exports.getAll = async (req, res) => {
    let tenantler = await TenantService.find(req.query)
    return res.status(200).json({error: null, data: tenantler})
}

exports.getById = async (req, res) => {
    let id = req.params.id
    let tenant = await TenantService.findOne(id)
    return res.status(200).json({error: null, data: tenant})
}

exports.create = async (req, res) => {
    let data = req.body
    let tenant = await TenantService.create(data)
    return res.status(200).json({error: null, data: tenant})
}

exports.update = async (req, res) => {
    let id = req.params.id
    let data = req.body
    let tenant = await TenantService.update(id, data)
    return res.status(200).json({error: null, data: tenant})
}

exports.remove = async (req, res) => {
    let id = req.params.id
    return res.status(200).json({error: null, data: null})
}

exports.getNakitAkisi = async (req, res) => {
    let id = req.params.id
    let nakitAkisi = await TenantService.nakitAkisi(id)
    return res.status(200).json({error: null, data: nakitAkisi})
}

exports.getToplamlar = async (req, res) => {
    let id = req.params.id
    let tenant = await TenantService.findOne({_id: id})
    let nakitAkisi = await TenantService.toplamlar(tenant)
    return res.status(200).json({error: null, data: nakitAkisi})
}

exports.gidenBelgeleriSorgula = async (req, res) => {
    let id = req.params.id
    let tenant = await TenantService.findOne({_id: id})
    let resp = await AmqpService.sendFatura({command: 'gidenBelgeleriSorgula', tenant: tenant.key})

    return res.status(200).json({error: null, data: null})
}

exports.gerceklesenTahsilatlar = async (req, res) => {
    let id = req.params.id
    let data = await TenantService.gerceklesenTahsilatlat(id)
    return res.status(200).json({error: null, data: data})
}

exports.gecikenTahsilatlar = async (req, res) => {
    let id = req.params.id
    let data = await TenantService.gecikenTahsilatlar(id)
    return res.status(200).json({error: null, data: data})
}

exports.planlananTahsilatlar = async (req, res) => {
    let id = req.params.id
    let data = await TenantService.planlananTahsilatlar(id)
    return res.status(200).json({error: null, data: data})
}

exports.gelecekTahsilatlar = async (req, res) => {
    let id = req.params.id
    let data = await TenantService.gelecekTahsilatlar(id)
    return res.status(200).json({error: null, data: data})
}
