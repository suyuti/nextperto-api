const UserService = require('../services/user.service')
const EkipService = require('../services/ekip.service')

exports.getAll = async (req, res) => {
    let users = await UserService.find(req.query)
    return res.status(200).json({error: null, data: users})
}

exports.getById = async (req, res) => {
    let id = req.params.id
    let user = await UserService.findOne({_id: id, ...req.query}) // attention! special call
    return res.status(200).json({error: null, data: user})
}

exports.create = async (req, res) => {
    let data = req.body
    let user = await UserService.create(data)
    return res.status(200).json({error: null, data: user})
}

exports.update = async (req, res) => {
    let id = req.params.id
    let data = req.body
    let user = await UserService.update(id, data)
    return res.status(200).json({error: null, data: user})
}

exports.remove = async (req, res) => {
    let id = req.params.id
    return res.status(200).json({error: null, data: null})
}

exports.getAvailableUsers = async (req, res) => {
    let id = req.params.id
    return res.status(200).json({error: null, data: null})
}

exports.getAvailableDepartmen = async (req, res) => {
    let id = req.params.id
    return res.status(200).json({error: null, data: null})
}

exports.getMyTeam = async (req, res) => {
    let id = req.params.id
    return res.status(200).json({error: null, data: null})
}

exports.getMyManager = async (req, res) => {
    let id = req.params.id
    return res.status(200).json({error: null, data: null})
}

exports.getMyColleauges = async (req, res) => {
    let id = req.params.id
    return res.status(200).json({error: null, data: null})
}

exports.getEkibim = async (req, res) => {
    let id = req.params.id
    let ekibim = await EkipService.getEkibim(req.user)
    return res.status(200).json({error: null, data: ekibim})
}