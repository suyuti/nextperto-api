const ToplantiService = require('../services/toplanti.service')
const ToplantiKararService = require('../services/toplantiKarar.service')
const GorevService = require('../services/gorev.service')
const PdfProvider = require('../providers/pdf.provider')

exports.getAll = async (req, res) => {
    let toplantilar = await ToplantiService.find(req.query)
    return res.status(200).json({error: null, data: toplantilar})
}

exports.getById = async (req, res) => {
    let id = req.params.id
    let toplanti = await ToplantiService.findOne(id, req.query)
    return res.status(200).json({error: null, data: toplanti})
}

exports.create = async (req, res) => {
    let data = req.body
    data.createdBy = req.user._id
    let toplanti = await ToplantiService.create(data, req.user)
    return res.status(200).json({error: null, data: toplanti})
}

exports.update = async (req, res) => {
    let id = req.params.id
    let data = req.body
    let toplanti = await ToplantiService.update(id, data, req.user)
    return res.status(200).json({error: null, data: toplanti})
}

exports.remove = async (req, res) => {
    let id = req.params.id
    return res.status(200).json({error: null, data: null})
}

exports.getKararlar = async (req, res) => {
    let id = req.params.id
    let toplanti = await ToplantiService.findOne(id)
    let kararlar = await ToplantiKararService.find(`vToplanti=${toplanti.key}`)
    return res.status(200).json({error: null, data: kararlar})
}

exports.addKararlar = async (req, res) => {
    let id = req.params.id
    let data = req.body
    let kararlar = await ToplantiKararService.addOrCreateKararlar(id, data)
    return res.status(200).json({error: null, data: kararlar})
}

exports.getHistory = async (req, res) => {
    let id = req.params.id
    let histories = await ToplantiService.getHistory(id)
    return res.status(200).json({error: null, data: histories})
}

exports.getGorevler = async (req, res) => {
    let id = req.params.id
    let toplanti = await ToplantiService.findOne(id)
    let gorevler = await GorevService.find(`vtoplanti=${toplanti.key}`)
    return res.status(200).json({error: null, data: gorevler})
}

exports.tamamla = async (req, res) => {
    let id = req.params.id
    let tamamlaDto = req.body
    let toplanti = await ToplantiService.tamamla(id, tamamlaDto, req.user)
    //let gorevler = await ToplantiService.find(`toplanti=${id}`)
    return res.status(200).json({error: null, data: null})
}

exports.getPdfRapor = async (req, res) => {
    let id = req.params.id

    let toplanti = await ToplantiService.find(`skip=0&limit=1&_id=${id}&populate=firma.marka,organizerKatilimcilar.username,firmaKatilimcilar.adi,firmaKatilimcilar.soyadi`)

    let kararlar = await ToplantiKararService.find(`skip=0&limit=100&vToplanti=${toplanti.data[0].key}&populate=firmaSorumlu,organizerSorumlu.username`) 
    let pdf = await PdfProvider.createToplantiPdf(toplanti.data[0], kararlar.data)
    res.end(pdf)
}

exports.getToplantiYapilmamisFirmalar = async (req, res) => {
    let result = await ToplantiService.toplantiYapilmamisFirmalar()
    return res.status(200).json({error: null, data: result})
}

exports.getToplantiYapilmisFirmalar = async (req, res) => {
    let result = await ToplantiService.toplantiYapilmisFirmalar()
    return res.status(200).json({error: null, data: result})
}


