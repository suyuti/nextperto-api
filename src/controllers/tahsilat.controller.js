const TahsilatService = require('../services/tahsilat.service')
const FaturaService = require('../services/fatura.service')

exports.getAll = async (req, res) => {
    let tahsilatlar = await TahsilatService.find(req.query)
    return res.status(200).json({error: null, data: tahsilatlar})
}

exports.getById = async (req, res) => {
    let id = req.params.id
    let tahsilat = await TahsilatService.findOne(id)
    return res.status(200).json({error: null, data: tahsilat})
}

exports.create = async (req, res) => {
    let data = req.body
    let tahsilat = await TahsilatService.create(data, req.user)
    FaturaService.tahsilatYap(data)
    return res.status(200).json({error: null, data: tahsilat})
}

exports.update = async (req, res) => {
    let id = req.params.id
    let data = req.body
    let tahsilat = await TahsilatService.update(id, data, req.user)
    return res.status(200).json({error: null, data: tahsilat})
}

exports.remove = async (req, res) => {
    let id = req.params.id
    return res.status(200).json({error: null, data: null})
}

exports.getHistory = async (req, res) => {
    let id = req.params.id
    let histories = await TahsilatService.getHistory(id)
    return res.status(200).json({error: null, data: histories})
}
