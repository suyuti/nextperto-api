//const RoleService = require('../services/role.service')

const permissions = [
    {label:'Aktif Müşteri',         key: 'aktifmusteri'},
    {label:'Pasif Müşteri',         key: 'pasifmusteri'},
    {label:'Sorunlu Müşteri',       key: 'sorunlumusteri'},
    {label:'Potansiyel Müşteri',    key: 'potansiyelmusteri'},
    {label:'Kişi',                  key: 'kisi'},
    {label:'Görüşme',               key: 'gorusme'},
    {label:'Görev',                 key: 'gorev'},
    {label:'Toplantı',              key: 'toplanti'},
    {label:'Muhasebe',              key: 'muhasebe'},
    {label:'Muhasebe Güncel',       key: 'muhasebe_guncel'},
    {label:'Fatura',                key: 'fatura'},
    {label:'Toplu Fatura',          key: 'topluFatura'},
    {label:'Muhasebe Müşteriler',   key: 'muhasebe_musteriler'},
    {label:'Satış Raporları',       key: 'satisRapor'},
    {label:'Tahsilat Raporları',    key: 'tahsilatRapor'},
    {label:'Gelir-Gider Raporları', key: 'gelirGiderRapor'},
    {label:'Gider',                 key: 'gider'},
    {label:'Tedarikçiler',          key: 'tedarikciler'},
    {label:'Çalışanlar',            key: 'calisanlar'},
    {label:'Gider Raporu',          key: 'giderRaporu'},
    {label:'Ödemeler Raporu',       key: 'odemelerRaporu'},
    {label:'KDV Raporu',            key: 'kdvRaporu'},
    {label:'Tahsilat',              key: 'tahsilat'},
    {label:'Tahsilat Durumu',       key: 'tahsilatDurumu'},
    {label:'Tahsilat Planlama',     key: 'tahsilatPlan'},
    {label:'Toplantı Rapor',        key: 'toplantiRapor'},
    {label:'Ürün',                  key: 'urun'},
    {label:'Ürün Rapor',            key: 'urunRapor'},
    {label:'Onay',                  key: 'onay'},
]

exports.getAll = async (req, res) => {
    return res.status(200).json({error: null, data: permissions})
}

exports.getById = async (req, res) => {
    let id = req.params.id
    return res.status(200).json({error: 'not implemented', data: null})
}

exports.create = async (req, res) => {
    let data = req.body
    return res.status(200).json({error: 'not implemented', data: null})
}

exports.update = async (req, res) => {
    let id = req.params.id
    let data = req.body
    return res.status(200).json({error: 'not implemented', data: null})
}

exports.remove = async (req, res) => {
    let id = req.params.id
    return res.status(200).json({error: 'not implemented', data: null})
}
