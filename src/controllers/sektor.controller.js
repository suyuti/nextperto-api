const SektorService = require('../services/sektor.service')

exports.getAll = async (req, res) => {
    let sektorler = await SektorService.find(req.query)
    return res.status(200).json({error: null, data: sektorler})
}

exports.getById = async (req, res) => {
    let id = req.params.id
    let sektor = await SektorService.findOne(id, req.query)
    return res.status(200).json({error: null, data: sektor})
}

exports.create = async (req, res) => {
    let data = req.body
    let sektor = await SektorService.create(data, req.user)
    return res.status(200).json({error: null, data: sektor})
}

exports.update = async (req, res) => {
    let id = req.params.id
    let data = req.body
    let sektor = await SektorService.update(id, data, req.user)
    return res.status(200).json({error: null, data: sektor})
}

exports.remove = async (req, res) => {
    let id = req.params.id
    return res.status(200).json({error: null, data: null})
}

exports.getHistory = async (req, res) => {
    let id = req.params.id
    let histories = await SektorService.getHistory(id)
    return res.status(200).json({error: null, data: histories})
}

exports.tamamla = async (req, res) => {
    let id = req.params.id
    return res.status(200).json({error: null, data: null})
}
