const FirmaService = require('../services/firma.service')
const KisiService = require('../services/kisi.service')
const FaturaService = require('../services/fatura.service')
const TahsilatService = require('../services/tahsilat.service')

exports.getAll = async (req, res) => {
    let firmalar = await FirmaService.find(req.query)
    return res.status(200).json({error: null, data: firmalar})
}

exports.getById = async (req, res) => {
    let id = req.params.id
    let firma = await FirmaService.findOne(id, req.query)
    return res.status(200).json({error: null, data: firma})
}

exports.create = async (req, res) => {
    let data = req.body
    let firma = await FirmaService.create(data)
    return res.status(200).json({error: null, data: firma})
}

exports.update = async (req, res) => {
    let id = req.params.id
    let data = req.body
    let firma = await FirmaService.update(id, data)
    return res.status(200).json({error: null, data: firma})
}

exports.remove = async (req, res) => {
    let id = req.params.id
    return res.status(200).json({error: null, data: null})
}

exports.getKisiler = async (req, res) => {
    let id = req.params.id
    let query = req.query || {}
    if (!id) { // Ic toplantilarda firma NULL geliyor.
        return res.status(200).json({error: null, data: []})
    }
    let firma = await FirmaService.findOne(id)
    query.vfirma = firma.key
    //let kisiler = await KisiService.find(`vfirma=${firma.key}&${query}`)
    let kisiler = await KisiService.find(query)
    return res.status(200).json({error: null, data: kisiler})
}

exports.getFaturalarToplam = async (req, res) => {
    let id = req.params.id
    try {
        let faturalar = await FaturaService.find({vfirma: id})
        let toplam = faturalar.data.reduce((acc, v) => { return acc + v.genelToplam}, 0)
        return res.status(200).json({error: null, data: toplam})
    }
    catch(e) {
        console.log(e)
    }
}

exports.getTahsilatlarToplam = async (req, res) => {
    let id = req.params.id
    try {
        let tahsilatlar = await TahsilatService.find({vfirma: id})
        let toplam = tahsilatlar.data.reduce((acc, v) => { return acc + v.tutar}, 0)
        return res.status(200).json({error: null, data: toplam})
    }
    catch(e) {
        console.log(e)
    }
}

