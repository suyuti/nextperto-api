const StatsService = require('../services/stats.service')
const FaturaService = require('../services/fatura.service')
const TopluFaturaService = require('../services/toplufatura.service')
const MuhasebeHelper = require('../helpers/muhasebe.helper')

exports.getGorev = async (req, res) => {
    let gorevStatsDto = await StatsService.getGorev(req.query)
    return res.status(200).json({error: null, data: gorevStatsDto})
}

// Bugun, Bu hafta yapilacak tahsilatlari doner
exports.getYapilacakTahsilatlar = async (req, res) => {
    let tahsilatlarDto = await StatsService.getBugunYapilacakTahsilatlar(req.query)
    return res.status(200).json({error: null, data: tahsilatlarDto})
}

// Bugun kesilecek faturalari doner
exports.getGundeKesilecekFaturalar = async (req, res) => {
    let {gun} = req.body
    let faturalar = await TopluFaturaService.gundeKesilecekFaturalar(gun)
    return res.status(200).json({error: null, data: faturalar})
}

exports.getGelecekTahsilatlar = async (req, res) => {
    let {count, type} = req.query
    let rapor = await StatsService.getGelecekTahsilatlar(count)
    return res.status(200).json({error: null, data: rapor})
}

exports.getTahsilatRapor = async (req, res) => {
    let rapor = await StatsService.tahsilatRapor()
    return res.status(200).json({error: null, data: rapor})
}

exports.getAlacaklar = async (req, res) => {
    let rapor = await StatsService.getAlacaklar(6, req.query)
    return res.status(200).json({error: null, data: rapor})
}

exports.getOdemeler = async (req, res) => {
    let rapor = await StatsService.getAlacaklar(6)           // TODO Odemeler icin yapilacak
    return res.status(200).json({error: null, data: rapor})
}

exports.getGunlukRapor = async (req, res) => {
    let pdf = await MuhasebeHelper.gunlukRapor()
    res.end(pdf)
}

