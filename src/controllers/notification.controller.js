exports.getAll = async (req, res) => {
    return res.status(200).json({error: null, data: null})
}

exports.getById = async (req, res) => {
    let id = req.params.id
    return res.status(200).json({error: null, data: null})
}

exports.create = async (req, res) => {
    let data = req.body
    return res.status(200).json({error: null, data: null})
}

exports.update = async (req, res) => {
    let id = req.params.id
    let data = req.body
    return res.status(200).json({error: null, data: null})
}

exports.remove = async (req, res) => {
    let id = req.params.id
    return res.status(200).json({error: null, data: null})
}
