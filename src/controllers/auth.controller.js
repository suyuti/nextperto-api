const AuthService = require('../services/auth.service')

exports.login = async (req, res) => {
    let data = req.body
    AuthService.login(data).then(resp => {
        return res.status(201).json({error: null, data: resp})
    }).catch(error => {
        return res.status(401).json({error: null, data: null})
    })
}

exports.register = async (req, res) => {
    return res.status(200).json({error: null, data: null})
}

exports.session = async (req, res) => {
    let data = req.body
    AuthService.session(data).then(resp => {
        return res.status(201).json({error: null, data: resp})
    }).catch(error => {
        return res.status(401).json({error: null, data: null})
    })
}

exports.logout = async (req, res) => {
    return res.status(200).json({error: null, data: null})
}
