const RoleService = require('../services/role.service')

exports.getAll = async (req, res) => {
    let roles = await RoleService.find(req.query)
    return res.status(200).json({error: null, data: roles})
}

exports.getById = async (req, res) => {
    let id = req.params.id
    let role = await RoleService.findOne(id)
    return res.status(200).json({error: null, data: role})
}

exports.create = async (req, res) => {
    let data = req.body
    let role = await RoleService.create(data)
    return res.status(200).json({error: null, data: role})
}

exports.update = async (req, res) => {
    let id = req.params.id
    let data = req.body
    let role = await RoleService.update(id, data)
    return res.status(200).json({error: null, data: role})
}

exports.remove = async (req, res) => {
    let id = req.params.id
    return res.status(200).json({error: null, data: null})
}
