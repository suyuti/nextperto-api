const OnayService = require('../services/onay.service')

exports.getAll = async (req, res) => {
    let onaylar = await OnayService.find(req.query)
    return res.status(200).json({error: null, data: onaylar})
}

exports.getById = async (req, res) => {
    let id = req.params.id
    let onay = await OnayService.findOne(id, req.query)
    return res.status(200).json({error: null, data: onay})
}

exports.onayla = async (req, res) => {
    let id = req.params.id
    let onaylar = await OnayService.onayla(id, req.user, '')
    return res.status(200).json({error: null, data: onaylar})
}

exports.reddet = async (req, res) => {
    let id = req.params.id
    let onaylar = await OnayService.red(id, req.user, '')
    return res.status(200).json({error: null, data: onaylar})
}

exports.iptalet = async (req, res) => {
    let id = req.params.id
    let onaylar = await OnayService.iptal(id, req.user, '')
    return res.status(200).json({error: null, data: onaylar})
}
