const TopluFaturaService = require('../services/toplufatura.service')

exports.getAll = async (req, res) => {
    let listeler = await TopluFaturaService.find(req.query)
    return res.status(200).json({error: null, data: listeler})
}

exports.getById = async (req, res) => {
    let id = req.params.id
    let liste = await TopluFaturaService.findOne(id, req.query)
    return res.status(200).json({error: null, data: liste})
}

exports.create = async (req, res) => {
    let data = req.body
    let liste = await TopluFaturaService.create(data)
    return res.status(200).json({error: null, data: liste})
}

exports.update = async (req, res) => {
    let id = req.params.id
    let data = req.body
    let liste = await TopluFaturaService.update(id, data, req.user)
    return res.status(200).json({error: null, data: liste})
}

exports.remove = async (req, res) => {
    let id = req.params.id
    let liste = await TopluFaturaService.update(id, {deleted: true}, req.user)
    return res.status(200).json({error: null, data: liste})
}

exports.getHistory = async (req, res) => {
    let id = req.params.id
    let histories = await TopluFaturaService.getHistory(id)
    return res.status(200).json({error: null, data: histories})
}
