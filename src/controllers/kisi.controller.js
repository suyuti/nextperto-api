const KisiService = require('../services/kisi.service')

exports.getAll = async (req, res) => {
    let kisiler = await KisiService.find(req.query)
    return res.status(200).json({error: null, data: kisiler})
}

exports.getById = async (req, res) => {
    let id = req.params.id
    let kisi = await KisiService.findOne(id, req.query)
    return res.status(200).json({error: null, data: kisi})
}

exports.create = async (req, res) => {
    let data = req.body
    let kisi = await KisiService.create(data, req.user)
    return res.status(200).json({error: null, data: kisi})
}

exports.update = async (req, res) => {
    let id = req.params.id
    let data = req.body
    let kisi = await KisiService.update(id, data, req.user)
    return res.status(200).json({error: null, data: kisi})
}

exports.remove = async (req, res) => {
    let id = req.pararms.id
    return res.status(200).json({error: null, data: null})
}
