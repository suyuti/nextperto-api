const ToplantiKararService = require('../services/toplantiKarar.service')

exports.getAll = async (req, res) => {
    let toplantilar = await ToplantiKararService.find(req.query)
    return res.status(200).json({error: null, data: toplantilar})
}

exports.getById = async (req, res) => {
    let id = req.params.id
    let toplanti = await ToplantiKararService.findOne(id)
    return res.status(200).json({error: null, data: toplanti})
}

exports.create = async (req, res) => {
    let data = req.body
    let toplanti = await ToplantiKararService.create(data)
    return res.status(200).json({error: null, data: toplanti})
}

exports.update = async (req, res) => {
    let id = req.params.id
    let data = req.body
    let toplanti = await ToplantiKararService.update(id, data)
    return res.status(200).json({error: null, data: toplanti})
}

exports.remove = async (req, res) => {
    let id = req.params.id
    return res.status(200).json({error: null, data: null})
}
