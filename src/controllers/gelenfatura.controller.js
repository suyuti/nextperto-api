const GelenFaturaService = require('../services/gelenfatura.service')
const TenantService = require('../services/tenant.service')
const EFaturaProvider = require('../providers/efatura.provider')
const fs = require('fs')
var AdmZip = require('adm-zip');

exports.getAll = async (req, res) => {
    let faturalar = await GelenFaturaService.find(req.query)
    return res.status(200).json({ error: null, data: faturalar })
}

exports.getById = async (req, res) => {
    let id = req.params.id
    let fatura = await GelenFaturaService.findOne(id, req.query)
    return res.status(200).json({ error: null, data: fatura })
}

exports.create = async (req, res) => {
    let data = req.body
    let fatura = await GelenFaturaService.create(data)
    return res.status(200).json({ error: null, data: fatura })
}

exports.update = async (req, res) => {
    let id = req.params.id
    let data = req.body
    let gorev = await GelenFaturaService.update(id, data, req.user)
    return res.status(200).json({ error: null, data: gorev })
}

exports.remove = async (req, res) => {
    let id = req.params.id
    return res.status(200).json({ error: null, data: null })
}

exports.getBelge = async (req, res) => {
    let id = req.params.id
    let fatura = await GelenFaturaService.findOne(id)

    let buf = Buffer.from(fatura.faturaPDF, 'base64')
    return res.end(buf)
}

