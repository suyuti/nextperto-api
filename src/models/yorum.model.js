const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var SchemaModel = new Schema({
    key         : String,
    type        : String,
    referans    : String,
    yorum       : String,
    vuser       : String,
    tarih       : String,

    okundu      : Boolean, // Okuma gorev sorumlusu ise
    okunmaTarihi: Date,
    okuyan      : String
},
{
    toJSON: {virtuals: true},
    toObject: {virtuals: true}
})

SchemaModel.virtual('user', {
    ref         : 'User',
    localField  : 'vuser',
    foreignField: 'key',
    justOne     : true
})

module.exports = mongoose.models.Yorum || mongoose.model("Yorum", SchemaModel);
