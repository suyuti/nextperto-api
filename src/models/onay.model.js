const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var SchemaModel = new Schema({
    //createdBy       : {type: mongoose.Schema.Types.ObjectId, ref: 'User'},  // Degisikligi kim yapti
    //createdAt       : Date,                                                 // Degisiklik ne zaman yapildi
    //changeDetail    : String,                                               // Degisiklik mesaji
    //diff            : {type: mongoose.Schema.Types.Mixed},                  // Yapilan degisiklik
    
    onayIsteyen     : {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
    onayIstenen     : {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
    onaylayan       : {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
    refCollection   : String,                                               // Degisiklik hangi collectionda yapildi
    referenceItem   : String,                                               // Degisiklik nerede yapildi
    istemeTarihi    : Date,
    onayTarihi      : Date,
    onayDurumu      : {type: String, enum: ['acik', 'iptal', 'onay', 'red']}, // acik: onay olusturuldu bekliyor, iptal: onay talebi geri cekildi
    onayAciklamasi  : String, // Onay veya Red verenin aciklamasi
    nextState       : { type: mongoose.Schema.Types.Mixed},
    diff            : { type: mongoose.Schema.Types.Mixed},
},
{
    toJSON: {virtuals: true},
    toObject: {virtuals: true}
})

module.exports = mongoose.models.Onay || mongoose.model("Onay", SchemaModel);
