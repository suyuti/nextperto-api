const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var SchemaModel = new Schema({
    key                     : { type: String },
    vToplanti               : { type: String },
    //toplantiId              : { type: mongoose.Schema.Types.ObjectId, ref: "Toplanti" },
    karar                   : String,
    vorganizerSorumlu       : String,
    //organizerSorumlu        : {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
    vfirmaSorumlu           : String,
    //firmaSorumlu            : {type: mongoose.Schema.Types.ObjectId, ref: 'Kisi'},
    tarih                   : Date,
    durum                   : { type: String, enum: ['Açık', 'Bilgi', 'Tamamlandı', 'İptal', 'Başarısız']} // 'Açık', 'Bilgi', 'Tamamlandı', 'İptal'
},
{
    toJSON: {virtuals: true},
    toObject: {virtuals: true}
})

SchemaModel.virtual('toplanti', {
    ref: 'Toplanti',
    localField: 'vToplanti',
    foreignField: 'key'
})

SchemaModel.virtual('firmaSorumlu', {
    ref: 'Kisi',
    localField: 'vfirmaSorumlu',
    foreignField: 'key',
    justOne     : true
})

SchemaModel.virtual('organizerSorumlu', {
    ref: 'User',
    localField: 'vorganizerSorumlu',
    foreignField: 'key',
    justOne     : true
})


module.exports = mongoose.models.ToplantiKarar || mongoose.model("ToplantiKarar", SchemaModel);
