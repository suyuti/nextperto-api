const mongoose = require('mongoose');
var diffHistory = require('mongoose-diff-history/diffHistory')
const Schema = mongoose.Schema;

var SchemaModel = new Schema({
    key                     : String,
    vfirma                  : String,
    vtahsilatYapanFirma     : String,
    tarih                   : Date,
    tutar                   : Number,
    aciklama                : String,
    vcreatedBy              : String,
    createdAt               : Date,
    durum                   : { type: String, enum: ['acik', 'ertelendi', 'odendi']},
},
{
    toJSON  : { virtuals: true },
    toObject: { virtuals: true }
})

SchemaModel.plugin(diffHistory.plugin)

SchemaModel.virtual('firma', {
    ref         : 'Firma',
    localField  : 'vfirma',
    foreignField: 'key',
    justOne     : true
})

SchemaModel.virtual('tahsilatYapanFirma', {
    ref         : 'Tenant',
    localField  : 'vtahsilatYapanFirma',
    foreignField: 'key',
    justOne     : true
})

SchemaModel.virtual('createdBy', {
    ref         : 'User',
    localField  : 'vcreatedBy',
    foreignField: 'key',
    justOne     : true
})

module.exports = mongoose.models.PlanlanmisTahsilat || mongoose.model("PlanlanmisTahsilat", SchemaModel);
