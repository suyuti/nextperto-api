const mongoose = require('mongoose');
//var diffHistory = require('mongoose-diff-history/diffHistory')
const Schema = mongoose.Schema;

var SchemaModel = new Schema({
    key: String,
    belgeNo: String,
    belgeSiraNo: String,
    belgeTarihi: String,
    belgeTuru: String,
    ettn: String,
    gonderenEtiket: String,
    gonderenVknTckn: String,

    // Fatura XML detayindan gelen bilgiler
    faturaId: String,
    faturaTarihi: String,
    faturaNo: String,
    faturaTipi: String,
    faturaTuru: String,
    paraBirimi: String,
    saticiVergiNo: String,
    saticiVergiDairesi: String,
    saticiUnvan: String,
    saticiUlke: String,
    saticiSehir: String,
    saticiIlce: String,
    saticiCaddeSokak: String,
    saticiTel: String,
    saticiEposta: String,

    kalemler: [{
        siraNo: Number,
        urunAdi: String,
        birimKodu: String,
        birimFiyat: Number,
        miktar: Number,
        malHizmetMiktari: Number,
        iskontoOrani: Number,
        iskontoTutari: Number,
        vergiler: [{
            toplamVergiTutari: Number,
            vergi: [{
                ad: String,
                kod: String,
                matrah: Number,
                oran: Number,
                vergiTutari: Number
            }]
        }]
    }],
    toplamMalHizmetMiktari: Number,
    toplamIskontoTutari: Number,
    toplamArtirimTutari: Number,
    vergiHaricToplam: Number,
    vergiDahilTutar: Number,
    yuvarlamaTutari: Number,
    odenecekTutar: Number,

    vergiler: [{
        toplamVergiTutari: Number,
        vergi: [{
            ad: String,
            kod: String,
            matrah: Number,
            oran: Number,
            vergiTutari: Number,
        }]
    }],

    faturaNot: String,

    // Fatura XML
    belgeVerisiXML: String,

    // Fatura PDF base64
    faturaPDF: String,

    sorguTarihi: Date,
    firmaAdi: String,
    firmaKey: String,

    odendi          : {type:Boolean, default: false},
    odemeTarihi     : Date,
    odemePlanlandi  : {type:Boolean, default: false},
    planlananOdemeTarihi: Date,
    tedarikci           :{type: mongoose.Schema.Types.ObjectId, ref: 'Tedarikci'},
    aliciFirma          :{type: mongoose.Schema.Types.ObjectId, ref: 'Tenant'},
},
    {
        toJSON: { virtuals: true },
        toObject: { virtuals: true }
    })

//SchemaModel.plugin(diffHistory.plugin)

SchemaModel.virtual('firma', {
    ref: 'Firma',
    localField: 'vfirma',
    foreignField: 'key',
    justOne: true
})

module.exports = mongoose.models.GelenFatura || mongoose.model("GelenFatura", SchemaModel);
