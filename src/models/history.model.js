const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var SchemaModel = new Schema({
    refCollection   : String,                                               // Degisiklik hangi collectionda yapildi
    createdBy       : {type: mongoose.Schema.Types.ObjectId, ref: 'User'},  // Degisikligi kim yapti
    createdAt       : Date,                                                 // Degisiklik ne zaman yapildi
    referenceItem   : String,                                               // Degisiklik nerede yapildi
    changeDetail    : String,                                               // Degisiklik mesaji
    diff            : {type: mongoose.Schema.Types.Mixed},                  // Yapilan degisiklik
},
{
    toJSON: {virtuals: true},
    toObject: {virtuals: true}
})

module.exports = mongoose.models.MyHistory || mongoose.model("MyHistory", SchemaModel);
