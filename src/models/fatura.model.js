const mongoose = require('mongoose');
var diffHistory = require('mongoose-diff-history/diffHistory')
const Schema = mongoose.Schema;

const YENI_KAYIT = -99

const ALINDI = 1
const ISLENEMEDI = 2
const ISLENDI = 3

const GIB_GONDERIM_IPTAL = -2
const GIB_GONDERIM_KUYRUKTA = -1
const GIB_GONDERILEMEDI = 0
const GIB_GONDERILECEK = 1
const GIB_GONDERILDI = 2
const GIB_YANITLADI = 3
const ALICI_YANITLADI = 4

const ALICI_YANITI_GEREKMIYOR = -1
const ALICI_YANITI_BEKLENIYOR = 0
const ALICI_REDDETTI = 1
const ALICI_KABUL_ETTI = 2

var SchemaModel = new Schema({
    key: String,
    vfirma: String,
    duzenlemeTarihi: Date,
    aciklama: String,
    vkesenFirma: String,
    dovizTuru: String,
    vadeTarihi: Date,
    vadeGun: Number,

    araToplam: { type: Number, default: 0 },
    kdvOrani: { type: Number, default: 0 },
    toplamKdv: { type: Number, default: 0 },
    genelToplam: { type: Number, default: 0 },

    acik: { type: Boolean, default: true },
    tahsilatTutari: { type: Number, default: 0 },
    taslak: Boolean,
    belgeOid: String,   // EFaturadan gelir
    faturaNo: String,   // EFaturadan gelir

    ubl: String,
    belgeXml: String,
    pdfOnIzleme: String, // Base64
    aliciEFatura: Boolean,

    // Tablo 3.1.4 gidenBelgeSorgula()
    gidenBelgeSoruSayisi: { type: Number, default: 0 },
    eFaturaDurum: {
        type: Number, enum: [
            YENI_KAYIT,
            ALINDI,
            ISLENEMEDI,
            ISLENDI],
        default: YENI_KAYIT
    },          // 5.2 Durum kodlari
    eFaturaDurumAciklama: String,                                   // ISLENEMEDI ise hata kodu yazar
    gonderimDurumu: {
        type: Number, enum: [
            YENI_KAYIT,
            GIB_GONDERIM_IPTAL,
            GIB_GONDERIM_KUYRUKTA,
            GIB_GONDERILEMEDI,
            GIB_GONDERILECEK,
            GIB_GONDERILDI,
            GIB_YANITLADI,
            ALICI_YANITLADI],
        default: YENI_KAYIT
    }, // 5.4 Gonderim kodlari
    gonderimCevabiKodu: Number,                                   // 1200 Basarili
    gonderimCevabiDetayi: String,                                   // GIB merkezden vrya alicidan gelen cvap detayi veya hata mesaji
    yanitDurumu: {
        type: Number, enum: [
            YENI_KAYIT,
            ALICI_YANITI_GEREKMIYOR,
            ALICI_YANITI_BEKLENIYOR,
            ALICI_REDDETTI,
            ALICI_KABUL_ETTI],
        default: YENI_KAYIT
    }, // 5.5 yanit durumu kodlari
    yanitDetayi: String,                                   // Alicidan gelen kabul vera red cevabi notu
    yanitTarihi: String,                                   // YYYYAAGG
    olusturulmaTarihi: String,                                   // YYYYAAGGSSDDssMMM
    gonderimTarihi: String,                                   // YYYYAAGGSSDDssMMM
    ettn: String,                                   // UUID
    belgeNo: String,                                   // Fatura No
    gidenBelgePdf: String,                                   // Basarili gonderilmis fatura belgesi. Base64 PDF
    //urunler: [{
    //    key: String,
    //    adi: String,
    //    aciklama: String,
    //    birimFiyat: Number,
    //    adet: Number,
    //    kdvOran: Number,
    //    toplam: Number,
    //}]
    faturaTuru: {type: String, enum: ['SATIS', 'IADE']},
    iadeFaturaNo: String,
    iadeFaturaTarih: String, // iade edilen faturanin tarihi
    iadeTarihi: Date, // iade islemi tarihi,
    iadeEdildi: {type: Boolean, default: false},

    //topluFaturaListesi: {type: mongoose.Schema.Types.ObjectId, ref: 'TopluFatura'}, // Toplu Fatura Listesinden kesildiyse
    //topluFaturaKesimDonemi: String,                                                 // Hangi donem icin kesildi MM-YYYY
    status: String
},
    {
        toJSON: { virtuals: true },
        toObject: { virtuals: true }
    })

SchemaModel.plugin(diffHistory.plugin)

SchemaModel.virtual('firma', {
    ref: 'Firma',
    localField: 'vfirma',
    foreignField: 'key',
    justOne: true
})

SchemaModel.virtual('kesenFirma', {
    ref: 'Tenant',
    localField: 'vkesenFirma',
    foreignField: 'key',
    justOne: true
})

module.exports = mongoose.models.Fatura || mongoose.model("Fatura", SchemaModel);
