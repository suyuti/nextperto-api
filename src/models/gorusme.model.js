const mongoose = require('mongoose');
const Schema = mongoose.Schema;
var diffHistory = require('mongoose-diff-history/diffHistory')

var SchemaModel = new Schema({
    key         : String,
    not         : String,
    tarih       : Date,
    saat        : String,
    vfirma      : String,
    vkisi       : String,
    kanal       : String,
    icGorusmeMi : Boolean,
    vcreatedBy  : String,
    createdAt   : Date
},
{
    toJSON: {virtuals: true},
    toObject: {virtuals: true}
})

SchemaModel.plugin(diffHistory.plugin)

SchemaModel.virtual('firma', {
    ref         : 'Firma',
    localField  : 'vfirma',
    foreignField: 'key',
    justOne     : true
})

SchemaModel.virtual('kisi', {
    ref         : 'Kisi',
    localField  : 'vkisi',
    foreignField: 'key',
    justOne     : true
})

SchemaModel.virtual('user', {
    ref         : 'User',
    localField  : 'vuser',
    foreignField: 'key',
    justOne     : true
})

SchemaModel.virtual('createdBy', {
    ref         : 'User',
    localField  : 'vcreatedBy',
    foreignField: 'key',
    justOne     : true
})

module.exports = mongoose.models.Gorusme || mongoose.model("Gorusme", SchemaModel);
