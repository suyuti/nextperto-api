const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var SchemaModel = new Schema({
    fatura: { type: mongoose.Schema.Types.ObjectId, ref: 'GelenFatura' },
    odemeYapacakFirma: { type: mongoose.Schema.Types.ObjectId, ref: 'Tenant' },
    planlananOdemeTarihi: Date,
    planlananOdemeTutari: Number,
    odemeTuru: { type: String, enum: ['nakit', 'eft', 'cek'] },
    createdBy: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
    createdAt: Date,
    durum: { type: String, enum: ['acik', 'kapali', 'iptal'] },
},
    {
        toJSON: { virtuals: true },
        toObject: { virtuals: true }
    })

module.exports = mongoose.models.OdemePlani || mongoose.model("OdemePlani", SchemaModel);
