const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var SchemaModel = new Schema({
    username    : {type: String, required: true},
    email       : String,
    vroles      : [String],
    oid         : String,
    active      : Boolean,
    tenants     : [{type: mongoose.Schema.Types.ObjectId, ref: 'Tenant'}],
    isAdmin     : Boolean,
    key         : String
},
)

SchemaModel.virtual('roles', {
    ref         : 'Role',
    localField  : 'vroles',
    foreignField: 'key',
})


module.exports = mongoose.models.User || mongoose.model("User", SchemaModel);
