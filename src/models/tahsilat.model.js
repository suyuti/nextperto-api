const mongoose = require('mongoose');
var diffHistory = require('mongoose-diff-history/diffHistory')
const Schema = mongoose.Schema;

var SchemaModel = new Schema({
    key                 : String,
    vfirma              : String,
    vtahsilatYapanFirma : String,
    tahsilatTarihi      : Date,
    tutar               : Number,
    hesap               : String,
    aciklama            : String,
    tahsilatTuru        : { type: String, enum: ['nakit', 'cek'] },
    cekVadeTarihi       : Date,
    cekNo               : String,
    cekBanka            : String,
    vcreatedBy          : String,
    createdAt           : Date
},
    {
        toJSON: { virtuals: true },
        toObject: { virtuals: true }
    })
SchemaModel.plugin(diffHistory.plugin)

SchemaModel.virtual('firma', {
    ref         : 'Firma',
    localField  : 'vfirma',
    foreignField: 'key',
    justOne     : true
})

SchemaModel.virtual('tahsilatYapanFirma', {
    ref         : 'Tenant',
    localField  : 'vtahsilatYapanFirma',
    foreignField: 'key',
    justOne     : true
})

SchemaModel.virtual('createdBy', {
    ref         : 'Firma',
    localField  : 'vcreatedBy',
    foreignField: 'key',
    justOne     : true
})

module.exports = mongoose.models.Tahsilat || mongoose.model("Tahsilat", SchemaModel);
