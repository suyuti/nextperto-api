const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var SchemaModel = new Schema({
    fatura              : {type: mongoose.Schema.Types.ObjectId, ref: 'GelenFatura'},
    odemeTarihi         : Date,
    odemeTutari         : Number,
    odemeYapilanHesap   : String,
    odemeYapanFirma     : {type: mongoose.Schema.Types.ObjectId, ref: 'Tenant'},
    odemeTuru           : {type: String, enum: ['nakit', 'eft', 'cek']},
    createdBy           : {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
    createdAt           : Date
},
{
    toJSON: {virtuals: true},
    toObject: {virtuals: true}
})

module.exports = mongoose.models.Odeme || mongoose.model("Odeme", SchemaModel);
