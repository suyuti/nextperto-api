const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var SchemaModel = new Schema({
    adi     : {type: String, required: true},
    adres: String,
    il: String,
    ilce: String,
    ulke: String,
    telefon: String,
    mail: String,
    vergiDairesi: String,
    vergiNo: String,
    
})

module.exports = mongoose.models.Tedarikci || mongoose.model("Tedarikci", SchemaModel);
