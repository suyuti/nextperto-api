const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var SchemaModel = new Schema({
    key         : { type: String},
    mesaj       : String,
    tarih       : Date,
    vkimden     : String,
    okundu      : {type: Boolean, default: false}
}, {
    toJSON: {virtuals: true},
    toObject: {virtuals: true}
})

let v = SchemaModel.virtual('kimden', {
    ref         : 'User',
    localField  : 'vkimden',
    foreignField: 'key',
    justOne     : true
})

module.exports = mongoose.models.Notification || mongoose.model("Notification", SchemaModel);
