const mongoose = require('mongoose');
const MpathPlugin = require ('mongoose-mpath');
const Schema = mongoose.Schema;

var SchemaModel = new Schema({
    key: String,
    adi: { type: String, required: true },
    vyonetici: String
}, {
    toJSON: { virtuals: true },
    toObject: { virtuals: true }
})

SchemaModel.virtual('yonetici', {
    ref: 'User',
    localField: 'vyonetici',
    foreignField: 'key'
})

module.exports = mongoose.models.Departman || mongoose.model("Departman", SchemaModel.plugin(MpathPlugin));
