const mongoose = require('mongoose');
var diffHistory = require('mongoose-diff-history/diffHistory')
const Schema = mongoose.Schema;

var SchemaModel = new Schema({
    listeAdi: String,
    liste: [{
        vfirma       : String,
        urun        : { type: mongoose.Schema.Types.ObjectId, ref: 'Urun' },
        aciklama    : { type: String},
        birimFiyat  : Number,
        kdv         : Number,
        toplam      : Number,

        vade        : Date,
        vadeGun     : Number,
        araToplam   : Number,
        kdvOrani    : Number,
        genelToplam : Number,
    }],
    vkesenFirma     : String,
    sabitFiyatliMi  : Boolean,
    faturaKesimGunu : Number,
    active          : {type: Boolean, default: true},
    deleted         : {type: Boolean, default: false}
},
{
    toJSON: {virtuals: true},
    toObject: {virtuals: true}
})

SchemaModel.plugin(diffHistory.plugin)

SchemaModel.virtual('liste.firma', { 
    ref         : 'Firma',
    localField  : 'liste.vfirma',
    foreignField: 'key',
    justOne     : true
})

SchemaModel.virtual('kesenFirma', { 
    ref         : 'Tenant',
    localField  : 'vkesenFirma',
    foreignField: 'key',
    justOne     : true
})

module.exports = mongoose.models.TopluFatura || mongoose.model("TopluFatura", SchemaModel);
