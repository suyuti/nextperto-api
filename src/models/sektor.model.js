const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var SchemaModel = new Schema({
    key    : String,
    adi    : {type: String, required: true},
})

module.exports = mongoose.models.Sektor || mongoose.model("Sektor", SchemaModel);
