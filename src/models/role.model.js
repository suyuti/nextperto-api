const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var SchemaModel = new Schema({
    key     : { type: String },
    adi     : { type: String, required: true},
    active  : Boolean,
    permissions: [{key: String, create: Boolean, read: Boolean, write: Boolean, dlete: Boolean}]
})


module.exports = mongoose.models.Role || mongoose.model("Role", SchemaModel);
