const express = require("express");
const cors = require("cors");
const bodyParser = require("body-parser");
const app = express();
const tokenControl = require('./middlewares/token')
const yetkiKontrol = require('./middlewares/yetki.middleware')
const db = require('./helpers/mongo')
const http = require('http').Server(app);
/*const io = require('socket.io')(http, {
    cors: {
        origin: 'http://localhost:3000',
        methods: ['GET', 'POST']
    }
});
*/
require('./helpers/mongo')
const ioService = require('./services/socket.service')

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json({ limit: '100mb' }));
app.use(cors());
app.use(tokenControl.tokenControl)
app.use(yetkiKontrol.yetkiKontrol)
db()
app.use('/api', require('./routes/v1'))
const port = process.env.API_PORT || 3001;
//const server = app.listen(port, () => {
//    console.log(`Listening on ${port}`)
//})

/*
io.on('connection', (socket) => {
    console.log('a user connected');
    socket.emit('message', 'Hosgeldin')
});
*/

ioService.initialize(http)

http.listen(port, () => {
    console.log(`Listening on ${port}`)
})
